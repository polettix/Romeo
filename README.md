Terminal utilities, mainly for fun and celebrating a great cat.

Installation:

```
# cd /somewhere/in/PATH
curl -LO https://codeberg.org/polettix/Romeo/raw/branch/main/romeo
chmod +x romeo
```

Available sub-commands (v.0.92):

```
         avatar: Generate ASCII-art avatars
         blocky: Turn stuff into Unicode blocky stuff
                 (also as: blk)
         choose: choose items from a list
             cf: validate Italian "Codice Fiscale"
                 (also as: codicefiscale, codice-fiscale)
          color: Colorize lines according to regular expressions
                 (also as: colorize)
      corkscrew: Corkscrew in Perl (https://github.com/bryanpkc/corkscrew)
       csv2json: Turn a CSV into a JSON file
           emim: Parse MIME email messages
                 (also as: mime-x, mime-extract)
          frame: Put a frame around the input
   gpg-key-list: filter gpg --list-keys --with-colons
                 (also as: gpg-list-keys)
          heail: head and tail a file
           hmac: calculate HMAC from several hashing algorithms
       json2csv: Turn a JSON into a CSV file
           mime: Build MIME email messages
                 (also as: mime-b, mime-build)
            pad: Pad input with whitespaces
           pass: generate a random password
                 (also as: password)
            png: Get PNG metadata from files
                 (also as: pngmeta, png-meta)
     same-width: Make all lines the same width (or at least try)
          slice: Slice input data and keep tasty samples
    slice-build: Slice input data and keep definitions for tasty samples
         teepee: Render Template::Perlish templates from JSON data
                 (also as: tp)
           time: Convert times depending on needs
      urldecode: url decode (or encode, depending on the name)
                 (also as: urlencode)
        version: print version of Romeo
            xxd: hex dumper a-la xxd (shipped with the Vim editor)
           help: print a help command
       commands: list sub-commands
           tree: print sub-commands in a tree
```

# COPYRIGHT & LICENSE

See separate file `lib/Romeo.pm`, in function `sub copying`.
