#!/usr/bin/env perl
package Romeo;
use v5.24;
use warnings;
use experimental qw< signatures >;
{ our $VERSION = '0.166' }

use App::Easer::V2 dd => -command => -spec => {
   aliases => [ 'MAIN' ],
   help => 'Mixture of helpers for command-line and TUIs',
   options => [
      {
         getopt => 'copying',
         help => 'get help about copyright & licenses',
      },
      {
         getopt => 'help',
         help => 'get help on the program',
      },
      {
         getopt => 'version',
         help => 'get the version of the program',
      }
   ],
   default_child => '-self',
};

sub version ($self) { our $VERSION }

sub execute ($self) {
   if ($self->config('copying')) {
      say $self->copying;
      return 0;
   }
   if ($self->config('version')) {
      say $self->version;
      return 0;
   }
   return $self->run_help;
}

sub description ($self) {
   my $basic = <<'END'

Romeo is a program implementing several sub-commands that can be of help on
the command line. The idea is that each will operate according to the Unix
philosophy --make one thing, and (try to) make it right--, while the whole
lot will be packed into a single Perl program for ease of transportation.

Romeo was a cat friend, who is and will be missed.
END

}

sub copying ($self) {
   my $basic = <<'END';
>  Copyright 2023 by Flavio Poletti (flavio@polettix.it).
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.
END


   if (scalar(@main::__MOBUNDLE_MODULES__) == 0) { # not bundled?
      return <<"END";

The contents of this program (including the modules it is directly composed
of, in the Romeo::* namespace) are licensed according to the Apache License
2.0:

$basic
END
   }

   # bundled version here
   my $full = << "END";

Except for what described below, the contents of this
repository/package/program are licensed according to the Apache License 2.0:

$basic
END

   $full .= <<'END';
This version of the program contains a bundle of the modules it depends on,
stripped of their respective documentation to squeeze them as much as
possible (or reasonable, anyway).

The original code belongs to the respective authors; you can find the
original code in CPAN, e.g.:

- "App::Easer" - https://metacpan.org/dist/App-Easer
- "Capture::Tiny" - https://metacpan.org/dist/Capture-Tiny
- "Data::HexDump::XXD" - https://metacpan.org/dist/Data-HexDump-XXD
- "Email::Address" - https://metacpan.org/dist/Email-Address
- "MailTools" - https://metacpan.org/dist/MailTools
- "MIME-tools" - https://metacpan.org/dist/MIME-tools
- "Ouch" - https://metacpan.org/dist/Ouch
- "Path::Tiny" - https://metacpan.org/dist/Path-Tiny
- "Role::Tiny" - https://metacpan.org/dist/Role-Tiny
- "Template::Perlish" - https://metacpan.org/dist/Template-Perlish
- "Term::ANSIColor" - https://metacpan.org/dist/Term-ANSIColor
- "Term::Choose" - https://metacpan.org/dist/Term-Choose
- "TimeDate" - https://metacpan.org/dist/TimeDate
- "Text::CSV" - https://metacpan.org/dist/Text-CSV
- "Try::Catch" - https://metacpan.org/dist/Try-Catch
- "YAML::Tiny" - https://metacpan.org/dist/YAML-Tiny

Additionally, the program also bundles a copy of the X11 "rgb.txt" file with
X11 color definitions.

The ASCII art in sub-command `version` is by Felix Lee
(https://montcs.bloomu.edu/Graphics/ascii-art.html).

# RGB data

This repository/package/program includes an embedded copy of
https://gitlab.freedesktop.org/xorg/app/rgb/raw/master/rgb.txt which
is covered by the following:

> Copyright 1985, 1989, 1998  The Open Group
>
> Permission to use, copy, modify, distribute, and sell this software and its
> documentation for any purpose is hereby granted without fee, provided that
> the above copyright notice appear in all copies and that both that
> copyright notice and this permission notice appear in supporting
> documentation.
>
> The above copyright notice and this permission notice shall be included
> in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
> OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
> IN NO EVENT SHALL THE OPEN GROUP BE LIABLE FOR ANY CLAIM, DAMAGES OR
> OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
> ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
> OTHER DEALINGS IN THE SOFTWARE.
>
> Except as contained in this notice, the name of The Open Group shall
> not be used in advertising or otherwise to promote the sale, use or
> other dealings in this Software without prior written authorization
> from The Open Group.
>
> Copyright (c) 1994, 2008, Oracle and/or its affiliates. All rights reserved.
>
> Permission is hereby granted, free of charge, to any person obtaining a
> copy of this software and associated documentation files (the "Software"),
> to deal in the Software without restriction, including without limitation
> the rights to use, copy, modify, merge, publish, distribute, sublicense,
> and/or sell copies of the Software, and to permit persons to whom the
> Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice (including the next
> paragraph) shall be included in all copies or substantial portions of the
> Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
> THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
> FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
> DEALINGS IN THE SOFTWARE.

# Embedded modules

The repository/package/program contains a *bundled* version of the program,
containing all the code including the modules, which have their own
copyright notices as in the following sub-sections.

## App::Easer::V2

> Copyright 2021 by Flavio Poletti (flavio@polettix.it).
>
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
>
>     http://www.apache.org/licenses/LICENSE-2.0
>
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

## Capture::Tiny

> This software is Copyright (c) 2009 by David Golden.
>
> This is free software, licensed under:
>
>   The Apache License, Version 2.0, January 2004

## Data::HexDumper::XXD

> Copyright (c) 2007, Flavio Poletti <flavio [at] polettix [dot] it>. All
> rights reserved.
>
> This module is free software; you can redistribute it and/or modify it
> under the same terms as Perl itself. See perlartistic and perlgpl.
>
> Questo modulo è software libero: potete ridistribuirlo e/o modificarlo
> negli stessi termini di Perl stesso. Vedete anche perlartistic e perlgpl.

## Email::Address

> This software is copyright (c) 2004 by Casey West.
>
> This is free software; you can redistribute it and/or modify it under the
> same terms as the Perl 5 programming language system itself.

## MailTools

> Copyrights 1995-2000 Graham Barr <gbarr@pobox.com>
> and 2001-2017 Mark Overmeer <perl@overmeer.net>.
>
> This program is free software; you can redistribute it and/or modify it
> under the same terms as Perl itself.
> See http://www.perl.com/perl/misc/Artistic.html

## MIME-Tools

> Eryq (eryq@zeegee.com), ZeeGee Software Inc (http://www.zeegee.com).
> Dianne Skoll (dianne@skoll.ca)
>
> Copyright (c) 1998, 1999 by ZeeGee Software Inc (www.zeegee.com).
> Copyright (c) 2004 by Roaring Penguin Software Inc
> Copyright (c) 2022 by Dianne Skoll
>
> This program is free software; you can redistribute it and/or modify it
> under the same terms as Perl itself.

## Ouch

> Ouch is Copyright 2011 Plain Black Corporation (http://www.plainblack.com)
> and is licensed under the same terms as Perl itself.

## Path::Tiny

> This software is Copyright (c) 2014 by David Golden.
>
> This is free software, licensed under:
>
>     The Apache License, Version 2.0, January 2004

## Role::Tiny and Role::Tiny::With

> Copyright (c) 2010-2012 the Role::Tiny "AUTHOR" and "CONTRIBUTORS" as
> listed above.
>
> This library is free software and may be distributed under the same terms
> as perl itself.

At the time of inclusion, the list is available in the module's
documentation available [here](https://metacpan.org/pod/Role::Tiny#LICENSE).
The list below does *not* include the addresses in a feeble attempt to limit
the range of automatic email address collectors:

- AUTHOR: mst - Matt S. Trout (cpan:MSTROUT)
- CONTRIBUTORS:
-    dg - David Leadbeater (cpan:DGL)
-    frew - Arthur Axel "fREW" Schmidt (cpan:FREW)
-    hobbs - Andrew Rodland (cpan:ARODLAND)
-    jnap - John Napiorkowski (cpan:JJNAPIORK)
-    ribasushi - Peter Rabbitson (cpan:RIBASUSHI)
-    chip - Chip Salzenberg (cpan:CHIPS)
-    ajgb - Alex J. G. Burzyński (cpan:AJGB)
-    doy - Jesse Luehrs (cpan:DOY)
-    perigrin - Chris Prather (cpan:PERIGRIN)
-    Mithaldu - Christian Walde (cpan:MITHALDU)
-    ilmari - Dagfinn Ilmari Mannsåker (cpan:ILMARI)
-    tobyink - Toby Inkster (cpan:TOBYINK)
-    haarg - Graham Knop (cpan:HAARG)

## Template::Perlish

> Copyright (c) 2008-2016 by Flavio Poletti polettix@cpan.org.
>
> This module is free software. You can redistribute it and/or modify it
> under the terms of the Artistic License 2.0.
>
> This program is distributed in the hope that it will be useful, but
> without any warranty; without even the implied warranty of merchantability
> or fitness for a particular purpose.

## Term::ANSIColor

This module is shipped with `perl` itself, although the program uses
features that are available only from release 5 on, hence the inclusion. The
email address of the copyright holders have been stripped away, they can be
found in the documentation page at
https://metacpan.org/pod/Term::ANSIColor#COPYRIGHT-AND-LICENSE.

> Copyright 1996-1998, 2000-2002, 2005-2006, 2008-2018, 2020 Russ Allbery
>
> Copyright 1996 Zenin
>
> Copyright 2012 Kurt Starsinic
>
> This program is free software; you may redistribute it and/or modify it
> under the same terms as Perl itself.

## Term::Choose

This module comes along with ancillary modules in the Term::Choose
namespace, all within the same distribution.

> Copyright (C) 2012-2022 Matthäus Kiem.
>
> This library is free software; you can redistribute it and/or modify it
> under the same terms as Perl 5.10.0. For details, see the full text of the
> licenses in the file LICENSE.

## Text::CSV

"Text::CSV":

> Copyright (C) 1997 Alan Citterman. All rights reserved. Copyright (C)
> 2007-2015 Makamaka Hannyaharamitu. Copyright (C) 2017- Kenichi Ishigaki A
> large portion of the doc is taken from Text::CSV_XS. See below.

"Text::CSV_PP":

> Copyright (C) 2005-2015 Makamaka Hannyaharamitu. Copyright (C) 2017-
> Kenichi Ishigaki A large portion of the code/doc are also taken from
> Text::CSV_XS. See below.

"Text:CSV_XS":

> Copyright (C) 2007-2016 H.Merijn Brand for PROCURA B.V. Copyright (C)
> 1998-2001 Jochen Wiedmann. All rights reserved. Portions Copyright (C)
> 1997 Alan Citterman. All rights reserved.
>
> This library is free software; you can redistribute it and/or modify it
> under the same terms as Perl itself.

## TimeDate

> Copyright 1995-2009 Graham Barr.
>
> This library is free software; you can redistribute it and/or modify
> it under the same terms as Perl itself.

## Try::Catch

Author is Mamod A. Mehyar. The email address of the author can be found in
the documentation page at https://metacpan.org/pod/Try::Catch#AUTHOR.

> This library is free software; you can redistribute it and/or modify it
> under the same terms as Perl itself

## YAML::Tiny

> Copyright 2006 - 2013 Adam Kennedy.
>
> This program is free software; you can redistribute it and/or modify it
> under the same terms as Perl itself.
>
> The full text of the license can be found in the LICENSE file included
> with this module.

END

   return $full;
}

1;
