#!/usr/bin/env perl
package Romeo::CmdMime::CmdRecipients;
use v5.24;
use warnings;
use experimental qw< signatures >;
use Email::Address ();
use List::Util qw< uniqstr >;
use Try::Catch;
use Ouch qw< :trytiny_var >;
use Romeo::Util qw< encode_json >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 -command => -spec => {
   help => 'get recipients',
   aliases => [qw< recipients >],
   description => <<'END',

Extract list(s) of recipients from email messages.

By default the output will be a JSON object indexed with the input file
name, with sub-objects carrying values for "to", "cc", and "bcc":

   $ romeo mime recipients sample1.eml sample2.eml
   {
      "sample1.eml" : {
         "bcc" : [],
         "cc" : [],
         "to" : [
            "foo@example.com",
            "foo@example.com",
            "bar@example.com"
         ]
      },
      "sample2.eml" : {
         "bcc" : [
            "foo@example.com"
         ],
         "cc" : [],
         "to" : [
            "FOO@example.com",
            "bar@example.com",
            "baz@example.com"
         ]
      }
   }

File name "-" is a shortand for reading the standard input, which might
come handy in pipelines:

   $ romeo mime create --to foo@example.com --bcc bar@example.com \
      | romeo mime recipients
   {
      "-" : {
         "bcc" : [
            "bar@example.com"
         ],
         "cc" : [],
         "to" : [
            "foo@example.com"
         ]
      }
   }

By default all three "to"/"cc"/"bcc" are taken, but it's possible to
restrict this with options "--to"/"--cc"/"--bcc" respectively (what is
provided on the command line eventually appears in the output):

   $ romeo mime create --to foo@example.com --bcc bar@example.com  
      | romeo mime recipients --bcc
   {
      "-" : {
         "bcc" : [
            "bar@example.com"
         ]
      }
   }

Option "--flat" merges all lists together into a single one, represented
as a JSON array:

   $ romeo mime create --to foo@example.com --bcc bar@example.com \
      | romeo mime recipients  ./tmp/sample.eml - --flat
   [
      "bar@example.com",
      "baz@example.com",
      "bar@example.com",
      "bar@example.com",
      "foo@example.com"
   ]

Option "--unique" can filter out duplicates:


   $ romeo mime create --to foo@example.com --bcc bar@example.com \
      | romeo mime recipients  ./tmp/sample.eml - --flat --unique
   [
      "bar@example.com",
      "baz@example.com",
      "foo@example.com"
   ]

Last, option "--plain" prints out the list of (selected) addresses, one
per line:

   $ romeo mime create --to foo@example.com --bcc bar@example.com \
      | romeo mime recipients  ./tmp/sample.eml - --flat --unique --plain
   bar@example.com
   baz@example.com
   foo@example.com

END

   options_help => {
      preamble => <<'END',

Non-option arguments represent input files, in EML format. File name "-"
represents standard input. If nothing is provided, standard input is
assumed as well.

By default no email will be sent because of option "--dry-run" being set
by default. It is possible to disable this protection either by passing
command-line option "--no-dry-run" or by setting the associated
environment variable "POSTEL_DRY_RUN" to a false value. Dry run mode
default is disable when "--print" or "--print-to" options are present,
because they mean that no actual message would be sent anyway.

== wnat to give this a try? ==

Head to https://ethereal.email/ and click on "Create Ethereal Account"
(no registration required as of September 2024). You will get a page
with some configuration details, like the host, port, username,
password, etc. that you can set as environment variables:

   $ esport POSTEL_HOST=smtp.ethereal.email POSTEL_TLS=starttls
   $ export POSTEL_USERNAME=.... POSTEL_PASSWORD=....

The same page links to the Messages page, where all messages sent
through SMTP will be collected (nothing is actually sent to any
mailbox).



Supported options:

END
   },
};

sub options ($self) {
   return (
      $self->resolve_options('+parent'),
      $self->handle_help_options,
      $self->input_output_options(qw< output >),
      {
         getopt => 'address|a!',
         help => 'get addresses only (no display name, etc.)',
      },
      {
         getopt => 'bcc',
         help => 'get "bcc" recipeints',
      },
      {
         getopt => 'cc',
         help => 'get "cc" recipeints',
      },
      {
         getopt => 'flat|mix|F',
         help => 'mix recipients in a single output array',
      },
      {
         getopt => 'output|o=s',
         help => 'output file',
         default => '-',
      },
      {
         getopt => 'to',
         help => 'get "to" recipients',
      },
      {
         getopt => 'unique',
         help => 'get only unique values',
      },
      {
         getopt => 'plain',
         help => 'get plain text, not a JSON output (implies --flat',
      },
   );
}

use constant FIELDS => qw< to cc bcc >;

sub execute ($self) {
   return 0 unless $self->handle_help;

   $self->set_config(input => [ $self->residual_args ]);

   my @fields = qw< to cc bcc >;
   my @wants = grep { $self->config($_) } FIELDS;
   @wants = FIELDS unless @wants;
   my $plain = $self->config('plain');
   my $flat = $plain || $self->config('flat');
   my $unique = $self->config('unique');

   my $parser = MIME::Parser->new;
   my (%collected, @all, $did_stdin);
   for (1 .. $self->n_inputs) {
      try {
         my $message = $parser->parse($self->next_ifh);
         my $input = $self->current_input;
         my $c = $collected{$input} = $self->recipients($message, @wants);
         if ($flat) {
            push @all, map { $_->@* } values($c->%*);
         }
         elsif ($unique) {
            $_->@* = uniqstr($_->@*) for values($c->%*);
         }
      }
      catch {
         my $input = $self->current_input;
         ouch 400, "error parsing <$input>: " . bleep;
      };
   } ## end for (1 .. $self->n_inputs)

   @all = uniqstr(@all) if $flat && $unique;

   my $output = $plain ? join("\n", @all, '')
      : encode_json($flat ? \@all : \%collected);
   my $ofh = $self->ofh;
   print {$ofh} $output;
   return 0;
} ## end sub execute

sub recipients ($self, $message, @fields) {
   my $head = $message->head;
   my $ao = $self->config('address'); # get address only, strip name etc.
   return {
      map {
         my @values =
            map { $ao ? $_->address : $_->format }
            map { Email::Address->parse($_)      }
            $head->get_all($_);
         $_ => \@values;
      } @fields
   };
}

1;
