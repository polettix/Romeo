#!/usr/bin/env perl
package Romeo::CmdMime::CmdCreate;
use v5.24;
use warnings;
use experimental qw< signatures >;
use Email::Address ();
use List::Util qw< uniqstr >;
use Try::Catch;
use Ouch qw< :trytiny_var >;
use Path::Tiny;
use JSON::PP qw< decode_json >;
use MIME::Entity;
use Encode qw< encode >;
use Time::Local qw< timegm timelocal >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 -command => -spec => {
   aliases => [qw< create >],
   help => 'Create new email messages in EML format',
   description => <<'END',

Create a new email message via Email::Stuffer.

Basic options should be pretty straighforward to use:

- "--from" accepts one sender email address
- "--to"/"--cc"/"--bcc" can all be provided multiple times on the
  command line to add more addresses

The subject and message of the email can be provided as text and/or HTML
using options:

- "--subject", "--text" and "--html" sets the string to include in the
  respective sections (i.e. you will want to provide HTML text to option
  "--html")
- "--subject-from", "--text-from" and "--html-from" allow loading the
  respective strings from a file.

In all cases, it's also possible to treat the strings as
Template::Perlish templates and expand them with variables provided via
JSON objects:

- "--template-perlish" sets the JSON string or a path to a JSON file
  with key/value pairs to set for *both* text and HTML
- "--subject-template-perlish" sets the json string/path for key/value
  pairs applicable to the subject part only
- "--text-template-perlish" sets the json string/path for key/value
  pairs applicable to the text part only
- "--html-template-perlish" sets the JSON string/path for key/value
  pairs applicable to the HTML part only

The presence of any variable triggres the expansion as templates.

If the option starts with an open curly brace, the option is assumed to
be a JSON string and parsed accordingly; otherwise, it's considered a
file path. In case the file name starts with a curly brace, just pass
the path as absolute or relative to the current directory, so that the
first character is not an open brace.

Specific JSON key/value pairs will take precedence on comon ones; other
than this, later JSON definitions on the command line override previous
ones:

   $ ./postel create --subject 'Hey [% foo %]' \
         --subject-tp '{"foo":"bar"}' \
         --subject-tp '{"foo":"galook"}'
   Date: Wed, 25 Sep 2024 22:25:29 +0200
   MIME-Version: 1.0
   Subject: Hey galook

Options "--attach" and "--attach-string" allow adding attachments, with
content that is respectively loaded from a file or directly provided on
the command line (contrarily to text/html, it's assumed that attachments
will preferentially be loaded from a file). It is possible to set
options for attached files by pre-pending the file name or content with
a JSON prefix, like this:

   $ postel create --subject foo --text bar \
      --attach-string '{"filename":"foo.tar.gz"}something'

Supported options are the same as the attributes in
https://metacpan.org/pod/Email::MIME#create

If you need to pass data or a filename starting with open curly brace,
just pass an empty JSON object in the beginning.

END
};

sub options ($self) { 
   return (
      $self->resolve_options('+parent'),
      $self->handle_help_options,
      $self->input_output_options(qw< output >),
      {
         getopt => 'lorem|lorem-ipsum|L',
         help   => 'fill in blank "normal" fields if absent',
      },
      {
         getopt => 'lorem-size|lorem-ipsum-size=s',
         help   => 'text+html size for lorem-ipsum filled messages',
      },
      {
         getopt => 'from|f=s',
         help   => 'from header field',
      },
      {
         getopt => 'to|t=s@',
         help   => 'main recipient(s)',
      },
      {
         getopt => 'cc|c=s@',
         help   => 'carbon-copy recipient(s)',
      },
      {
         getopt => 'bcc|b=s@',
         help   => 'blind carbon-copy recipient(s)',
      },
      {
         getopt => 'reply-to|reply_to|r=s@',
         help   => 'reply-to header field',
      },
      {
         getopt => 'subject|s=s',
         help   => 'subject of the message',
      },
      {
         getopt => 'subject-from=s',
         help => 'subject of the message, loaded from a file',
      },
      {
         getopt => 'subject-template-perlish|subject-tp=s@',
         help => 'expand the ssubject with Template::Perlish, using data from JSON',
      },
      {
         getopt => 'text|text-body|T=s',
         help   => 'textual part of the body',
      },
      {
         getopt => 'text-from|text-body-from=s',
         help   => 'file for textual part of the body',
      },
      {
         getopt => 'text-template-perlish|text-tp=s@',
         help   => 'expand text part with Template::Perlish, using data from JSON',
      },
      {
         getopt => 'html|html-body|H=s',
         help   => 'HTML part of the body',
      },
      {
         getopt => 'html-from|html-body-template=s',
         help   => 'file for HTML part of the body',
      },
      {
         getopt => 'html-template-perlish|html-tp=s@',
         help   => 'expand HTML part with Template::Perlish, using data from JSON',
      },
      {
         getopt => 'template-perlish|tp|X=s@',
         help   => 'expand text and HTML parts with Template::Perlish from JSON',
      },
      {
         getopt => 'attach|attachment|attach-from|a=s@',
         help   => 'attachment to add to the email, loaded from file',
      },
      {
         getopt => 'attach-string|attachment-string=s@',
         help   => 'attachment to add to the email (provided as string)',
      },
      {
         getopt => 'add-header|X=s@',
         help   => 'set additional headers at top level'
      },
      {
         getopt => 'output|o=s',
         help   => 'output for generated email',
         default => '-',
      },
   ),
}

use Romeo::Util qw< encode_json >;

sub execute ($self) {
   return 0 unless $self->handle_help;

   my %message = (
      'X-Mailer' => undef,
      __additional_headers($self->config('add-header') // []),
      __may(subject   => $self->get_textual('subject')),
      __may(reply_to  => $self->config('reply-to')),
      ( map { $self->_may($_) } qw< to from cc bcc > ),
      __may(_text_body => $self->get_textual('text')),
      __may(_html_body => $self->get_textual('html')),
      __may(_attachment_strings => [ $self->attachment_strings ]),
      __may(_attachment_files   => [ $self->attachment_files   ]),
      Date => __now(),
   );
   $message{_n_attachments} = map { ($message{$_} // [])->@* }
      qw< _attachment_strings _attachment_files >;

   __lorem_ipsum_fill(\%message, $self->config('lorem-size'))
      if $self->config('lorem');
   
   my $entity = __encode(\%message);
   my $ofh = $self->ofh;
   print {$ofh} $entity->as_string;
   return 0;
}

sub __lorem_ipsum_fill ($message, $textual_parts_size) {
   state $li = <<'END';
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat.  Duis aute irure dolor in reprehenderit in voluptate
velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
occaecat cupidatat non proident, sunt in culpa qui officia deserunt
mollit anim id est laborum.

END

   $message->{subject} //= 'Lorem ipsum dolor sit amet';
   $message->{from} //= 'sender@example.com';

   if (! exists($message->{to})) {
      $message->{to} = [ qw< to1@example.com to2@example.com > ];
      $message->{cc} //= [ qw< cc1@example.com cc2@example.com > ];
   }

   if (! exists($message->{_text_body}) && ! exists($message->{_html_body})) {
      $textual_parts_size //= 2 * length($li);
      my $size = int($textual_parts_size / 2);
      if ($size > 0) {
         my $n_repeat = 1 + int($size / length($li));
         my $part = substr(($li x $n_repeat), 0, $size);
         $message->{_text_body} = $message->{_html_body} = $part;
      }
   }

   return $message;
}

sub __now {
   state $days = [ qw< Sun Mon Tue Wed Thu Fri Sat > ];
   state $mths = [ qw< Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec> ];
   my $now = time();
   my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst)
      = my @lt = localtime($now);
   my $offset = timegm(@lt) - $now;
   my $sign = $offset >= 0 ? '+' : '-';
   $offset = -$offset if $offset < 0;
   my $offset_sec = $offset % 60;
   $offset = ($offset - $offset_sec) / 60;
   my $offset_min = $offset % 60;
   my $offset_hrs = ($offset - $offset_min) / 60;

   sprintf '%s, %d %s %4d %02d:%02d:%02d %s%02d%02d',
      $days->[$wday], $mday, $mths->[$mon], ($year + 1900),
      $hour, $min, $sec, $sign, $offset_hrs, $offset_min;
}

sub __encode ($message) {
   my $n_attach = $message->{_n_attachments};
   my $has_text_body = exists($message->{_text_body});
   my $has_html_body = exists($message->{_html_body});
   if ($has_text_body) {
      return __multipart_mixed($message) if $n_attach > 0;
      return __multipart_alternative($message) if $has_html_body;
      return __text_plain($message);
   }
   if ($has_html_body) {
      return __multipart_mixed($message) if $n_attach > 0;
      return __text_html($message);
   }
   return __multipart_mixed($message) if $n_attach > 1;
   return __generic_single($message);
}

sub __additional_headers ($list) {
   map {
      my ($key, $value) = split m{[:=]}mxs, $_, 2;
      ($key, $value);
   } $list->@*;
}

sub _hash_filter_out ($hr) {
   map { $_ => $hr->{$_} } grep { substr($_, 0, 1) ne '_' } keys($hr->%*);
}

sub __text_plain ($message, $top = 1) {
   my $text = encode('UTF-8', $message->{_text_body}, Encode::FB_CROAK);
   return MIME::Entity->build(
      $message->%*,
      Type    => 'text/plain',
      Charset => 'utf-8',
      Data    => $text,
      Top     => $top,
      Encoding => 'quoted-printable',
   );
}

sub __text_html ($message, $top = 1) {
   my $html = encode('UTF-8', $message->{_html_body}, Encode::FB_CROAK);
   return MIME::Entity->build(
      $message->%*,
      Type    => 'text/html',
      Charset => 'utf-8',
      Data    => $html,
      Top     => $top,
      Encoding => 'quoted-printable',
   );
}

sub __generic_single ($message, $top = 1) {
   my ($as, $af) = map { $message->{$_} // [] }
      qw< _attachment_strings _attachment_files >;
   my ($key, $value) = $as->@* ? (Data => $as->[0])
      : $af->@* ? (Path => $af->[0])
      : (Data => ['',
         'Content-Type' => undef,
         'Content-Disposition' => undef,
         'Content-Transfer-Encoding' => undef,
         ]);
   return MIME::Entity->build(
      Type => 'binary/octet-string',
      Encoding => 'base64',
      $message->%*,
      Top => $top,
      $key => $value->@*, # will also unroll additional params...
   );
}

sub __multipart_alternative ($message) {
   my $top = MIME::Entity->build(
      $message->%*,
      Type => 'multipart/alternative',
      Top  => 1,
   );
   $top->add_part(__text_plain({_text_body => $message->{_text_body}}, 0));
   $top->add_part(__text_html({_html_body => $message->{_html_body}}, 0));

   return $top;
}

sub __multipart_mixed ($message) {
   my $top = MIME::Entity->build(
      $message->%*,
      Type => 'multipart/mixed',
      Top  => 1,
   );
   $top->add_part(__text_plain({_text_body => $message->{_text_body}}, 0))
      if defined($message->{_text_body});
   $top->add_part(__text_html({_html_body => $message->{_html_body}}, 0))
      if defined($message->{_html_body});

   for my $keys ([ qw< Data _attachment_strings > ],
         [ qw< Path _attachment_files > ]) {
      my ($akey, $mkey) = $keys->@*;
      my $aref = $message->{$mkey} // [];
      for my $item ($aref->@*) {
         $top->attach(
            Type => 'binary/octet-stream',
            Encoding => 'base64',
            ($akey eq 'Path' ? __guess_type($item->[0]) : ()),
            $akey => $item->@*,
         );
      }
   }

   return $top;
}

sub __guess_type ($path) { warn "guessing path <$path>";
   return unless -e $path;

   state $stuff_for = {
      '7z' => 'application/x-7z-compressed',
      apng => 'image/apng',
      bmp  => 'image/bmp',
      bz2  => 'application/x-bzip2',
      bz   => 'application/x-bzip',
      css  => 'text/css',
      csv  => 'text/csv',
      doc  => 'application/msword',
      docx => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      eot  => 'application/vnd.ms-fontobject',
      gif  => 'image/gif',
      gz   => 'application/gz',
      htm  => '~html',
      html => 'text/html',
      ico  => 'image/vnd.microsoft.icon',
      ics  => 'text/calendar',
      jpeg => 'image/jpeg',
      jpg  => '~jpeg',
      md   => 'text/markdown',
      odp  => 'application/vnd.oasis.opendocument.presentation',
      ods  => 'application/vnd.oasis.opendocument.spreadsheet',
      odt  => 'application/vnd.oasis.opendocument.text',
      otf  => 'font/otf',
      pdf  => 'application/pdf',
      png  => 'image/png',
      ppt  => 'application/vnd.ms-powerpoint',
      pptx => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      rar  => 'application/vnd.rar',
      rtf  => 'application/rtf',
      svg  => 'image/svg+xml',
      tar  => 'application/x-tar',
      tiff => 'image/tiff',
      tif  => '~tiff',
      ttf  => 'font/ttf',
      txt  => 'text/plain',
      vsd  => 'application/vnd.visio',
      webp => 'image/webp',
      woff2 => 'font/woff2',
      woff => 'font/woff',
      xhtml => 'application/xhtml+xml',
      xls  => 'application/vnd.ms-excel',
      xlsx => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      xml  => 'application/xml',
      zip  => 'application/zip',
   };
   my $extension = lc($path =~ s{\A .* \. }{}rmxs);
   return unless exists($stuff_for->{$extension});
   my $stuff = $stuff_for->{$extension};
   $stuff = $stuff_for->{substr($stuff, 1)} if substr($stuff, 0, 1) eq '~';

   if (! ref($stuff)) {
      my $encoding = $stuff =~ m{\A text}imxs ? 'quoted-printable' : 'base64';
      $stuff = { Type => $stuff, Encoding => $encoding };
   }

   $stuff->%*;
}


sub attachment_strings ($self) {
   map {
      my ($attributes, $payload) = __attributes_and_payload($_);
      if (delete($attributes->{_base64})) {
         require MIME::Base64;
         $payload = MIME::Base64::decode($payload);
      }
      [ $payload, $attributes->%* ];
   } ($self->config('attach-string') // [])->@*;
}

sub attachment_files ($self) {
   map {
      my ($attributes, $payload) = __attributes_and_payload($_);
      [ $payload, $attributes->%* ];
   } ($self->config('attach') // [])->@*;
}

sub __attributes_and_payload ($value) {
   return ({}, $value) if $value !~ m{\A\s*\{}mxs;
   my ($href, $pos) = JSON::PP->new->decode_prefix($value);
   my %attrs = map { ucfirst(lc(s{-}{_}rgmxs)) => $href->{$_} } keys($href->%*);
   return (\%attrs, substr($value, $pos));
}

sub _may ($self, $key) { return __may($key, $self->config($key)) }

sub __may ($key, $value = undef) {
   return unless defined($value);
   return if ref($value) eq 'ARRAY' && scalar($value->@*) == 0;
   return ($key, $value);
}

# get text or HTML parts for the email's text
sub get_textual ($self, $format) {
   my $string = $self->config($format);
   if (! defined($string) && defined(my $path = $self->config("$format-from"))) {
      $string = path($path)->slurp_utf8;
   }
   return unless defined($string);

   # possibly expand with Template::Perlish, even an empty object '{}' can
   # trigger this
   if (my @jsons = ($self->tp_common_jsons, $self->load_tp_jsons($format))) {
      my %squash = map { $_->%* } @jsons;
      require Template::Perlish;
      $string = Template::Perlish::render($string, \%squash);
   }

   return $string;
}

sub tp_common_jsons ($self) {
   my $val = $self->{tp_common} //= $self->load_tp_jsons;
   return unless defined($val);
   return $val;
}

sub load_tp_jsons ($self, $format = undef) {
   my $cname = 'template-perlish';
   $cname = $format . '-' . $cname if defined($format);
   my $aref = $self->config($cname) or return;
   scalar($aref->@*) or return;
   return {
      map {
         my $json = m{\A\s*\{}mxs ? $_ : path($_)->slurp_raw;
         decode_json($json)->%*;
      } $aref->@*
   };
}

1;
