#!/usr/bin/env perl
package Romeo::CmdMime::CmdBlueprint;
use v5.24;
use warnings;
use experimental 'signatures';

use Path::Tiny 'path';
use JSON::PP 'decode_json';
use Ouch;
use Template::Perlish 'render';
use MIME::Entity;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< blueprint >],
   help    => 'Build MIME email messages, based on a JSON blueprint',
};

sub description ($self) {
   return <<'END';

Build email messages according to a "MIME blueprint".

    # a blueprint contains the structure of the email, vars specify
    # values for expanding templates.
    $ mime -b blueprint.json -i vars.json from:me@example.com

It renders one *blueprint* (structure & data for an email, see below)
using variables that are collected:

- from JSON files provided as `-i`/`--input` (`-` means standard input)
- from straight command line arguments, in the form `key:value` or
  `key=value` (interchangeable, no trimming)

Note that this command does *not* set standard input as a source by
default.

The output goes to standard output but there's option `-o`/`--output`:

    $ mime -b blueprint.json subj:'Hello, world!' -o greetings.eml

A *blueprint* is a JSON file with the email structure and possibly some
content as well. This command uses MIME::Entity, so everything available
there is available here too, with some additions.

This is an example:

    {
       "From": "me@foo.bar",
       "Encoding": "7bit",
       "-parts": [
          {
             "Type": "text/plain; charset=UTF-8",
             "Encoding": "quoted-printable",
             "Data": "Hey, [% foo %]!\n"
          },
          {
             "Type": "text/plain; charset=UTF-8",
             "Encoding": "quoted-printable",
             "~Data": "Hey, [% foo %]!\n"
          },
          {
             "Type": "text/plain; charset=UTF-8",
             "Encoding": "quoted-printable",
             "Data": { "slurp_sibling": "text.txt" }
          },
          {
             "Type": "text/plain; charset=UTF-8",
             "Encoding": "quoted-printable",
             "Data": { "render_sibling": "text.txt" }
          },
          {
             "Type": "text/plain; charset=UTF-8",
             "Encoding": "quoted-printable",
             "Data": {
                "render_path": {
                   "render": "[% _romeo.dir %]/text.txt"
                }
             }
          }
       ]
    }

Keys starting with a hypen will be ignored in generating output. At the
moment only `-parts` is supported, which points to an array of parts for
a *multipart* message (`multipart/mixed` by default).

Keys and values are normally taken verbatim, with the following
exceptions:

- If the key starts with a tilde (~), the tilde is stripped and both the
  key and the values are expanded as Template::Perlish templates.
- Any value that is a hash reference is assumed to be a *function call*
  to a set of pre-defined functions. The hash is supposed to have
  exactly one key that does *not* start with a hyphen, which is taken as
  the function name, and a value that might be used by the function.

The example above can be used as a blueprint in the following call:

    $ mime -b blueprint.json foo:bar

to generate this:

    Content-Type: multipart/mixed; boundary="----------=_1697054232-1587-0"
    Content-Transfer-Encoding: 7bit
    MIME-Version: 1.0
    From: me@foo.bar
    
    This is a multi-part message in MIME format...
    
    ------------=_1697054232-1587-0
    Content-Type: text/plain; charset=UTF-8
    Content-Disposition: inline
    Content-Transfer-Encoding: quoted-printable
    MIME-Version: 1.0
    
    Hey, [% foo %]!
    
    ------------=_1697054232-1587-0
    Content-Type: text/plain; charset=UTF-8
    Content-Disposition: inline
    Content-Transfer-Encoding: quoted-printable
    MIME-Version: 1.0
    
    Hey, bar!
    
    ------------=_1697054232-1587-0
    Content-Type: text/plain; charset=UTF-8
    Content-Disposition: inline
    Content-Transfer-Encoding: quoted-printable
    MIME-Version: 1.0
    
    Hey [% foo %], again!
    
    ------------=_1697054232-1587-0
    Content-Type: text/plain; charset=UTF-8
    Content-Disposition: inline
    Content-Transfer-Encoding: quoted-printable
    MIME-Version: 1.0
    
    Hey bar, again!
    
    ------------=_1697054232-1587-0
    Content-Type: text/plain; charset=UTF-8
    Content-Disposition: inline
    Content-Transfer-Encoding: quoted-printable
    MIME-Version: 1.0
    
    Hey bar, again!
    
    ------------=_1697054232-1587-0--

The first part shows the unrendered template, because it was provided as
a simple string and the associated key `Data` does not start with a
tilde.

The second part started from the same template but shows its expansion,
because the key starts with a tilde.

The third part includes the file `text.txt` from the same directory as
the blueprint (*not* the current directory) thanks to function
`slurp_sibling`. This inclusion is verbatim.

The fourth and fifth part both load the same file and expand it as a
Template::Perlish template. The former is easier because it leverages a
function that does both loading the file and rendering it; the latter
shows that functions can be composed.

The available functions are:

- `render` does the rendering on a template. This template might be a
  string or a call to another function, e.g. to "slurp" a template file.
- `render_path` loads a file from a path (absolute or relative to the
  current directory) and renders its content.
- `render_sibling` loads a file from a path (absolute or relative to the
  blueprint's directory) and renders its content.
- `slurp` loads a file from a path (absolute or relative to the current
  directory), returning it verbatim.
- `slurp_sibling` loads a path (absolute or relative to the blueprint's
  directory), returning it verbatim.

Note that the function composition capabilities allows to flexibly load
different template files depending on variables. As an example, suppose
to have this directory layout where the blueprint is stored with two
template files:

    some-blueprint/
      blueprint.json
      message-en.html
      message-it.html

and have this `blueprint.json` file:

    {
      "From": "me@foo.bar",
      "To": "you@foo.bar",
      "Encoding": "quoted-printable",
      "Type": "text/html; charset=UTF-8",
      "Data": { "render_sibling": { "render": "message-[% lang %].html" }}
    }

The inner `render` will generate the file name to load depending on the
value for variable `lang`, which will then be consumed by
`render_sibling` to read and render. This allows defining one single
blueprint that can be adapted to different situations.

END
}

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options(qw< input output >),
      {
         getopt => 'blueprint|b=s',
         help => 'blueprint path (absolute or relative to include dir)',
      },
      {
         getopt => 'include|inc=s@',
         help => 'include directories for looking for blueprints',
         default => [],
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   my $vars = $self->collect_vars;
   $vars->{_romeo} = my $blueprint = $self->get_blueprint;
   $vars->{_romeo}{trail} = [];

   my $entity = $self->build_entity($blueprint->{root}, $vars);
   $self->spew(__ensure_crlf($entity->stringify));

   return 0;
}


sub __ensure_crlf ($text) { $text =~ s{\x{0d}?\x{0a}}{\x{0d}\x{0a}}rgmxs }

sub collect_vars ($self) {
   local $/;
   my %vars = map { decode_json(readline($self->open_input($_)))->%* }
      ($self->config('input') // [])->@*;
   for my $direct ($self->residual_args) {
      my ($key, $value) = split m{[:=]}mxs, $direct, 2;
      $vars{$key} = $value;
   }
   return \%vars;
}

sub __blueprint ($path) {
   return {
      dir  => $path->parent,
      path => $path,
      root => decode_json($path->slurp_raw),
   };
}

sub get_blueprint ($self) {
   my $target = $self->config('blueprint') // '';
   ouch 400, 'no blueprint' unless length($target);

   my $path = path($target);

   return __blueprint($path) if $path->is_absolute;

   my @includes = ($self->config('include') // [])->@*;
   @includes = ('.') unless @includes;

   my $mbp = path($ENV{HOME})->child(qw< .config romeo mime blueprint >);
   push @includes, $mbp->stringify if $mbp->exists;

   for my $base (@includes) {
      my $candidate = $path->absolute($base);
      return __blueprint($candidate) if $candidate->exists;
   }

   ouch 404, "Not found: '$target'";
}

sub build_entity ($self, $def, $vars) {
   my $exp= $self->expand_templatized($def, $vars);
   my $trail = $vars->{_romeo}{trail};
   push $trail->@*, { basic => $def, expanded => $exp };
   my $retval = $def->{'-parts'} ? $self->multipart_entity($exp, $vars)
      : $self->single_entity($exp, $vars);
   pop $trail->@*;
   return $retval;
}

sub entity_base ($self, $def, $vars) {
   MIME::Entity->build(
      'X-Mailer' => undef,
      (map { $_ => $def->{$_ } } grep { ! m{\A -}mxs } $def->%* ),
   );
}

sub render_or_eval ($self, $f, $vars) {
   return render($f, $vars) unless ref($f);
   return [ map { $self->render_or_eval($_, $vars) } $f->@* ]
      if ref($f) eq 'ARRAY';
   my ($name) = grep { ! m{\A -}mxs } keys $f->%*;
   my $method = $self->can('_eval_' . $name)
      or ouch 404, "no such function '$name'";
   return $self->$method($f, $vars);
}

sub _eval_slurp ($self, $f, $vars) {
   my $path = $self->render_or_eval($f->{slurp}, $vars);
   ouch 404, 'path not found' unless -e $path;
   return path($path)->slurp_raw;
}

sub _eval_slurp_sibling ($self, $f, $vars) {
   my $rel = $self->render_or_eval($f->{slurp_sibling}, $vars);
   my $path = path($rel)->absolute($vars->{_romeo}{dir});
   return $path->slurp_raw;
}

sub _eval_render ($self, $f, $vars) {
   my $template = $self->render_or_eval($f->{render}, $vars);
   return $self->render_or_eval($template // '', $vars);
}

sub _eval_render_path ($self, $f, $vars) {
   my $path = $f->{render_path};
   my $template = $self->_eval_slurp({slurp => $f->{render_path}}, $vars);
   return $self->render_or_eval($template // '', $vars);
}

sub _eval_render_sibling ($self, $f, $vars) {
   my $rel = $self->render_or_eval($f->{render_sibling}, $vars);
   my $path = path($rel)->absolute($vars->{_romeo}{dir})->stringify;
   return $self->_eval_render_path({render_path => $path}, $vars);
}

sub expand_templatized ($self, $def, $vars) {
   my %def = $def->%*;

   # handle group of stuff that's been "templatized"
   if (my $templatized = delete $def{'-templatized'}) {
      for my $tkey (keys $templatized->%*) {
         my $rkey = render($tkey, $vars);
         $def{$rkey} = $self->render_or_eval($templatized->{$tkey}, $vars);
      }
   }

   # handle items that have been marked for templatization
   for my $key (keys %def) {
      my ($tkey) = $key =~ m{\A [~] (.*)}mxs or next;
      my $rkey = render($tkey, $vars);
      $def{$rkey} = $self->render_or_eval(delete($def{$key}), $vars);
   }

   # handle values that need evaluation (restrict to HASH only)
   for my $value (values %def) {
      $value = $self->render_or_eval($value, $vars)
         if ref($value) eq 'HASH';
   }

   return \%def;
}

sub multipart_entity ($self, $def, $vars) {
   my %default = ( Type => 'multipart/mixed', );
   my $entity = $self->entity_base({ %default, $def->%* }, $vars);
   my $parts = delete($def->{'-parts'});
   $entity->add_part($self->build_entity($_, $vars)) for $parts->@*;
   return $entity;
}

sub get_template ($self, $def) {
   return $def->{'-template'} if defined($def->{'-template'});
   return unless defined($def->{'-template-path'});
   return path($def->{'-template-path'})->slurp_raw
}

sub single_entity ($self, $def, $vars) {
   my %default;
   my %addon;
   if (defined(my $template = $self->get_template($def))) {
      $addon{Data} = render($template, $vars, { binmode => ':raw' });
   }
   return $self->entity_base({ $def->%*, %addon }, $vars);
}

1;
