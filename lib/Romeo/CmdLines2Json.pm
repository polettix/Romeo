#!/usr/bin/env perl
package Romeo::CmdLines2Json;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch;
use English;
use utf8;

use Romeo::Util::Transform
  qw< encode_json encode_json_compact url_decode >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< lines2json >],
   help    => 'Turn a list of lines into a JSON file',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt  => 'chomp!',
         help    => 'remove trailing newline from input lines',
         default => 1,
      },
      {
         getopt  => 'compact|shrink!',
         help    => 'print out compact JSON (not pretty)',
         default => 0,
      },
      {
         getopt  => 'flatten|flat|F!',
         help    => 'flatten all inputs into a single array',
         default => 1,
      },
      {
         getopt  => 'key|k=s',
         help    => 'key to associate to each line produces objects',
         default => 'line',
      },
      {
         getopt  => 'plain|p!',
         help    => 'generate a plain array of strings (default is AoH)',
         default => 0,
      },
      {
         getopt  => 'trim|t!',
         help    => 'trim leading and trailing whitespace from input',
         default => 0,
      },
      {
         getopt  => 'urlencoded|url-encoded|u!',
         help    => 'key and input lines are URL-encoded',
         default => 0,
      },
   );
} ## end sub options

sub execute ($self) {
   my $chomp       = $self->config('chomp');
   my $trim        = $self->config('trim');
   my $flatten     = $self->config('flatten');
   my $plain       = $self->config('plain');
   my $url_encoded = $self->config('urlencoded');

   my $key;
   if (!$plain) {
      $key = $self->config('key');
      $key = url_decode($key) if $url_encoded;
   }

   my @retval;
   while (my $ifh = $self->next_ifh) {
      my $target = $flatten ? \@retval : ($retval[@retval] = []);
      while (<$ifh>) {
         chomp                if $chomp;
         s{\A\s+|\s+\z}{}gmxs if $trim;
         $_ = url_decode($_)  if $url_encoded;
         push $target->@*, $plain ? $_ : {$key => $_};
      } ## end while (<$ifh>)
   } ## end while (my $ifh = $self->next_ifh)

   my $encoder =
     $self->config('compact') ? \&encode_json_compact : \&encode_json;
   $self->spew($encoder->(\@retval));

   return 0;
} ## end sub execute

1;
