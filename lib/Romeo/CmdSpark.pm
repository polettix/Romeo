package Romeo::CmdSpark;
use v5.24;
use Carp;
use English qw< -no_match_vars >;
use experimental qw< signatures >;
use Ouch;
use Term::ANSIColor 5.01 qw< colored colorvalid >;
use Romeo::Util::IO qw< slurp_raw >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< spark >],
   help    => 'little histograms on the command line',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
   );
}

sub execute ($self) {
   my $args = join(' ', $self->residual_args) // '';
   my $inputs = $self->config('input') // [];
   unshift($inputs->@*, \$args) if length($args);
   $self->set_config(input => $inputs);
   $self->filter_inputs_to_output;
}

sub filter ($self, $ifh, $ofh, $idx, $n) {
   local $/;
   my @numbers = split m{\s+}mxs, <$ifh>;
   say {$ofh} __sparkline_string(@numbers);
}

sub __sparkline_string (@n) {
   state $sparks = [map { chr($_) } 0x2581 .. 0x2588];
   state $n_sparks = $sparks->@*;

   my ($min, $max) = @n[0, 0];
   for my $i (@n) {
      if ($i < $min)    { $min = $i }
      elsif ($i > $max) { $max = $i }
   }

   my @chars;
   if (my $delta = ($max - $min)) {
      my $scale = ($n_sparks - 1) / $delta;
      @chars = map { $sparks->[($_ - $min) * $scale] } @n;
   }
   else {
      @chars = ($sparks->[$n_sparks / 2]) x @n;
   }

   return join '', @chars;
}

1;
