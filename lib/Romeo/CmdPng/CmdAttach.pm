#!/usr/bin/env perl
package Romeo::CmdPng::CmdAttach;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use Ouch;
use Fcntl 'SEEK_CUR';
use English;
use File::Basename 'basename';

use constant PORTABLE_ATTACHMENT_KEYWORD => "polettix.it:Attachment";

use Romeo::Util::IO qw< open_fh slurp_raw :syswrap >;
use Romeo::Util::PNG qw< png_crc png_parse_atCh png_parse_iTXt >;
use Romeo::Util::Transform qw< encode deflate :base64 :json >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< attach a add atch >],
   help => 'attach with atCh (default) or iTXt',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options(qw< single-input output >),
      {
         getopt => 'atch|a',
         help => 'use atCh instead of iTXt',
      },
      {
         getopt => 'base64|b64|b!',
         help => 'use Base64 for encoding binary data',
         default => 1,
      },
      {
         getopt => 'buflen|B=i',
         help => 'length of buffer for copying data from input to output',
         default => DEFAULT_BUFFER_SIZE,
         environment => 'ROMEO_BUFLEN',
      },
      {
         getopt => 'direct|d=s@',
         help   => 'definition for stuff directly on command line',
         default => [],
      },
      {
         getopt => 'extract|x',
         help => 'extract files in the current directory',
      },
      {
         getopt => 'stdout|O',
         help => 'extract to standard output',
      },
      {
         getopt => 'renamed|r=s@',
         help   => 'rename file as name:path',
         default => [],
      },
      {
         getopt => 'verbose|v',
         help => 'print names of files as they are extracted',
      },
   );
}

sub execute ($self) {
   $self->set_config(($_ . '_encoding') => ':raw') for qw< input output >;
   return $self->extract if $self->config('extract');
   return $self->add_meta;
}

sub extract ($self) {
   my %requested = map { $_ => 1 } $self->residual_args;
   my $any_file = !scalar(keys(%requested));
   my $name_fh = $self->config('verbose') ? $self->ofh : undef;
   my $data_fh = $self->config('stdout') ? $self->ofh : undef;

   my $ifh = $self->ifh;

   # drop PNG header
   my $header = sysread_n($ifh, 8);

   my %missing = %requested; # will be removing from it as we go
   my %extracted;
   while ('necessary') {
      my $length = unpack('N', sysread_n($ifh, 4));
      my $type   = sysread_n($ifh, 4);
      last if $type eq 'IEND';
      if ($type !~ m{\A (?: atCh | iTXt ) \z}mxs) { # skip chunk
         sysseek($ifh, $length + 4, SEEK_CUR);
         next;
      }

      my $dataref = sysread_n($ifh, $length, 1);
      my $crc = sysread_n($ifh, 4);
      ouch 400, 'invalid CRC' unless $crc eq png_crc($type, $dataref);

      my $method = $self->can('read_' . $type) or ouch 500, 'wtf?!?';
      my ($file, $contentref) = $self->$method($dataref)
         or next;
      next unless $any_file || $requested{$file};

      delete $missing{$file};

      my $name = $self->proper_name($file);
      warn "writing file '$name' (from '$file') again in this session"
         if $extracted{$file}++ && ! $data_fh;

      # verbose mode?
      syswrite_all($name_fh, $name, "\n") if $name_fh;

      my $fh = $data_fh ? $data_fh : open_fh($name, '>', ':raw');
      syswrite_all($fh, $contentref);
      close($fh) or ouch 500, "close(): $OS_ERROR";
   }

   return 0;
}

sub add_meta ($self) {
   my $ifh = $self->ifh;
   my $ofh = $self->ofh;
   my $buflen = $self->config('buflen');

   # copy "most" of the file from input to output
   syscopy_n($ifh, $ofh, 8, 8); # copy PNG header
   while ('necessary') {
      my $binary_length = sysread_n($ifh, 4);
      my $chunk_type    = sysread_n($ifh, 4);
      if ($chunk_type eq 'IEND') {
         sysread_n($ifh, 4); # advance to end of file
         last;
      }

      # copy length and type as read
      syswrite_all($ofh, $binary_length, $chunk_type);

      # copy chunk data and crc (4 bytes)
      my $length = unpack('N', $binary_length);
      syscopy_n($ifh, $ofh, $length + 4, $buflen);
   }

   # add chunks
   $self->set_config(atch => 1) if $self->call_name eq 'atch';
   my $method_name = $self->config('atch') ? 'add_atCh' : 'add_iTXt';
   my $method = $self->can($method_name) or ouch 500, 'unimplemented';

   for my $path ($self->residual_args) {
      ouch 400, "cannot read input file <$path>" unless -r $path;
      $self->$method($path, slurp_raw($path));
   }

   for my $part ($self->config('renamed')->@*) {
      my ($name, $path) = split m{:}mxs, $part, 2;
      ouch 400, "cannot read input file <$path>" unless -r $path;
      $self->$method($name, slurp_raw($path));
   }

   for my $part ($self->config('direct')->@*) {
      my ($name, $data) = split m{:}mxs, $part, 2;
      $self->$method($name, $data);
   }

   # put the IEND, as a fixed post-amble
   syswrite_all($ofh, "\0\0\0\0IEND\x{ae}\x{42}\x{60}\x{82}");

   return 0;
}

sub add_chunk ($self, $type, $dataref) {
   my $bin_length = pack('N', length($$dataref));
   my $crc = png_crc($type, $dataref);
   syswrite_all($self->ofh, $bin_length, $type, $dataref, $crc);
   return;
}

sub add_atCh ($self, $raw_name, $contents) {
   my $cref = deflate(\$contents, 1);
   my $data = join '',
      $self->proper_name($raw_name), # file name
      "\0",                          # null-termination for file name
      "\1",                          # compression flag
      $$cref;                        # compressed data
   $self->add_chunk(atCh => \$data);
   return;
}

sub add_iTXt ($self, $raw_name, $contents) {
   my $b64 = $self->config('base64');
   $contents = encode_json(
      {
         filename => $self->proper_name($raw_name),
         bytes    => ($b64 ? encode_base64($contents) : $contents),
         base64   => ($b64 ? 1 : 0),
      }
   );
   my $cref = deflate(\$contents, 1);

   my $data = join '',
      PORTABLE_ATTACHMENT_KEYWORD, # keyword as we like it
      "\0",                        # keyword null-termination
      "\1\0",                      # compression, PNG default (deflate)
      "\0\0",                      # no language tag & translated keyword
      $$cref;                      # data

   $self->add_chunk(iTXt => \$data);
   return;
}

sub proper_name ($self, $input, $limit = -1) {
   my $name = basename($input);
   $name =~ s{[\x{00}-\x{1f}\x{7f}/\\:]+}{}gmxs;
   $name =~ s{\A\.+}{}mxs;         # no hidden stuff
   $name =~ s{\A\s+|\s+\z}{}gmxs;  # trim
   $name = substr($name, 0, $limit) if $limit > 0;
   return encode('UTF-8', $name);
}

sub read_atCh ($self, $dataref) {
   my $parsed = png_parse_atCh($dataref);
   return ($parsed->{filename}, \($parsed->{content}{bytes}));
}

sub read_iTXt ($self, $dataref) {
   my $parsed = png_parse_iTXt($dataref);
   return if $parsed->{keyword} ne PORTABLE_ATTACHMENT_KEYWORD;
   my $content_bytes = encode('UTF-8', $parsed->{content}{characters});
   my $struct = decode_json($content_bytes);
   my $filename = $struct->{filename};
   $dataref = \($struct->{bytes});
   $dataref = decode_base64($dataref) if $struct->{base64};
   return ($filename, $dataref);
}

sub description ($self) {
   return <<'END';

Implement pngattach (https://nullprogram.com/blog/2021/12/31/) and then
some.

   # defaults to treating command-line args as filenames
   # uses "portable" ways to attach stuff (iTXt + base64 encoding)
   $ cat somefile.png | romeo png-meta attach somefile.txt > with-meta.png

   # set a filename explictly for attachment
   $ romeo <in.png >out.png pngmeta a -r hey.txt:somefile.txt

   # attach data from command line directly
   $ romeo <in.png >out.png pngmeta a -d foo.txt:'bar baz'

   # live risky! Do not use base-64 encoding (also: --no-base64)
   $ romeo <in.png >out.png pngmeta a foo.txt --no-b64

   # use atCh instead of iTXt
   $ romeo <in.png >out.png pngmeta a --atch foo.txt
   $ romeo <in.png >out.png pngmeta atch foo.txt


END
}
