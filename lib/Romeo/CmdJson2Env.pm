#!/usr/bin/env perl
package Romeo::CmdJson2Env;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch;
use English;
use utf8;

use Template::Perlish qw< crumble traverse >;

use Romeo::Util::Transform qw< decode_json encode_json_compact >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< json2env j2e >],
   help    => 'Turn arguments into a JSON (file)',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options(
         qw< single_input input_encoding output output_encoding >),
      {
         getopt => 'json|j=s',
         help   => 'the JSON string to read from',
      },
      {
         getopt => 'prefix=s',
         default => undef,
         help => 'prefix for environment variables',
      },
      {
         getopt => 'separator=s',
         default => '_',
         help => 'key steps separator for environment variables',
      },
      {
         getopt => 'skip-undef!',
         default => 1,
         help => 'skip generating environment variables for undefined values',
      },
      {
         getopt => 'strict!',
         help => 'exit with an error if variable names are not valid',
         default => 1,
      },
      {
         getopt => 'unset|unset-variable|u=s',
         help => 'generate a "unset" environment variable too',
      },
   );
} ## end sub options

sub commit ($self) {
   return if defined($self->config('json'));
   $self->set_config(json => $self->slurp);
   return $self;
}

sub __recursive_traversal ($data, $cb, $skip, $path = []) {
   my $dref = ref($data);
   if (! $dref) {
      return if $skip && ! defined($data);
      $cb->($path, $data);
   }
   elsif ($dref eq 'HASH') {
      for my $k (keys($data->%*)) {
         __recursive_traversal($data->{$k}, $cb, $skip, [ $path->@*, $k ]);
      }

   }
   else {
      for my $i (0 .. $data->$#*) {
         __recursive_traversal($data->[$i], $cb, $skip, [ $path->@*, $i ]);
      }
   }
}

sub execute ($self) {
   return 0 unless $self->handle_help;
 
   my $data = decode_json($self->config('json'));
   my $sep = $self->config('separator');
   my $pfx = $self->config('prefix');
   my $err_throw = $self->config('strict');
   my $ofh = $self->ofh;
   my @vars;
   my $callback = sub ($key_steps, $value) {
      my $var = join($sep, $key_steps->@*);
      $var = $pfx . $sep . $var if defined($pfx);
      $var = $var;
      if ($var !~ m{\A [a-zA-Z_] \w* \z}mxs) {
         my $msg = "'$var' is not a valid environment variable name";
         die "$msg, aborting as requested" if $err_throw;
         warn "$msg, skipping\n";
         return;
      }
      push @vars, $var;

      $value =~ s{'}{'"'"'}gmxs;
      say {$ofh} "$var='$value'";
   };

   if (my @keys = $self->residual_args) {
      for my $key (@keys) {
         my $key_steps = crumble($key);
         $callback->($key_steps, traverse($data, $key_steps));
      }
   }
   else {
      __recursive_traversal($data, $callback, $self->config('skip-undef'));
   }

   if (@vars && defined(my $var = $self->config('unset'))) {
      local $" = ' ';
      $var = $var;
      say {$ofh} "$var='@vars $var'";
   }

   return 0;
}

1;
