#!/usr/bin/env perl
package Romeo::CmdJson2Csv;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch;
use English;
use utf8;

use Romeo::Util::Transform qw< decode_json decode_json_array url_decode >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< json2csv >],
   help    => 'Turn a JSON into a CSV file',
};

use Text::CSV_PP qw< csv >;

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt => 'columns|columns-first|C=s@',
         help => 'columns to always include, at the beginning',
      },
      {
         getopt => 'columns-only|columns-first-only|O!',
         help => 'only include the columns-first',
      },
      {
         getopt      => 'eol|e=s',
         help => 'end-of-record character',
         environment => 'ROMEO_JSON2CSV_EOL',
      },
      {
         getopt      => 'flatten|flat|F!',
         help => 'flatten records from all inputs (array inputs)',
      },
      {
         getopt => 'json|j=s',
         help => '(small) JSON as direct command-line argument',
      },
      {
         getopt      => 'sep|separator|sep_char|sep-char|s=s',
         help  => 'fields-separator character',
         environment => 'ROMEO_JSON2CSV_SEP',
         default     => ';',
      },
      {
         getopt => 'slurp|S!',
         help => 'slurp mode - sequences of hashes are read inot an array',
         environment => 'ROMEO_JSON2CSV_SLURP',
         default => 1,
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   # adjust parameters
   for my $key (qw< eol sep >) {
      my $value = $self->config($key) // next;
      $self->{$key} = url_decode($value);
   }

   $self->set_config(input => [$self->direct_json])
     if defined($self->config('json'));

   my $data = $self->parse_inputs;
   $data = [ map { (ref($_) eq 'ARRAY') ? $_->@* : $_ } $data->@* ]
     if ($data->@* == 1) || $self->config('flatten');

   my @columns = $self->get_columns($data);

   my $ofh = $self->ofh;
   my @csv_args = (
      headers  => \@columns,
      in       => $data,
      out      => $ofh,
      sep_char => $self->{sep},
   );
   push @csv_args, eol => $self->{eol} if defined($self->{eol});
   csv(@csv_args);

   close $ofh or ouch 500, "close(): $OS_ERROR";

   return 0;
}

sub get_columns ($self, $data) {
   my @columns = map { map { url_decode($_) } split m{[,;]}mxs }
     ($self->config('columns') // [])->@*;
   if (! $self->config('columns-only')) {
      my %present = map { $_ => 1 } @columns;
      my @additional;
      for my $record ($data->@*) {
         ouch 400, 'invalid input (not a hash)' if ref($record) ne 'HASH';
         for my $key (keys $record->%*) {
            next if $present{$key}++;
            push @additional, $key;
         }
      }
      @columns = (@columns, sort { $a cmp $b } @additional);
   }
   return @columns;
}

sub parse_inputs ($self) {
   my @data;
   $self->set_config(input_encoding => ':raw');
   my $dec = $self->config('slurp') ? \&decode_json_array : \&decode_json;
   while (defined(my $input = $self->next_ifh)) {
      local $/;
      my $json = <$input>;
      push @data, $dec->($json);
   }
   return \@data;
}

sub direct_json ($self) {
   my $json = $self->config('json') // ouch 500, 'WTF?!?';
   return \$json;
}

sub description ($self) {
   return <<'END'

Turn a JSON into a CSV.

All columns are considered, sorted lexicographically.

It's possible to put colums at the beginning, in a specific order, with
option "-C"/"--columns"/"--columns-first", repeatable multiple times.

It's possible to restrict to the provided columns only with option
"-O"/"--columns-only"/"--columns-first-only".

The separator defaults to ";" and can be set with "-s"/"--sep" and their
aliases.

The end-of-line defaults to a newlines and can be set with option
"-e"/"--eol".

Both the separator and the end-of-line can be provided as percent-encoded
strings on the command line.

Data coming from multiple input arrays can be "flattened" with
"-f"/"--flatten". This allows merging arrays before outputting them as CSV.

END
}

1;
