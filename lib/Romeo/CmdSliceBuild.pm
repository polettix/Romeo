#!/usr/bin/env perl
package Romeo::CmdSliceBuild;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;

use Romeo::Util::Transform 'shell_quote';

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO Romeo::Role::Slice >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< slice-build >],
   help    => 'Slice input data and keep definitions for tasty samples',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      $self->slice_options,
      {
         getopt => 'cmdline|c',
         help => 'print list of definitions suitable for command line',
         default => 0,
      },
      {
         getopt => 'compact|C',
         help => 'with --selection, compactify definitions as strings',
         default => 0,
      },
   );
} ## end sub options

sub execute ($self) {
   return 0 unless $self->handle_help;

   my ($definitions, $input) = $self->choose;

   my @final = $definitions->@*;
   @final = map { __compactify($_) } @final
      if $self->config('compact') || $self->config('cmdline');

   my $ofh = $self->ofh;
   if ($self->config('cmdline')) {
      my $list = join ' ', map { __shell_quote($_) } @final;
      print {$ofh} $list, "\n";
   }
   else {
      print {$ofh} $self->_json(\@final);
   }

   return 0;
}

sub __compactify ($definition) {
   my ($dst, $src) = map {
      join '.', map {
         length($_) == 0 ? q{""} # empty string
            : m{\A \w+ \z}mxs ? $_          # token is good
            : q{"} . (s{[\\"]}{\\$1}rgmxs) . q{"};
      } $_->@*;
   } $_->@{qw< dst src >};
   return $dst eq $src ? $src : "$dst:$src";
}

sub description ($self) {
   return <<'END';

This command allows "slicing" input data to only retain a subset, possibly
rearranged.

   $ cat data.json
   {
      "foo": "bar",
      "array": ["one", "two", "three" ],
      "hash": {
         "sub_hash": {
            "baz": "this is important",
            "galook": "this is not"
         },
         "other-stuff": [ 1, 2, 3 ]
      }
   }

   # estract only neeeded data
   $ script/romeo slice -i tmp/data.json foo hash.sub_hash.baz
   {
      "foo" : "bar",
      "hash" : {
         "sub_hash" : {
            "baz" : "this is important"
         }
      }
   }

   # move hash.sub_hash.baz up and rename to bar
   $ script/romeo slice -i tmp/data.json foo bar:hash.sub_hash.baz
   {
      "bar" : "this is important",
      "foo" : "bar"
   }

   # emerge to top level and keep last name part with "@"
   $ script/romeo slice -i tmp/data.json foo @:hash.sub_hash.baz
   {
      "baz" : "this is important",
      "foo" : "bar"
   }

END
} ## end sub description

1;
