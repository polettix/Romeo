#!/usr/bin/env perl
package Romeo::CmdFrame;
use v5.24;
use warnings;
use experimental 'signatures';

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< frame >],
   help    => 'Put a frame around the input',
};

sub description ($self) {
   return <<'END';

Draw a frame around the input. Most options allow setting the character for
the specific part of the frame.

END
}

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt  => 'bottom|b=s',
         help    => 'stuff to add to the bottom',
      },
      {
         getopt  => 'bottom_left|bottom-left|bl=s',
         help    => 'stuff to add to the bottom-left',
      },
      {
         getopt  => 'bottom_right|bottom-right|br=s',
         help    => 'stuff to add to the bottom-right',
      },
      {
         getopt  => 'left|l=s',
         help    => 'stuff to add to the left',
      },
      {
         getopt  => 'right|r=s',
         help    => 'stuff to add to the right',
      },
      {
         getopt  => 'top|t=s',
         help    => 'stuff to add to the top',
      },
      {
         getopt  => 'top_left|top-left|tl=s',
         help    => 'stuff to add to the top-left',
      },
      {
         getopt  => 'top_right|top-right|tr=s',
         help    => 'stuff to add to the top-right',
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   for my $lr (qw< left right >) {
      my $v = $self->{$lr} = $self->config($lr) // '';
      for my $tb (qw< top bottom >) {
         my $key = $tb . '_' . $lr;
         $self->{$key} = $self->config($key) // $v;
      }
   }
   return $self->filter_inputs_to_output;
}

sub parse_spec ($self, $spec) {
   return (0, '') unless defined($spec);
   my ($n, $string) = $spec =~ m{\A (?:([1-9]\d*):)? (.*) \z}mxs;
   $n = 1 unless length($n // '');
   return ($n, $string);
}

sub frame_line ($self, $ofh, $side, $len) {
   my ($n, $s) = $self->parse_spec($self->config($side));
   return (0, '') unless length $s;
   my $frame = substr(($s x int(1 + $len / length($s))), 0, $len);
   my ($left, $right) = map {
      my $string = '';
      my $s = $self->{$side . '_' . $_};
      if (my $slen = length($s)) {
         my $rlen = length($self->{$_});
         $string = substr(($s x int(1 + $rlen / $slen)), 0, $rlen);
      }
      $string;
   } qw< left right >;
   say {$ofh} $left, $frame, $right for 1 .. $n;
   return;
}

sub filter ($self, $ifh, $ofh, $idx, $n) {
   my ($left, $right) = map { $self->{$_} } qw< left right >;

   if (! defined($self->{length})) { # looking for the first line
      defined(my $first_line = <$ifh>) or return;
      chomp($first_line);
      my $len = length($first_line);

      $self->frame_line($ofh, top => $len);

      say {$ofh} $left, $first_line, $right;
      $self->{length} = $len; # mark as done
   }

   while (<$ifh>) {
      chomp;
      say {$ofh} $left, $_, $right;
   }

   # if this was the last one, emit the bottom side of the frame
   $self->frame_line($ofh, bottom => $self->{length})
     if $idx + 1 == $n;
}

1;
