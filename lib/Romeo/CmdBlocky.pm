#!/usr/bin/env perl
package Romeo::CmdBlocky;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< blocky blk >],
   help    => 'Turn stuff into Unicode blocky stuff',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt  => 'reverse|r!',
         default => 0,
         help    => 'reverse the coloring',
      },
      {
         getopt  => 'trim|t!',
         default => 0,
         help    => 'trim trailing spaces from output',
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;
   return $self->filter_inputs_to_output;
}

sub filter ($self, $ifh, $ofh, @rest) {
   state $direct_char_for = [
      ' ',                       # 0
      "\N{LOWER HALF BLOCK}",    # 1
      "\N{UPPER HALF BLOCK}",    # 2
      "\N{FULL BLOCK}",          # 3
   ];

   my @char_for = $direct_char_for->@*;
   @char_for = reverse @char_for if $self->config('reverse');

   my $trim = $self->config('trim');
   while (defined(my $upper = <$ifh>)) {
      chomp($upper);
      chomp(my $lower = <$ifh> // '');

      my $ulen = length($upper);
      my $llen = length($lower);
      my $len = $ulen < $llen ? $llen : $ulen;

      my $output = join '', map {
         my $u = $_ < $ulen ? substr($upper, $_, 1) : ' ';
         my $l = $_ < $llen ? substr($lower, $_, 1) : ' ';
         $_ = ($_ eq ' ' ? 0 : 1) for ($u, $l);
         $char_for[$u * 2 + $l];
      } 0 .. $len - 1;

      $output =~ s{\s+\z}{}mxs if $trim;

      say {$ofh} $output;
   } ## end while ($first_row_id <= $encoded...)

   return $self;
}

sub description ($self) {
   return <<'END'

This command transforms the input into a "blocky" representation, where each
pair of lines is compacted using Unicode characters for blocks
representations. This allows addressing the usual 2x1 aspect ratio in
terminal fonts (i.e. height is usually double of width).

The character space " " (ASCII hex 0x20) is interpreted as an empty slot;
anything else is rendered as a full block.

As an example, the following ASCII-art lines:

   *   *
   * * *
   ** **
    ***

is rendered as follows:

   █ ▄ █
   ▀█▄█▀

It is also possible to reverse the rendering using option "--reverse"/"-r";
this can be handy when using a terminal with light foreground over dark
background, especially when rendering QR codes.

Last, option "--trim"/"-t" allows removing *trailing* spaces from the
output; leading spaces will be preserved so that shapes will be preserved
too.

Example:

   printf '*   *\n* * *\n** **\n *** \n' | romeo blocky

END
}

1;
