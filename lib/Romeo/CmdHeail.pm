#!/usr/bin/env perl
package Romeo::CmdHeail;
use v5.24;
use warnings;
use experimental 'signatures';

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< heail >],
   help    => 'head and tail a file',
};

sub description ($self) {
   return <<'END';

Print the head and the tail of a file.

   $ heail somefile

   this is the first line in the file
   this is the second
   and this is the third
   ---
   in the tail here, line above is the separator
   now something
   this is the last line in the file

END
}

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt  => 'head|h=i',
         help    => 'number of lines to print out from head',
      },
      {
         getopt  => 'lines|l|n=i',
         help    => 'number of lines to print out from head/tail',
         default => 3,
      },
      {
         getopt => 'separator|s=s',
         help => 'separator line(s) between head and tail',
         default => '...',
      },
      {
         getopt  => 'tail|t=i',
         help    => 'number of lines to print out',
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   $self->{head} = $self->config('head') // $self->config('lines');
   $self->{tail} = $self->config('tail') // $self->config('lines');
   $self->{separator} = $self->config('separator');
   $self->{separator} .= "\n" unless $self->{separator} =~ m{\n}mxs;
   $self->{buffer} = [];
   $self->{printed} = 0;
   return $self->filter_inputs_to_output;
}

sub filter ($self, $ifh, $ofh, $idx, $n) {
   my $buffer = $self->{buffer};
   while (<$ifh>) {
      if ($self->{n_lines}++ < $self->{head}) {
         print {$ofh} $_;
         next;
      }
      shift $buffer->@* if $buffer->@* == $self->{tail};
      push $buffer->@*, $_;
   }

   # if this was the last one, emit the bottom side
   if ($idx + 1 == $n) {
      unshift $buffer->@*, $self->{separator}
         if $self->{n_lines} > $self->{head} + $self->{tail};
      print {$ofh} $_ for $buffer->@*;
   }
}

1;
