#!/usr/bin/env perl
package Romeo::CmdHmac;
use v5.24;
use warnings;
use experimental qw< bitwise signatures >;
use Digest       ();
use Ouch;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< hmac >],
   help    => 'calculate HMAC from several hashing algorithms',
};

sub description ($self) {
   return <<'END';

Calculate a HMAC:

- provide key with option "-k"/"--key"
- select the hashing algorithm with "-a"/"-h"/"--hash"/"--algorithm".
- provide the data directly with option "-d"/"--data", or provide it
  through "-i"/"--input" (defaults to reading from standard input,
  multiple files will be merged before calculating the HMAC).

The output format defaults to hexadecimal; it's possible to select a
different one with "-f"/"--format".

END
}

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options(qw< input input_encoding output >),
      {
         getopt  => 'algorithm|hash|a|h=s',
         help    => 'hashing (MD5/SHA(1/224/256/384/512)',
         default => 'sha512',
      },
      {
         getopt  => 'format|f=s',
         help    => 'format (binary/hex/base64)',
         default => 'hex',
      },
      {
         getopt => 'key|k=s',
         help   => 'key for calculating the HMAC',
      },
      {
         getopt => 'data|d=s',
         help   => 'data to calculate the HMAC (defaults input slurping)',
      },
   );
} ## end sub options

sub key ($self) {
   my $key = $self->config('key');
   my $kl  = length($key);
   my $bl  = $self->{bl};
   $key = Digest->new($self->{alg})->add($key)->digest if $kl > $bl;
   $key .= "\x{00}" x ($bl - $kl) if $kl < $bl;
   return $key;
} ## end sub key

sub execute ($self) {
   return 0 unless $self->handle_help;

   # do validation here...
   my $format = lc($self->config('format'));
   $format = 'binary' if $format eq 'bin';
   $format =~ m{\A(?: binary | hex | base64  )\z}mxs
     or ouch 400, "invalid format '$format'";
   $self->{format} = $format;

   # get inputs from parameter if so requested
   if (defined(my $data = $self->config('data'))) {
      $self->set_config(input => [\$data]);
   }

   # normalize algorithm's name
   my $alg = uc($self->config('algorithm'));
   $alg =~ s{\A SHA(\d+) \z}{SHA-$1}imxs;
   $self->{alg} = $alg;

   # calculate block length
   state $bl_for = {
      MD5          => 64,
      'SHA-1'      => 64,
      'SHA-224'    => 64,
      'SHA-256'    => 64,
      'SHA-384'    => 128,
      'SHA-512'    => 128,
   };
   my $bl = $self->{bl} = $bl_for->{$alg};

   # derive key
   my $key = $self->{key} = $self->key;

   # initialize "inner key" and "outer key"
   $self->@{qw< ikey okey >} =
     map { $key ^. ($_ x $bl); } ("\x{36}", "\x{5c}");

   # initialize digest calculators
   $self->@{qw< idigest odigest >} =
     map { Digest->new($alg)->add($_); } $self->@{qw< ikey okey >};

   $self->filter_inputs_to_output;

   return 0;
} ## end sub execute

sub filter ($self, $ifh, $ofh, $idx, $n) {
   while (<$ifh>) { $self->{idigest}->add($_); $self->{buffer} .= $_ }

   # return unless this the last one
   return unless $idx + 1 == $n;

   # do the magic
   my $od     = $self->{odigest}->add($self->{idigest}->digest);
   my $format = $self->{format};
   my $value =
       $format eq 'binary' ? $od->digest
     : $format eq 'hex'    ? $od->hexdigest
     : $format eq 'base64' ? $od->b64digest
     :                       undef;
   binmode $ofh, ':raw';
   say {$ofh} $value;
} ## end sub filter

1;
