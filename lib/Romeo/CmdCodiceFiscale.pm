#!/usr/bin/env perl
package Romeo::CmdCodiceFiscale;
use v5.24;
use warnings;
use experimental 'signatures';

use Validate::CodiceFiscale 'decode_cf';

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< cf codicefiscale codice-fiscale >],
   help    => 'validate Italian "Codice Fiscale"',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt => 'cf=s',
         help => 'input directly as command-line argument',
      },
      {
         getopt => 'compact|c!',
         help => 'remove any whitespace',
      },
      {
         getopt => 'decode|d!',
         help => 'include decoding in output',
      },
      {
         getopt => 'format|f=s',
         help => 'format (json or yaml)',
         default => 'json',
      },
      {
         getopt => 'full-decode|D!',
         help => 'include full decoding (with portions) in output',
      },
      {
         getopt => 'stringify|s',
         help => 'stringify errors',
      },
      {
         getopt => 'trim|t!',
         help => 'trim leading and trailing whitespace',
      }
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   my $format = lc($self->config('format'));
   die "invalid format\n" unless $format =~ m{\A(?: json | yaml )\z}mxs;

   if (my @directs = $self->residual_args) {
      my $direct = join "\n", @directs;
      push $self->config('input')->@*, \$direct;
   }

   my $trim = $self->config('trim');
   my $compact = $self->config('compact');
   my $stringify = $self->config('stringify');
   my $decode = $self->config('full-decode') ? 'full'
      : $self->config('decode') ? 'simple' : 'none';

   my @tests;
   while (defined(my $ifh = $self->next_ifh)) {
      while (defined(my $cf = <$ifh>)) {

         if ($compact) { $cf =~ s{\s+}{}gmxs }
         elsif ($trim) { $cf =~ s{\A\s+|\s+\z}{}gmxs }
         else          { chomp($cf) }

         my $decoded = decode_cf($cf);
         my $errors = $decoded->{errors};
         my $status = $errors->@* ? 'not ok' : 'ok';
         $errors = join(' ', map { +"{$_}" } ($errors // [])->@*) || ''
            if $stringify;

         $decoded = {} if $decode eq 'none';
         delete($decoded->{portions}) if $decode eq 'simple';
         push @tests, {
            $decoded->%*,
            cf => $cf,
            errors => $errors,
            status => $status,
         };
      }
   }

   my $ofh = $self->ofh;
   if ($format eq 'json') {
      require Romeo::Util::Transform;
      say {$ofh} Romeo::Util::Transform::encode_json(\@tests);
   }
   elsif ($format eq 'yaml') {
      require YAML::Tiny;
      say {$ofh} YAML::Tiny::Dump(\@tests);
   }
   else { die "wtf?!?" }
}

sub description ($self) {
   return <<'END';

Validate Italian "Codice Fiscale". They can be provided on the command line
or from input file(s) via option "-i"/"--input" (can be provided multiple
times, including also "-" for standard input).

Output can be JSON or YAML depending on "-f"/"--format".

Examples:

   # straight on command line
   romeo cf bcadfe70a01h501j

   # nothing? standard input to the rescue
   echo bcadfe70a01h501j | romeo cf

   # you get the idea, this prints out two checks
   echo bcadfe70a01h501j | romeo cf bcadfe70a01h501j -i -



END
}

1;
