#!/usr/bin/env perl
package Romeo::CmdXxd;
use v5.24;
use warnings;
use experimental 'signatures';

use Ouch;
use Data::HexDump::XXD qw< xxd xxd_r >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< xxd >],
   help    => 'hex dumper a-la xxd (shipped with the Vim editor)',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      {
         getopt => 'plain|p!',
         help   => 'plain dump (no address nor spaces etc.)',
      },
      {
         getopt => 'revert|r!',
         help   => 'revert the conversion (from hex to original)',
      },
   );
} ## end sub options

sub execute ($self) {
   return 0 unless $self->handle_help;

   my ($input, $output, @rest) = $self->residual_args;
   ouch 400, 'too many arguments' if @rest;

   # drive Romeo::Role::IO's inteface
   $self->set_config(input => [$input // '-']);
   $self->set_config(output => ($output // '-'));
   $self->set_config(($_ . '_encoding') => ':raw') for qw< input output >;

   my $idata = $self->slurp;

   my $odata;
   my ($r, $p) = $self->config(qw< revert plain >);
   if ($r) {
      $odata = $p ? pack('H*', $idata =~ s{\s+}{}rgmxs) : xxd_r($idata);
   }
   else {
      $odata = $p ? unpack('H*', $idata) : xxd($idata);
      $odata .= "\n"; # this just makes sense
   }
   $self->spew($odata);

   return 0;
} ## end sub execute

sub description ($self) {
   return <<'END';

This program mimics the most basic features of the xxd utility distributed
with the Vim editor (e.g.  see
https://github.com/vim/vim/tree/master/src/xxd).

   $ xxd [-r|--revert] [-p|--plain] [input [output]]

Input and output default to the standard channels (can also be set
explicitly with the conventional file names "-"), so by defaults it acts as
a filter.

It's possible to use option "-r"/"--revert" to turn a hex dump back into
regular data.

Using option "-p"/"--plain" the hex dump is produced/read without the usual
format, but just with plain hex values.

END
} ## end sub description

1;
