#!/usr/bin/env perl
package Romeo::CmdJson2Value;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch;
use English;
use utf8;

use Template::Perlish qw< traverse >;

use Romeo::Util::Transform qw< decode_json encode_json_compact >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< json2value j2v jv >],
   help    => 'Turn arguments into a JSON (file)',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options(
         qw< single_input input_encoding output output_encoding >),
      {
         getopt => 'json|j=s',
         help   => 'the JSON string to read from',
      },
      {
         getopt => 'newline|n!',
         help   => 'print newline after last line',
         default => 0,
      },
   );
} ## end sub options

sub commit ($self) {
   return if defined($self->config('json'));
   $self->set_config(json => $self->slurp);
   return $self;
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   my $data = decode_json($self->config('json'));
   my $ofh = $self->ofh;
   my $not_first = 0;
   for my $key ($self->residual_args) {
      my $value = traverse($data, $key);
      $value = encode_json_compact($value) if ref($value);
      $value = "\n" . $value if $not_first++;
      print {$ofh} $value;
   }

   print {$ofh} "\n" if $self->config('newline') || $not_first > 1;

   return 0;
}

1;
