#!/usr/bin/env perl
package Romeo::CmdUrlEncode;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;

use Romeo::Util::Transform qw< encode url_decode url_encode >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO Romeo::Role::DescriptionFromPod >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< urldecode urlencode >],
   help    => 'url decode (or encode, depending on the name)',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt => 'string|s=s',
         help => 'the string to work on',
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;
   if (defined(my $string = $self->config('string'))) {
      $self->set_config(input => [\$string]);
   }
   return $self->filter_inputs_to_output('urlencode')
     if $self->call_name eq 'urlencode';
   return $self->filter_inputs_to_output('urldecode');
}

sub urlencode ($self, $ifh, $ofh, @rest) {
   while (<$ifh>) {
      print {$ofh} url_encode(encode('UTF-8', $_));
   }
}

sub urldecode ($self, $ifh, $ofh, @rest) {
   while (<$ifh>) {
      print {$ofh} url_decode($_);
   }
}

1;

=pod

=encoding UTF-8

=head1 DESCRIPTION

Apply URL encode/decode to the inputs.

   # works as a filter by default
   printf %s 'whatever? uh?' | romeo urlencode        # whatever%3f%20uh%3f

   # call as urldecode for decoding
   printf %s 'whatever%3f%20uh%3f' | romeo urldecode  # whatever? uh?

   # -s/--string to provide the string directly on command line
   romeo urlencode -s 'whatever? uh?'           # whatever%3f%20uh%3f
   romeo urldecode -s 'whatever%3f%20uh%3f'     # whatever? uh?


=cut
