#!/usr/bin/env perl
package Romeo::CmdPad;
use v5.24;
use warnings;
use experimental 'signatures';

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< pad >],
   help    => 'Pad input with whitespaces',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt  => 'bottom|b=i',
         help    => 'white lines to add to the bottom',
      },
      {
         getopt  => 'char|c=s',
         help    => 'char for filling',
      },
      {
         getopt  => 'left|l=i',
         help    => 'whitespaces to add to the left',
      },
      {
         getopt  => 'right|r=i',
         help    => 'whitespaces to add to the right',
      },
      {
         getopt  => 'top|t=i',
         help    => 'white lines to add to the top',
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;
   $self->{$_} = $self->config($_) // 0 for qw< left right top bottom >;
   $self->{char} = $self->config('char');
   $self->{char} = ' ' unless length($self->{char} // '');
   $self->{char} = substr($self->{char}, 0, 1);
   return $self->filter_inputs_to_output;
}

sub filter ($self, $ifh, $ofh, $idx, $n) {
   my $char = $self->{char};
   my ($left, $right) = map { $char x $self->{$_} } qw< left right >;

   if (! defined($self->{empty})) { # looking for the first line
      defined(my $first_line = <$ifh>) or return;
      chomp($first_line);
      my $len = length($first_line);
      my $empty = $left . ($char x $len) . $right;
      say {$ofh} $empty for 1 .. $self->{top};
      say {$ofh} $left, $first_line, $right;
      $self->{empty} = $empty; # mark as  first-line-done
   }

   while (<$ifh>) {
      chomp;
      say {$ofh} $left, $_, $right;
   }

   # if this was the last one, emit the bottom side of the frame
   if ($idx + 1 == $n) {
      say {$ofh} $self->{empty} for 1 .. $self->{bottom};
   }
}

sub description ($self) {
   return <<'END';

Add padding around text.

By default the padding is composed of spaces. It's possible to set a
different character with "-c"/"--char".

The line width for top and bottom is taken from the first line.

END
}

1;
