#!/usr/bin/env perl
package Romeo::CmdSameWidth;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< same-width >],
   help    => 'Make all lines the same width (or at least try)',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt => 'cut|c!',
         help => 'cat long lines instead of complaining',
      },
      {
         getopt => 'lookahead|l=i',
         help => 'limit width seeking (0 => ingest everything first)',
         default => 0,
      },
      {
         getopt => 'width|w=i',
         help => 'set width independently of inputs',
      }
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   $self->{analyzed} = 0;
   $self->{limit} = $self->config('limit');
   $self->{width} = $self->config('width');
   $self->filter_inputs_to_output;
   return 0;
}

sub flush_buffer ($self, $ofh) {
   my $width = $self->{candidate} // return;
   for my $line (($self->{buffer} // [])->@*) {
      my $len = length($line);
      say {$ofh} $line, (' ' x ($width - $len));
   }
   return $self;
}

sub filter ($self, $ifh, $ofh, $idx, $n) {
   if (! defined($self->{width})) {
      my $buffer = $self->{buffer} //= [];
      my $limit = $self->{limit};
      $self->{candidate} //= 0;
      while ((! $limit) || ($buffer->@* < $limit)) {
         defined(my $line = <$ifh>) or last;
         chomp($line);
         push $buffer->@*, $line;

         my $len = length($line) or next;
         $self->{candidate} = $len if $len > $self->{candidate};
      }

      if ($limit && ($buffer->@* == $limit)) {
         $self->flush_buffer($ofh);
         $self->{width} = delete($self->{candidate});
      }
   }

   if (defined(my $width = $self->{width})) {
      my $cut = $self->config('cut');
      while (<$ifh>) {
         chomp;
         my $len = length($_);
         if ($len > $width) {
            ouch 400, "line longer than width<$width>" unless $cut;
            substr($_, $width, $len - $width, ''); # cut excess chars
            $len = $width;
         }
         say {$ofh} $_, (' ' x ($width - $len));
      }
   }

   # if this was the last one and there's still buffer...
   $self->flush_buffer($ofh) if $idx + 1 == $n;
}

sub description ($self) {
   return <<'END';

Adapt input so that all lines have the same width.

Option "-l"/"--lookahead" sets a number of lines to look for the target
width. By default it's 0, which means slurping everything and have an exact
idea of the width.

It's also possible to set a width with "-w"/"--width".

If a line is found to exceed the width, an exception is thrown, This does
not happen if option "-c"/"--cut", which cuts the long line instead.

END
}

1;
