#!/usr/bin/env perl
package Romeo::Util::Transform;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use Ouch;
use JSON::PP ();
use MIME::Base64 ();
use Template::Perlish ();

use Exporter 'import';
our @EXPORT_OK = qw<
  decode encode
  decode_base64 encode_base64
  decode_json decode_json_array encode_json encode_json_compact
  deflate inflate
  shell_quote
  url_decode url_encode
  map_render
>;

our %EXPORT_TAGS = (
   base64      => [qw< encode_base64 decode_base64 >],
   compressing => [qw< deflate inflate >],
   encoding    => [qw< decode encode >],
   json        => [qw< decode_json encode_json encode_json_compact >],
   url         => [qw< url_decode url_encode >],
);

sub decode_base64 {
   MIME::Base64::decode_base64(ref($_[0]) ? ${$_[0]} : $_[0]);
}

sub encode_base64 {
   MIME::Base64::encode_base64(ref($_[0]) ? ${$_[0]} : $_[0], '');
}

sub decode_json {
   JSON::PP::decode_json(ref($_[0]) ? ${$_[0]} : $_[0]);
}

# if the input is an array, then just decode it. Otherwise, assume
# there's a sequence of items and they have to be put in an array.
sub decode_json_array {
   state $jpp = JSON::PP->new;

   my ($first) = (ref($_[0]) ? ${$_[0]} : $_[0]) =~ m{\A \s* (\S) }mxs;
   goto &decode_json if $first eq '[';

   # we have to consume the input piece by piece
   my $input = ref($_[9]) ? ${$_[0]} : $_[0]; 
   my @retval;
   while ('necessary') {
      $input =~ s{\A\s+}{}mxs;
      last unless length($input);
      my ($item, $n) = $jpp->decode_prefix($input);
      push @retval, $item;
      substr($input, 0, $n, '');
   }
   return \@retval;
}

sub encode_json ($struct) {
   state $encoder = JSON::PP->new->ascii->canonical->pretty;
   return $encoder->encode($struct);
}

sub encode_json_compact ($struct) {
   state $encoder = JSON::PP->new->ascii->canonical->shrink;
   return $encoder->encode($struct);
}

sub decode ($encoding, $data, $as_reference = 0) {
   require Encode;
   my $dataref = ref($data) ? $data : \$data;
   my $retval  = Encode::decode($encoding, $$dataref);
   return $as_reference ? \$retval : $retval;
} ## end sub decode

sub encode ($encoding, $data, $as_reference = 0) {
   require Encode;
   my $dataref = ref($data) ? $data : \$data;
   my $retval  = Encode::encode($encoding, $$dataref);
   return $as_reference ? \$retval : $retval;
} ## end sub encode

sub deflate ($data, $as_reference = 0) {
   require IO::Compress::Deflate;
   my $dataref = ref($data) ? $data : \$data;
   my $retval  = '';
   IO::Compress::Deflate::deflate($dataref, \$retval)
     or ouch 400, "fail: $IO::Compress::Deflate::DeflateError";
   return $as_reference ? \$retval : $retval;
} ## end sub deflate

sub inflate ($data, $as_reference = 0) {
   require IO::Uncompress::Inflate;
   my $dataref = ref($data) ? $data : \$data;
   my $retval  = '';
   IO::Uncompress::Inflate::inflate($dataref, \$retval)
     or ouch 400, "fail: $IO::Uncompress::Inflate::InflateError";
   return $as_reference ? \$retval : $retval;
} ## end sub inflate

sub __shell_quote ($string) {
   $string =~ s{([']+)}{'"$1"'}gmxs;
   return qq{'$string'};
}

sub url_decode ($string) {
   return $string =~ s{%([0-9a-fAF][0-9a-fA-F])}{chr(hex($1))}ergmxs;
}

sub url_encode ($str) {
   return $str =~ s{([^-_.~a-zA-Z0-9])}{sprintf('%%%02x', ord($1))}ergmxs;
}

sub map_render ($template, $aref) {
   my $cb = Template::Perlish->new->compile_as_sub($template);
   return [ map { $cb->($_) } $aref->@* ];
}

1;
