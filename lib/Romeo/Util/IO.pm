#!/usr/bin/env perl
package Romeo::Util::IO;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use Ouch;
use English;

use Exporter 'import';
our @EXPORT_OK = qw<
  open_fh

  slurp
  slurp_raw
  slurp_utf8

  DEFAULT_BUFFER_SIZE
  syscopy_n
  sysread_n
  syswrite_all
>;

our %EXPORT_TAGS = (
   slurping => [qw< slurp slurp_raw slurp_utf8 >],
   syswrap => [qw< DEFAULT_BUFFER_SIZE syscopy_n sysread_n syswrite_all >],
);

use constant DEFAULT_BUFFER_SIZE => 2**20;

sub set_fh_encoding ($fh, $encoding) {
   return $fh unless defined($encoding);
   my $bm = $encoding =~ m{\A :}mxs ? $encoding : ":encoding($encoding)";
   binmode $fh, $bm or ouch 500, "binmode(): $OS_ERROR";
   return $fh;
} ## end sub set_fh_encoding

sub open_fh ($target, $mode, $encoding = undef) {
   $encoding //= 'UTF-8';
   my $fh;
   if ($target eq '-') {
      $fh = $mode eq '>' ? \*STDOUT : \*STDIN;
   }
   elsif (my ($fd) = $target =~ m{\A \* (0 | [1-9]\d*) \z}mxs) {
      require IO::File;
      $fh = IO::File->new_from_fd($fd, ($mode eq '>' ? 'w' : 'r'));
   }
   else {
      open $fh, $mode, $target
        or ouch 400, "open('$target'): $OS_ERROR";
   }
   return set_fh_encoding($fh, $encoding);
} ## end sub open_fh

sub slurp ($input, %args) {
   my ($fh, $name) =
     ref($input) eq 'GLOB'
     ? ($input, '<filehandle>')
     : (open_fh($input, '<', ':raw'), $input);
   set_fh_encoding($fh, $args{encoding});
   local $/;
   defined(my $retval = readline($fh))
     or ouch 500, "readline('$name'): $OS_ERROR";
   close($fh) or ouch 500, "close('$name'): $OS_ERROR";
   return $args{'as_reference'} ? \$retval : $retval;
} ## end sub slurp

sub slurp_raw ($file, $as_reference = 0) {
   slurp($file, encoding => ':raw', as_reference => $as_reference);
}

sub slurp_utf8 ($file, $as_reference = 0) {
   slurp($file, encoding => 'UTF-8', as_reference => $as_reference);
}

sub syscopy_n ($ifh, $ofh, $n, $buflen = DEFAULT_BUFFER_SIZE) {
   while ($n > 0) {
      my $buffer     = '';
      my $chunk_size = $n <= $buflen ? $n : $buflen;
      syswrite_all($ofh, sysread_n($ifh, $chunk_size, 1));
      $n -= $chunk_size;
   } ## end while ($n > 0)
   return;
} ## end sub syscopy_n

sub sysread_n ($fh, $n, $as_reference = 0) {
   my $buffer = '';
   my $n_read = 0;
   while ($n > 0) {
      my $n_step = sysread($fh, $buffer, $n, $n_read);
      ouch 500, "sysread(): $OS_ERROR"   unless defined $n_step;
      ouch 400, "not enough input bytes" unless $n_step;
      $n      -= $n_step;
      $n_read += $n_step;
   } ## end while ($n > 0)
   return $as_reference ? \$buffer : $buffer;
} ## end sub sysread_n

sub syswrite_all {
   my $fh = shift;
   for my $item (@_) {
      my $dataref = ref($item) ? $item : \$item;
      my $offset  = 0;
      my $n       = length($$dataref);
      while ($n > 0) {
         my $n_written = syswrite($fh, $$dataref, $n, $offset);
         ouch 500, "syswrite(): $OS_ERROR"    unless defined $n_written;
         ouch 400, "syswrite(): cannot write" unless $n_written;
         $n      -= $n_written;
         $offset += $n_written;
      } ## end while ($n > 0)
   } ## end for my $item (@_)
   return;
} ## end sub syswrite_all

1;
