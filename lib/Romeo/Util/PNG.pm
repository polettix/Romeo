#!/usr/bin/env perl
package Romeo::Util::PNG;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use Ouch;
use English;
use JSON::PP ();

use Romeo::Util::Transform qw< decode inflate >;

use Exporter 'import';
our @EXPORT_OK = qw<
  png_crc
  png_parse_atCh
  png_parse_iTXt
  png_parse_tEXt
  png_parse_zTXt
  png_parse_null_terminated
>;

our %EXPORT_TAGS = (all => [@EXPORT_OK]);

sub png_crc (@data) {
   state $full  = 0xffffffff;
   state $table = [
      map {
         my $c = $_;
         $c = $c & 1 ? (0xedb88320 ^ ($c >> 1)) : ($c >> 1) for 1 .. 8;
         $c;
      } 0 .. 255
   ];

   my $c = $full;
   for my $item (@data) {
      my $dataref = ref($item) ? $item : \$item;
      my $n       = length($$dataref);
      for my $i (0 .. ($n - 1)) {
         my $v = ord(substr($$dataref, $i, 1));
         $c = $table->[($c ^ $v) & 0xff] ^ ($c >> 8);
      }
   } ## end for my $item (@data)
   return pack 'N', $c ^ $full;
} ## end sub png_crc

sub png_parse_null_terminated {
   my $dataref      = ref($_[0]) ? $_[0] : \$_[0];
   my $offset       = @_ > 1     ? $_[1] : 0;
   my $as_reference = @_ > 2     ? $_[2] : 0;
   my $null_pos     = index($$dataref, "\0", $offset);
   ouch 400, 'cannot find null for string terminator' if $null_pos < 0;
   my $buffer = substr($$dataref, $offset, $null_pos - $offset);
   my $retval = $as_reference ? \$buffer : $buffer;
   return $retval unless wantarray;
   return ($retval, $null_pos + 1);
} ## end sub png_parse_null_terminated

sub png_parse_atCh ($dataref) {
   my ($keyword, $offset) = png_parse_null_terminated($dataref);
   my $compression_flag = ord(substr($$dataref, $offset++, 1));
   my $bytes            = substr($$dataref, $offset);
   $bytes = inflate($bytes) if $compression_flag;
   return {
      type             => 'atCh',
      filename         => decode('UTF-8', $keyword),
      compression_flag => $compression_flag,
      content          => {bytes => $bytes},
   };
} ## end sub png_parse_atCh

sub png_parse_iTXt ($dataref) {
   my ($keyword, $offset) = png_parse_null_terminated($dataref);
   my $compression_flag   = ord(substr($$dataref, $offset++, 1));
   my $compression_method = ord(substr($$dataref, $offset++, 1));

   (my $lang, $offset) = png_parse_null_terminated($dataref, $offset);
   (my $tkey, $offset) = png_parse_null_terminated($dataref, $offset);
   my $bytes = substr($$dataref, $offset);

   if ($compression_flag) {
      ouch 400, "unsupported compression with method $compression_method"
        if $compression_method != 0;
      $bytes = inflate($bytes);
   }
   return {
      keyword            => decode('ISO-8859-1', $keyword),
      compression_flag   => $compression_flag,
      compression_method => $compression_method,
      language           => $lang,
      translated_keyword => decode('UTF-8', $tkey),
      content            => {characters => decode('UTF-8', $bytes)},
   };
} ## end sub png_parse_iTXt

sub png_parse_tEXt ($dataref) {
   my ($keyword, $offset) = png_parse_null_terminated($dataref);
   my $bytes = substr($$dataref, $offset);
   return {
      type    => 'tEXt',
      keyword => decode('ISO-8859-1', $keyword),
      content => {characters => decode('ISO-8859-1', $bytes)},
   };
} ## end sub png_parse_tEXt

sub png_parse_zTXt ($dataref) {
   my ($keyword, $offset) = png_parse_null_terminated($dataref);

   my $compression_method = ord(substr($$dataref, $offset++, 1));
   ouch 400, "unsupported compression method $compression_method"
     if $compression_method != 0;

   my $bytes = inflate(substr($$dataref, $offset));

   return {
      type               => 'zTXt',
      compression_method => $compression_method,
      keyword            => decode('ISO-8859-1', $keyword),
      content            => {characters => decode('UTF-8', $bytes)},
   };
} ## end sub png_parse_zTXt

1;
