#!/usr/bin/env perl
package Romeo::Util::Choose;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use Ouch;
use English;
use Template::Perlish ();
use Term::Choose qw< choose >;

use Exporter 'import';
our @EXPORT_OK = qw<
   choose_items
   confirm
>;

our %EXPORT_TAGS = ();

sub __choose {
   my $need_tty_in  = ! -t \*STDIN;

   my ($orig_stdout, $tty_out);
   if (! -t \*STDOUT) {
      open $tty_out, '>', '/dev/tty'
         or ouch 500, "open('/dev/tty') (output): $OS_ERROR";
      $orig_stdout = select($tty_out);
   }

   my $orig_stdin;
   if (! -t \*STDIN) {
      open my $orig_stdin, '<&', \*STDIN
         or ouch 500, "dup STDIN: $OS_ERROR";
      close STDIN;
      open STDIN, '<', '/dev/tty'
         or ouch 500, "open('/dev/tty') (input): $OS_ERROR";
   }

   my @result = wantarray ? choose(@_) : scalar(choose(@_));

   if ($orig_stdin) {
      close STDIN;
      open STDIN, '<&', $orig_stdin
         or ouch 500, "dup STDIN back: $OS_ERROR";
   }

   if ($orig_stdout) {
      select($orig_stdout);
      close $tty_out;
   }

   return wantarray ? @result : $result[0];
}

sub confirm ($opts = {}) {
   return scalar __choose([qw< no yes >],
      {
         prompt => 'Confirm?',
         $opts->%*,
         index => 1,
      },
   );
}

sub choose_items ($items, $opts) {
   my $main_opts = {
      include_highlighted => 1,
      layout => 2,
      prompt => 'Select with space bar, select+complete with enter, quit qith q:',
      ($opts->{main} // {})->%*,
      index => 1,
   };

   my $confirm = $opts->{confirm};
   $confirm = {} if $confirm && ! ref($confirm);

   my @chosen;
   while ('necessary') {
      @chosen = __choose($items, $main_opts);
      last unless $confirm;

      my @confirm_info;
      push @confirm_info, $main_opts->{info} if defined($main_opts->{info});
      push @confirm_info, 'Selected items:',
         map { '- ' . $items->[$_] } @chosen;

      last if confirm({ $confirm->%*, info => join("\n", @confirm_info) });
   }

   return \@chosen;
}

1;
