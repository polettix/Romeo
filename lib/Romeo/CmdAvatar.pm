#!/usr/bin/env perl
package Romeo::CmdAvatar;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< avatar >],
   help    => 'Generate ASCII-art avatars',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt => 'blocky|b!',
         help => 'generate block ASCII art instead of blocks based on #',
      },
      {
         getopt => 'print-seed|P!',
         help => 'print seed on standard error',
      },
      {
         getopt => 'reverse|r!',
         help => 'reverse the avatar',
      },
      {
         getopt => 'seed|s=s',
         help => 'seed to feed into ',
      },
      {
         getopt => 'width|w=i',
         help => 'width of the generated avatar',
         default => 8,
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   my $seed = $self->config('seed') // time() . rand(1972);
   say {*STDERR} $seed if $self->config('print-seed');

   my $avatar = avatar($self->config(qw< width >), $seed);
   if ($self->config('blocky')) {
      $avatar = terminalize($avatar, ($self->config('reverse')))
   }
   else {
      reverse_avatar($avatar) if $self->config('reverse');
      $_ = join '', $_->@* for $avatar->@*;
   }

   my $ofh = $self->ofh;
   say {$ofh} $_ for $avatar->@*;
   return 0;
}


sub description ($self) {
   return <<'END'

Generate a random avatar, symmetric with respect to the vertical axis.

   $ romeo avatar
     #  #  
    ###### 
    ##  ## 
     ####  
    ###### 
   ########
   # #### #
   #      #

It is possible to set a seed for generating the avatar, which will
always generate the same avatar. This can be handy if the avatar should
be tied to a username:

   $ romeo avatar -s galook
    #    # 
     #  #  
   ## ## ##
   ##    ##
    #    # 
           
   ########
     ####  

Avatars are generated randomly if the seed is not provided; it's
possible to print the random seed to save avatars that are of interest,
using option "--print-seed"/"-p":

   $ romeo avatar -P
   16991207311625.6575102983
    #    # 
    ###### 
     #  #  
      ##   
   # #  # #
    #    # 
   ########
   #      #

The seed is printed on standard error, not standard output or any other
output set with the common options.

It is possible to print a "blockish" representation that is more compact
and graphical:

   $ romeo avatar -s galook -b
               
      ▀▄  ▄▀   
     ██ ▀▀ ██  
      ▀    ▀   
     ▀▀████▀▀  

Option "--reverse"/"-r" allows reversing the generated avatar, which can
be handy in blockish avatars like the one above:

   $ romeo avatar -bs galook -r
   ████████████
   ███▄▀██▀▄███
   ██  █▄▄█  ██
   ███▄████▄███
   ██▄▄    ▄▄██
   ████████████

Avatars default to a width of 8 pixels, but it's possible to set a
different value with option "--width"/"-w":

   $ romeo avatar -bs galook -w 5
            
      ▀ ▀   
     █ █ █  
     ▀▀ ▀▀  
            
END
}

package Randomish {
   sub new ($package, $seed = undef) {
      my $self = bless {}, $package;
      if (! defined($seed)) { $self->{s} = time() }
      elsif ($seed =~ m{\A(?: 0 | [1-9]\d* )\z}mxs) { $self->{s} = $seed }
      else {
         my $val = 0;
         $val = ($val << 8) | ord(substr($seed, $_))
            for 0 .. length($seed) - 1;
         $self->{s} = $val & 0xFFFFFFFF;
      }
      return $self;
   }

   sub uint32 ($self) {
      $self->{s} = ($self->{s} * 1664525 + 1013904223) & 0xFFFFFFFF;
   }

   sub bit ($self) { $self->uint32 & 0x80000000 ? 1 : 0 }
}

sub avatar ($width, $seed) {
   $width //= 16;
   my $width_2   = int(($width + 1) / 2);
   my $r = Randomish->new($seed);
   my @chars = (' ', '#');
   return [
      map {
         my @half_line = map { $chars[$r->bit ? 1 : 0] } 1 .. $width_2;
         my @reflected = reverse(@half_line);
         shift @reflected if $width % 2;
         [@half_line, @reflected];
      } 1 .. $width
   ];
} ## end sub avathar

sub reverse_avatar ($encoded) {
   for my $line ($encoded->@*) {
      $_ = $_ eq ' ' ? '#' : ' ' for $line->@*;
   }
   return $encoded;
}

sub terminalize ($encoded, $reverse = 1) {
   state $direct_char_for = [
      ' ',                       # 0
      "\N{LOWER HALF BLOCK}",    # 1
      "\N{UPPER HALF BLOCK}",    # 2
      "\N{FULL BLOCK}",          # 3
   ];
   state $c2i = sub ($c) { $c eq ' ' ? 0 : 1 };

   my @char_for = $direct_char_for->@*;
   @char_for = reverse @char_for if $reverse;

   my $first_row_id = 0;
   my @output;
   while ($first_row_id <= $encoded->$#*) {
      my $first_row = $encoded->[$first_row_id++];
      my $second_row =
          $first_row_id <= $encoded->$#*
        ? $encoded->[$first_row_id++]
        : [(' ') x scalar($first_row->@*)];
      push @output, join '', ($char_for[0] x 2), map {
         my $id = $c2i->($first_row->[$_]) * 2 + $c2i->($second_row->[$_]);
         $char_for[$id];
      } 0 .. $first_row->$#*;
      $output[-1] .= $char_for[0] x 2;
   } ## end while ($first_row_id <= $encoded...)
   my $blank = $output[0] =~ s{.}{$char_for[0]}grmxs;
   return [$blank, @output, $blank];
} ## end sub terminalize

1;
