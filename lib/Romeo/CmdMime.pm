#!/usr/bin/env perl
package Romeo::CmdMime;
use v5.24;
use warnings;
use experimental 'signatures';

use Path::Tiny 'path';
use JSON::PP 'decode_json';
use Ouch;
use Template::Perlish 'render';
use MIME::Entity;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< mime mime-b mime-build >],
   help    => 'Deal with MIME (email) messages',
   allow_residual_options => 1,
};

sub description ($self) {
   return <<'END';

Entry point for the `mime` sub-commands suite. Used to do the same thing
as sub-command `blueprint`, which is why it's the default fallback. It is
possible to set a different default through option `fallback-subcommand`,
although it will probably be easier to set the associated environment
variable `ROMEO_MIME_FALLBACK_SUBCOMMAND` instead. The default value for
this option is `blueprint` but this might change at some time in the
future; if you plan on relying on this behaviour, it's better to set the
environment variable permanently.

END
}

sub options ($self) {
   return (
      $self->handle_help_options,
      {
         getopt => 'fallback-subcommand=s',
         help => 'set default sub-command to use as a fallback',
         environment => 'ROMEO_MIME_FALLBACK_SUBCOMMAND',
         default => 'blueprint', # might change in the future
      },
   );
}

sub fallback ($self) {
   my $name = $self->call_name;
   return 'blueprint' if $name eq 'mime-b' || $name eq 'mime-build';
   return $self->config('fallback-subcommand');
}

1;
