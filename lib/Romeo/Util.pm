#!/usr/bin/env perl
package Romeo::Util;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use Ouch;
use English;
use JSON::PP ();
use Scalar::Util qw< reftype >;

use Exporter 'import';
our @EXPORT_OK = qw<
  DEFAULT_BUFFER_SIZE

  decode_json
  encode_json

  decode
  encode

  deflate
  inflate

  load_stuff

  png_crc
  png_parse_atCh
  png_parse_iTXt
  png_parse_tEXt
  png_parse_zTXt
  png_parse_null_terminated

  slurp_raw
  syscopy_n
  sysread_n
  syswrite_all
>;

use constant DEFAULT_BUFFER_SIZE => 2**20;

sub decode_json ($string) {
   JSON::PP::decode_json(ref($string) ? $$string : $string);
}

sub encode_json ($data) {
   state $encoder = JSON::PP->new->ascii->canonical->pretty;
   return $encoder->encode($data);
}

sub decode ($encoding, $data, $as_reference = 0) {
   require Encode;
   my $dataref = ref($data) ? $data : \$data;
   my $retval  = Encode::decode($encoding, $$dataref);
   return $as_reference ? \$retval : $retval;
} ## end sub decode

sub encode ($encoding, $data, $as_reference = 0) {
   require Encode;
   my $dataref = ref($data) ? $data : \$data;
   my $retval  = Encode::encode($encoding, $$dataref);
   return $as_reference ? \$retval : $retval;
} ## end sub encode

sub deflate ($data, $as_reference = 0) {
   require IO::Compress::Deflate;
   my $dataref = ref($data) ? $data : \$data;
   my $retval  = '';
   IO::Compress::Deflate::deflate($dataref, \$retval)
     or ouch 400, "fail: $IO::Compress::Deflate::DeflateError";
   return $as_reference ? \$retval : $retval;
} ## end sub deflate

sub inflate ($data, $as_reference = 0) {
   require IO::Uncompress::Inflate;
   my $dataref = ref($data) ? $data : \$data;
   my $retval  = '';
   IO::Uncompress::Inflate::inflate($dataref, \$retval)
     or ouch 400, "fail: $IO::Uncompress::Inflate::InflateError";
   return $as_reference ? \$retval : $retval;
} ## end sub inflate

sub png_crc (@data) {
   state $full  = 0xffffffff;
   state $table = [
      map {
         my $c = $_;
         $c = $c & 1 ? (0xedb88320 ^ ($c >> 1)) : ($c >> 1) for 1 .. 8;
         $c;
      } 0 .. 255
   ];

   my $c = $full;
   for my $item (@data) {
      my $dataref = ref($item) ? $item : \$item;
      my $n       = length($$dataref);
      for my $i (0 .. ($n - 1)) {
         my $v = ord(substr($$dataref, $i, 1));
         $c = $table->[($c ^ $v) & 0xff] ^ ($c >> 8);
      }
   } ## end for my $item (@data)
   return pack 'N', $c ^ $full;
} ## end sub png_crc

sub png_parse_null_terminated {
   my $dataref      = ref($_[0]) ? $_[0] : \$_[0];
   my $offset       = @_ > 1     ? $_[1] : 0;
   my $as_reference = @_ > 2     ? $_[2] : 0;
   my $null_pos     = index($$dataref, "\0", $offset);
   ouch 400, 'cannot find null for string terminator' if $null_pos < 0;
   my $buffer = substr($$dataref, $offset, $null_pos - $offset);
   my $retval = $as_reference ? \$buffer : $buffer;
   return $retval unless wantarray;
   return ($retval, $null_pos + 1);
} ## end sub png_parse_null_terminated

sub png_parse_atCh ($dataref) {
   my ($keyword, $offset) = png_parse_null_terminated($dataref);
   my $compression_flag = ord(substr($$dataref, $offset++, 1));
   my $bytes            = substr($$dataref, $offset);
   $bytes = inflate($bytes) if $compression_flag;
   return {
      type             => 'atCh',
      filename         => decode('UTF-8', $keyword),
      compression_flag => $compression_flag,
      content          => {bytes => $bytes},
   };
} ## end sub png_parse_atCh

sub png_parse_iTXt ($dataref) {
   my ($keyword, $offset) = png_parse_null_terminated($dataref);
   my $compression_flag   = ord(substr($$dataref, $offset++, 1));
   my $compression_method = ord(substr($$dataref, $offset++, 1));

   (my $lang, $offset) = png_parse_null_terminated($dataref, $offset);
   (my $tkey, $offset) = png_parse_null_terminated($dataref, $offset);
   my $bytes = substr($$dataref, $offset);

   if ($compression_flag) {
      ouch 400, "unsupported compression with method $compression_method"
        if $compression_method != 0;
      $bytes = inflate($bytes);
   }
   return {
      keyword            => decode('ISO-8859-1', $keyword),
      compression_flag   => $compression_flag,
      compression_method => $compression_method,
      language           => $lang,
      translated_keyword => decode('UTF-8', $tkey),
      content            => {characters => decode('UTF-8', $bytes)},
   };
} ## end sub png_parse_iTXt

sub png_parse_tEXt ($dataref) {
   my ($keyword, $offset) = png_parse_null_terminated($dataref);
   my $bytes = substr($$dataref, $offset);
   return {
      type    => 'tEXt',
      keyword => decode('ISO-8859-1', $keyword),
      content => {characters => decode('ISO-8859-1', $bytes)},
   };
} ## end sub png_parse_tEXt

sub png_parse_zTXt ($dataref) {
   my ($keyword, $offset) = png_parse_null_terminated($dataref);

   my $compression_method = ord(substr($$dataref, $offset++, 1));
   ouch 400, "unsupported compression method $compression_method"
     if $compression_method != 0;

   my $bytes = inflate(substr($$dataref, $offset));

   return {
      type               => 'zTXt',
      compression_method => $compression_method,
      keyword            => decode('ISO-8859-1', $keyword),
      content            => {characters => decode('UTF-8', $bytes)},
   };
} ## end sub png_parse_zTXt

sub slurp_raw ($file, $as_reference = 0) {
   my ($fh, $opened);
   if (reftype($file) ne 'GLOB') {
      open my $fh, '<:raw', $file or ouch 400, "open('$file'): $OS_ERROR";
      $opened = 1;
   }
   else {
      $fh = $file;
      $file = '<internal-GLOB>';
   }
   local $/;
   defined(my $retval = readline($fh))
     or ouch 500, "readline('$file'): $OS_ERROR";
   if ($opened) {
      close($fh) or ouch 500, "close('$file'): $OS_ERROR";
   }
   return $as_reference ? \$retval : $retval;

} ## end sub slurp_raw

sub syscopy_n ($ifh, $ofh, $n, $buflen = DEFAULT_BUFFER_SIZE) {
   while ($n > 0) {
      my $buffer     = '';
      my $chunk_size = $n <= $buflen ? $n : $buflen;
      syswrite_all($ofh, sysread_n($ifh, $chunk_size, 1));
      $n -= $chunk_size;
   } ## end while ($n > 0)
   return;
} ## end sub syscopy_n

sub sysread_n ($fh, $n, $as_reference = 0) {
   my $buffer = '';
   my $n_read = 0;
   while ($n > 0) {
      my $n_step = sysread($fh, $buffer, $n, $n_read);
      ouch 500, "sysread(): $OS_ERROR"   unless defined $n_step;
      ouch 400, "not enough input bytes" unless $n_step;
      $n      -= $n_step;
      $n_read += $n_step;
   } ## end while ($n > 0)
   return $as_reference ? \$buffer : $buffer;
} ## end sub sysread_n

sub syswrite_all {
   my $fh = shift;
   for my $item (@_) {
      my $dataref = ref($item) ? $item : \$item;
      my $offset  = 0;
      my $n       = length($$dataref);
      while ($n > 0) {
         my $n_written = syswrite($fh, $$dataref, $n, $offset);
         ouch 500, "syswrite(): $OS_ERROR"    unless defined $n_written;
         ouch 400, "syswrite(): cannot write" unless $n_written;
         $n      -= $n_written;
         $offset += $n_written;
      } ## end while ($n > 0)
   } ## end for my $item (@_)
   return;
} ## end sub syswrite_all

sub load_stuff ($src, %hint) {
   my $dataref;
   if (defined(my $type = delete($hint{type}))) {
      return load_json($src) if $type eq 'json';
      return load_yaml($src) if $type eq 'yaml';
      return load_csv($src, %hint) if $type eq 'csv';
      ouch 400, "'unsupported format '$type'";
   }

   $dataref = slurp_raw($src, 1); # get it as a reference to a string
   return decode_json($dataref) if maybe_json($dataref);
   return decode_yaml($dataref) if maybe_yaml($dataref);
   return load_csv($dataref, %hint);
}

sub maybe_json ($dataref) { $$dataref =~ m<\A\s* [\[\{] >mxs }

sub maybe_yaml ($dataref) {
   $$dataref =~ m{
      \A
      (?:
            \s* ^ ---                         # YAML explicit delimiter
         |  \s* - \s                          # sounds like an array
         |  \s* [\w-]+ \s* :                  # key:
         |  \s* ' [^']* ' \s* :               # 'key':
         |  \s* " (?: \\. | [^\\"] )* " \s* : # "key":
      )
   }mxs;
}

sub load_json ($src) { return decode_json(slurp_raw($src)) }
sub load_yaml ($src) { return decode_yaml(slurp_raw($src)) }

sub decode_yaml ($string) {
   require YAML::Tiny;
   YAML::Tiny::Load(ref($string) ? $$string : $string);
}

sub load_csv ($src, %opts) {
   require Text::CSV_PP;
   return Text::CSV_PP::csv(sep_char => ';', %opts, in => $src);
}

1;
