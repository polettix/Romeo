#!/usr/bin/env perl
package Romeo::CmdCsv2Json;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch;
use English;
use utf8;

use Romeo::Util::Transform 'url_decode';

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< csv2json >],
   help    => 'Turn a CSV into a JSON file',
};

use Romeo::Util::Transform 'encode_json';
use Text::CSV_PP   qw< csv >;

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt => 'bom|B',
         help   => 'take the Byte-Order Mark into consideration',
         environment => 'ROMEO_CSV2JSON_BOM',
      },
      {
         getopt => 'csv|c=s',
         help => '(small) CSV as direct command-line argument',
      },
      {
         getopt      => 'eol|e=s',
         help => 'end-of-record character',
         environment => 'ROMEO_CSV2JSON_EOL',
      },
      {
         getopt      => 'flatten|flat|F!',
         help => 'flatten records from all inputs (whatever the columns)',
      },
      {
         getopt      => 'sep|separator|sep_char|sep-char|s=s',
         help  => 'fields-separator character',
         environment => 'ROMEO_CSV2JSON_SEP',
         default     => ';',
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   # adjust parameters
   for my $key (qw< eol sep >) {
      my $value = $self->config($key) // next;
      $self->{$key} = url_decode($value);
   }

   $self->set_config(input => [ $self->direct_csv ])
     if defined($self->config('csv'));

   my $data = $self->parse_inputs;
   $data = [ map { $_->@* } $data->@* ]
     if ($data->@* == 1) || $self->config('flatten');

   my $ofh = $self->ofh;
   binmode $ofh, ':raw' or ouch 500, "binmode(): $OS_ERROR";

   print {$ofh} encode_json($data)
     or ouch 500, "print(): $OS_ERROR";

   close $ofh         or ouch 500, "close(): $OS_ERROR";

   return 0;
}

sub parse_inputs ($self) {
   my $eol = $self->{eol};
   my @base_csv_args = (sep_char   => $self->{sep});

   my $bom = $self->config('bom') // 0;
   push @base_csv_args, $bom ? (detect_bom => 1) : (headers => 'auto');

   my @data;
   while (defined(my $input = $self->next_ifh)) {
      my @csv_args = (@base_csv_args, in => $input);
      push @csv_args, eol => $eol if defined($eol);
      push @data, csv(@csv_args);
   }

   return \@data;
}

sub direct_csv ($self) {
   my $csv = $self->config('csv') // ouch 500, 'WTF?!?';
   if (defined(my $eol = $self->{eol})) {
      my $leol = length($eol);
      my $lcsv = length($csv);
      my $max_back_offset = $leol < $lcsv ? $leol : $lcsv;
      for my $back_offset (reverse(0 .. $max_back_offset)) {
         my $csv_tail = substr($csv, -$back_offset, $back_offset);
         my $eol_head = substr($eol, 0, $back_offset);
         next if $csv_tail ne $eol_head;
         my $n_chars_to_add = $leol - $back_offset;
         last unless $n_chars_to_add; # ends with the eol, all right
         $csv .= substr($eol, -$n_chars_to_add, $n_chars_to_add);
         last;
      }
   }
   return \$csv;
}

sub description ($self) {
   return <<'END'

Turn a CSV into a JSON.

Several input CSV files with different columsn can be flattened with
"-f"/"--flatten".

END
}

1;
