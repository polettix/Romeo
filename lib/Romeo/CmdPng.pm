#!/usr/bin/env perl
package Romeo::CmdPng;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use Ouch;
use Fcntl 'SEEK_CUR';
use English;
use Template::Perlish qw< crumble traverse >;

use Romeo::Util::IO 'sysread_n';
use Romeo::Util::PNG ':all';
use Romeo::Util::Transform qw< encode encode_json >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases                => [qw< png pngmeta png-meta >],
   help                   => 'Get PNG metadata from files',
   fallback_to            => '-self',
   default_child          => '-self',
   allow_residual_options => 1,
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options(
         qw< input output _output_encoding_none >),
      {
         getopt => 'key|k=s@',
         help   => 'keys to look for'
      },
      {
         getopt => 'include|I=s@',
         help   => 'add path to @INC for loading modules',
      },
      {
         getopt => 'list|l!',
         help   => 'get a list of available keywords',
      },
      {
         getopt => 'module|M=s@',
         help   => 'import module with handler',
      },
      {
         getopt => 'newline|n!',
         help   => 'ensure a newline at end of plain stuff',
      },
      {
         getopt  => 'format|f=s',
         help    => 'format of data output (plain|json|yaml)',
         default => 'json',
      },
   );
} ## end sub options

sub validate ($self) {
   my $format = $self->config('format');
   ouch 400, "unsupported format <$format>"
     unless $format =~ m{\A(?: plain | json | yaml )\z}imxs;
   return;
} ## end sub validate

sub execute ($self) {
   return 0 unless $self->handle_help;

   $self->setup_callbacks;
   my $meta = $self->collect_meta;

   if ($self->config('list')) {
      $meta = [sort { $a cmp $b } keys($meta->%*)];
   }
   elsif (my $keys = $self->config('key')) {
      my %filtered;
      for my $key ($keys->@*) {
         my $path = crumble($key);
         $filtered{$path->[0]} = traverse($meta, $path);
      }
      $meta = \%filtered;
   } ## end elsif (my $keys = $self->...)

   # deal with encoding directly
   my $output_enc = $self->config('output_encoding');
   $self->set_config(output_encoding => ':raw');
   my $ofh = $self->ofh;    # now this is raw

   my $format = lc($self->config('format'));
   if ($format eq 'plain') {
      $meta = {_list => join("\n", $meta->@*, '')}
        if ref($meta) eq 'ARRAY';
      my ($value, @rest) = values($meta->%*);
      ouch 400, 'too many keys available, list with -l, select with -k'
        if @rest;
      $value = $value->[0] if ref($value) eq 'ARRAY';
      (my $type, $value) = $value->%*;
      $value .= "\n" if $self->config('newline') && $value !~ m{\n\z}mxs;

      # characters must be encoded into bytes as per output_encoding
      $value = encode($output_enc, $value)
        if $type eq 'characters' && ($output_enc // ':raw') ne ':raw';

      print {$ofh} $value;
   } ## end if ($format eq 'plain')
   elsif ($format eq 'json') {

      # encode_json does UTF-8 encoding by itself and gives us bytes, so
      # we complain of any other output encoding
      $output_enc //= ':raw';
      ouch 400, "invalid output encoding '$output_enc' with JSON format"
        if $output_enc ne ':raw';

      print {$ofh} encode_json($meta);
   } ## end elsif ($format eq 'json')
   elsif ($format eq 'yaml') {
      require YAML::Tiny;
      my $characters = YAML::Tiny::Dump($meta);

      # YAML::Dump gives us characters, we need to convert them to bytes
      # this is why *here* we have a different default output encoding
      $output_enc //= 'UTF-8';

      # :raw is just plain bad here. We might default to UTF-8 but it does
      # not seem right
      ouch 400, "invalid output encoding ':raw' with YAML format"
        if $output_enc eq ':raw';

      print {$ofh} encode($output_enc, $characters);
   } ## end elsif ($format eq 'yaml')

   return 0;
} ## end sub execute

sub setup_callbacks ($self) {
   my %callback_for = (    # basic stuff
      atCh => sub ($dataref) {
         my $parsed = png_parse_atCh($dataref);
         $parsed->{keyword} = $parsed->{filename};
         return $parsed;
      },
      iTXt => \&png_parse_iTXt,
      tEXt => \&png_parse_tEXt,
      zTXt => \&png_parse_zTXt,
   );
   $self->{callback_for} = \%callback_for;

   # add modules if requested
   my $modules = $self->config('module') // [];
   return $self unless $modules->@*;

   my $includes = $self->config('include') // [];
   local @INC = @INC;
   push @INC, $includes->@*;

   for my $module ($modules->@*) {
      my $file = "$module.pm" =~ s{::}{/}rgmxs;
      require $file;
      %callback_for = (%callback_for, $module->png_callbacks);
   }

   return $self;
} ## end sub setup_callbacks

sub collect_meta ($self) {
   my $meta = {};
   $self->set_config(input_encoding => ':raw');
   while (defined(my $ifh = $self->next_ifh)) {
      $self->add_meta_from($ifh, $meta);
   }
   return $meta;
} ## end sub collect_meta

sub description ($self) {
   return <<'END';

This sub-command allows extracting metadata sections from PNG files. By
default, it will extract iTXt, tEXt, and zTXt chuunks; other ones can be
added.

Output defaults to JSON, each key associated to an array of values. YAML and
plain values are supported too.

With the plain format, it's possible to pass a key (or not, if there is only
one key in the file) and it will be printed. The first one is selected when
there are multiple values; it's possible to use Template::Perlish's
crumble-ing algorithm to pass an exact place (typically it would be "key.n",
where n is an integer).

Option "-n"/"--newline" allows adding a newline in plain format, if it's
missing in the text. This should ease printing on the terminal.

Option "-k"/"--key" allows selecting only one or a limited amount of keys;
again, these options follow the rules for Template::Perlish "crumble".

It's possible to code additional dissectors and make this sub-command aware
of them. As an example, consider the following module:

   package MyMod;
   use v5.24;
   use warnings;
   use experimental 'signatures';

   sub png_callbacks {
      return (
         default => sub ($type, $dataref) {
            warn "default: got type<$type>\n";
            return {};
         }
      );
   }

   1;


The "png_callback" must return key/value pairs, where the key is the
name of the chunk for the dissection, and the values are code references
with the following signature:

   sub ($type, $dataref) --> hashref
      # $type is the string of the chunk type
      # $dataref is a reference to a byte string with the data
      # return value is a hash reference with keys and array
      #      references or plain scalars.

If the module above is saved in the current directory as "MyMod.pm":

   $ romeo png-meta -I . -M MyMod -i /tmp/prova.png
   default: got type<IHDR>
   default: got type<IDAT>
   default: got type<IEND>

END
} ## end sub description

sub add_meta_from ($self, $ifh, $meta) {
   my $head = unpack 'H*', sysread_n($ifh, 8);
   ouch 400, "invalid PNG header <$head>\n"
     if fc($head) ne fc('89504E470D0A1A0A');
   my $cbf     = $self->{callback_for};
   my $default = $cbf->{default} // undef;
   while ('necessary') {
      my $length = unpack('N', sysread_n($ifh, 4));
      my $type   = sysread_n($ifh, 4);

      if (defined(my $cb = $cbf->{$type} // $default)) {
         my $dataref = sysread_n($ifh, $length, 1);    # gets a reference
         my $crc     = sysread_n($ifh, 4);
         die "CRC mismatch in <$type>\n"
           unless $crc eq png_crc($type, $dataref);

         for my $item ($cb->($dataref)) {
            my ($key, $value) = $item->@{qw< keyword content >};
            my $slot = $meta->{$key} //= [];
            push $slot->@*, ref($value) eq 'ARRAY' ? $value->@* : $value;
         }
      } ## end if (defined(my $cb = $cbf...))
      else {    # just skip this, forget about the CRC
         sysseek($ifh, $length + 4, SEEK_CUR);
      }

      last if $type eq 'IEND';
   } ## end while ('necessary')
   return $meta;
} ## end sub add_meta_from

1;
