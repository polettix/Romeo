#!/usr/bin/env perl
package Romeo::CmdChoose;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch ':trytiny_var';
use Romeo::Util 'load_stuff';
use Romeo::Util::Choose qw< choose_items >;
use Romeo::Util::Transform qw< encode_json map_render >;
use Template::Perlish ();

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< choose >],
   help    => 'choose items from a list',
};

sub description ($self) {
   return <<'END';
END
}

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt => 'confirm|C',
         help => 'print out selected items and ask confirmation',
      },
      {
         getopt => 'info|I=s',
         help => 'info printed before prompt',
      },
      {
         getopt => 'prompt|P=s',
         help => 'prompt for choosing items',
      },
      {
         getopt => 'template|T=s',
         help => 'template for selection list',
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   # some validation
   my $template = $self->config('template')
      // ouch 400, 'no template provided';

   # get inputs and generate associated labels
   my @items;
   while (my $ifh = $self->next_ifh) {
      my $stuff = load_stuff($ifh, type => 'json');
      push @items, (ref($stuff) eq 'ARRAY' ? $stuff->@* : $stuff);
   }
   my $labels = map_render($template, \@items);

   # gather options
   my %opts = (
      confirm => $self->config('confirm'),
      main => {},
   );
   for my $feature (qw< prompt info >) {
      defined(my $value = $self->config($feature)) or next;
      $opts{main}{$feature} = $value;
   }

   my $chosen = choose_items($labels, \%opts);

   $self->spew(encode_json([@items[$chosen->@*]]));
   return 0;
}

1;
