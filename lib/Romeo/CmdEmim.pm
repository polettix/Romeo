#!/usr/bin/env perl
package Romeo::CmdEmim;
use v5.24;
use warnings;
use experimental 'signatures';

use Path::Tiny;
use Ouch;
use Template::Perlish 'render';
use MIME::Parser;
use JSON::PP ();
use Email::Address ();

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< emim mime-x mime-extract >],
   help    => 'Parse MIME email messages',
};

sub description ($self) {
   return <<'END';

Parse email messages according to MIME.


END
} ## end sub description

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options(qw< output >),
      {
         getopt => 'dir|d=s',
         help   => 'output directory for parts/headers/skeleton',
      },
      {
         getopt => 'headers!',
         help   => 'print headers',
      },
      {
         getopt => 'skeleton',
         help   => 'print skeleton',
      },
   );
} ## end sub options

sub __encode_json ($data) {
   state $encoder = JSON::PP->new->ascii->canonical->pretty;
   $encoder->encode($data);
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   $self->set_config(input => [ $self->residual_args ]);

   my $parser = MIME::Parser->new;
   my $dir    = $self->config('dir');
   my $print_skeleton = $self->config('skeleton');
   my $print_headers = $self->config('headers');
   $print_headers //= 1 unless $dir || $print_skeleton;

   if ($dir) {
      my $output = path($dir);
      $output->mkdir;
      $parser->output_under($output->stringify);
      my $filer = $parser->filer;
      $filer->ignore_filename(1);
      $filer->output_prefix('msg');
   }
   else {
      $parser->output_to_core(1);
   }

   my $ofh = $self->ofh;
   for (1 .. $self->n_inputs) {
      my $entity = $parser->parse($self->next_ifh);
      my $headers = __encode_json(__hashify_headers($entity));

      if ($dir) {
         my $odir = path($parser->output_dir);
         $entity->dump_skeleton($odir->child('skeleton.txt')->openw_utf8);
         $odir->child('headers.json')->spew_raw($headers);
      }

      $entity->dump_skeleton($ofh) if $print_skeleton;
      print {$ofh} $headers if $print_headers;
   } ## end for (1 .. $self->n_inputs)

   return 0;
} ## end sub execute

sub __hashify_headers ($entity) {
   my $head = $entity->head;
   my $retval = {
      map {
         chomp(my @vals = $head->get_all($_));
         @vals =
            map { $_->format }
            map { Email::Address->parse($_) } @vals
            if m{\A (?: from | sender | to | cc | bcc ) \z}imxs;
         $_ => (@vals == 1 ? $vals[0] : \@vals);
      } $head->tags
   };

   $retval->{'-parts'} = [ map { __SUB__->($_) } $entity->parts ]
      if $entity->is_multipart;

   return $retval;
}

1;
