#!/usr/bin/env perl
package Romeo::CmdVersion;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;

use Role::Tiny::With;
with qw< Romeo::Role::Help >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< version >],
   help    => 'print version of Romeo',
};

sub options ($self) { return $self->handle_help_options }

sub execute ($self) {
   return 0 unless $self->handle_help;
   print {*STDERR} <<'END';

      |\      _,,,---,,_          ___                       
      /,`.-'`'    -.  ;-;;,_     | . \ ___ ._ _ _  ___  ___ 
     |,4-  ) )-,_..;\ (  `'-'    |   // . \| ' ' |/ ._)/ . \
    '---''(_/--'  `-'\_)  fL     |_\_\\___/|_|_|_|\___.\___/

END
   say $self->parent->version;
   return 0;
}

1;
