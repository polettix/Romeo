#!/usr/bin/env perl
package Romeo::CmdPass;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch;

use Role::Tiny::With;
with qw< Romeo::Role::Help >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< pass password >],
   help    => 'generate a random password',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      {
         getopt => 'alphabet|a=s',
         help => 'alphabet of characters to use',
         default => 'cdefhjkmnpqrtuvwxy37',
      },
      {
         getopt => 'count=i',
         help   => 'number of passwords that must be generated',
         default => 1,
      },
      {
         getopt => 'digit|ensure-digit|d',
         help => 'make sure that the generated password has a digit',
      },
      {
         getopt => 'entropy|print-entropy|e!',
         help => 'print entropy on standard error',
         default => undef,
      },
      {
         getopt => 'min-entropy|E=i',
         help => 'set minimum entropy for password',
      },
      {
         getopt => 'separator|S=s',
         help => 'separator between "words"',
         default => '.',
      },
      {
         getopt => 'size|s=s',
         help => 'size of password (NxM, e.g. 4x4)',
         default => '4x4',
      },
      {
         getopt => 'uppercase|uppercase-first|u',
         help => 'turn first non-digit character into an uppercase',
      },
   );
}

sub description ($self) {
   return <<'END';

Generate a random password that is easy to read and to type (especially on a
mobile phone).

   $ romeo pass
   pt3e.kkcu.pyj3.jtpt

   $ romeo pass -s 4x6
   hrwh.huqf.rmfq.mnwr.yc7x.j3qt

   # print entropy on standard error, ensure digit and uppercase
   $ romeo pass -edu
   entropy (16/4): 69.2   # <-- this is on standard error
   Pndf.uyxm.333u.dmcr    # <-- this is on standard output

   # ensure minimum entropy
   $ rome pass -E 100
   entropy (24/4): 103.7
   3f3c.v7vm.hw7x.w3jm.tfyd.qtfv

The size of the password is provided with option "--size"/"-s" and it's a
string like "4x5", which means 5 sections of 4 characters each, separated by
a separator char ("--separator"/"-S"). The characters are chosen from a
specific alphabet ("--alphabet"/"-a").

The defaults aim at making things simple:

- the defualt alphabet is "cdefhjkmnpqrtuvwxy37", which eliminates all pairs
  that might be confused (e.g. o, O and 0, or 1, i and l, etc.) and uses
  lowercase characters which are default on most keyboards;
- the default separator is the dot "." which is usually shown by default on
  most keyboards and has the same position/code/ease of accessibility;
- the default size is "4x4" so that grouping makes reading easier.

A typical password with the default values is:

   $ romeo pass
   ufve.xmmm.3fhn.khjq

There are options if the target system insists on having an uppercase and a
digit. Their logic is slightly different:

- for the uppercase, the first character that can be turned into an
  uppercase value is modified. If none can, one character is added.
- for the digit, the generated password is checked to see if there's one,
  otherwise it is added.

In these cases, the final password might be 1 or 2 or 3 characters longer
(depending also on whether an additional separator was needed or not).

Last, it's possible to print the entropy of the generated password with
"--entropy"/"-e" on standard error, as well as setting a minimum value for
entropy with "--min-entropy"/"-E". In the latter case, the number of random
characters in the password is adjusted to fulfil the requirement,
potentially leading to longer password. Example:

   $ romeo pass -E 100
   entropy (24/4): 103.7
   3qju.eur7.p3ex.cyfj.chrx.dvf7

Setting the minimum entropy also enables printing of the final entropy on
standard error; this can be turned off with "--no-entropy".

Entropy is printed on standard error with a form "(x/y) e", where "x" is the
number of total characters contributing to the random part of the password
(i.e. excluding the separators, which are fixed and thus not random), "y" is
the length of each group (according to the input size), and e is the entropy
value (truncated at the first decimal digit).

END
}

sub parse_size ($self) {
   my ($n, $m) = split m{x}imxs, $self->config('size');
   ouch 400, 'invalid input size' unless $n =~ m{\A[1-9]\d*\z}mxs;
   $m ||= 1;
   $self->@{qw< plen n >} = ($n * $m, $n);
}

sub parse_alphabet ($self) {
   my @alphabet = split m{}mxs, $self->config('alphabet')
     or ouch 400, 'invalid empty charset';

   my @digits;
   if ($self->config('digit')) {
      @digits = grep { m{[[:digit:]]}mxs } @alphabet
        or ouch 400, 'no digit in the input alphabet';
   }

   my @uppable;
   if ($self->config('uppercase')) {
      @uppable = grep { $_ ne uc($_) } @alphabet
        or ouch 400, 'no character that can be turned to uppercase';
   }

   $self->@{qw< alphabet digits uppable >} =
      (\@alphabet, \@digits, \@uppable);
}

sub init_entropy ($self) {
   my ($alphabet, $plen) = $self->@{qw< alphabet plen >};

   my $char_entropy = log(scalar($alphabet->@*)) / log(2);
   my $entropy = $plen * $char_entropy;

   my $min_entropy = $self->config('min-entropy');
   if (($min_entropy // -1) > $entropy) {
      $plen = int($min_entropy / $char_entropy);
      ++$plen if $plen * $char_entropy < $min_entropy;
      $entropy = $plen * $char_entropy;
   }

   $self->@{qw< entropy plen >} = ($entropy, $plen);
}

sub generate_password ($self) {
   my ($alphabet, $plen, $n) = $self->@{qw< alphabet plen n >};
   my $separator = $self->{separator} = $self->config('separator');
   $self->{password} = join $separator, map {
      join '', map { $alphabet->[rand $alphabet->@*] } 1 .. $_
   } grep { $_ } (($n) x ($plen / $n), $plen % $n);
}

sub append_separator_if_necessary ($self) {
   return if $self->{plen} % $self->{n};
   $self->{password} .= $self->{separator};
   $self->{plen} += 1;
   return;
}

sub adjust_for_digit ($self) {
   my $password = $self->{password};
   return if $password =~ m{[[:digit:]]}mxs;

   $self->append_separator_if_necessary;

   my $digits = $self->{digits};
   $self->{password} .= $digits->[rand $digits->@*];
   $self->{plen} += 1;
   $self->{entropy} += log(scalar($digits->@*)) / log(2);

   return $self->{password};
}

sub adjust_for_uppercase ($self) {
   my @chars = split m{}mxs, $self->{password};
   for my $char (@chars) {
      if ((my $upper = uc($char)) ne $char) {
         $char = $upper;
         return $self->{password} = join '', @chars;
      }
   }

   $self->append_separator_if_necessary;

   my $uppable = $self->{uppable};
   $self->{password} .= uc($uppable->[rand $uppable->@*]);
   $self->{plen} += 1;
   $self->{entropy} += log(scalar($uppable->@*)) / log(2);

   return $self->{password};
}

sub execute ($self) {
   $self->handle_help;

   for (1 .. $self->config('count')) {
      $self->parse_size;
      $self->parse_alphabet;
      $self->init_entropy;
      $self->generate_password;
      $self->adjust_for_digit if $self->config('digit');
      $self->adjust_for_uppercase if $self->config('uppercase');

      my ($plen, $n, $entropy) = $self->@{qw< plen n entropy >};
      printf {*STDERR} "entropy (%d/%d): %.1f\n", $plen, $n, $entropy
      if $self->config('entropy') // defined($self->config('min-entropy'));

      binmode STDOUT, ':encoding(UTF-8)';
      say {*STDOUT} $self->{password};
   }

   return 0;
}

1;
