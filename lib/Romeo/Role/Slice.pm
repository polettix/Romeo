#!/usr/bin/env perl
package Romeo::Role::Slice;
use v5.24;
use warnings;
use utf8;
use JSON::PP 'decode_json';
use YAML::Tiny 'Load';
use Template::Perlish qw< crumble traverse >;
use Scalar::Util 'refaddr';
use Storable 'dclone';
use Ouch;

use Role::Tiny;
use experimental 'signatures';

sub slice_options ($self) {
   return (
      {
         getopt  => 'definitions|d=s@',
         help    => 'file(s) with slicing definitions',
         default => [],
      },
      {
         getopt  => 'format|f=s',
         help    => 'format of data input',
         default => 'auto',
      },
      {
         getopt  => 'input|i=s@',
         help    => 'input path (supports "-" for standard input)',
         default => [],
      },
      {
         getopt => 'line-length|l=i',
         help => 'line length for interactive selection',
         default => 80,
      },
      {
         getopt => 'summary|x',
         help => 'default destination key as last portion',
         default => 0,
      },

   );
}

sub get_format ($self) {
   $self->{format} //= do {
      my $format = lc($self->config('format'));
      ouch 400, "invalid format '$format'"
        unless $format =~ m{\A(?: auto | json | yaml )\z}imxs;
      $format;
   };
} ## end sub get_format

sub load_file ($self, $ifh, $fmt = undef) {
   $ifh = $self->open_input($ifh) unless ref($ifh) eq 'GLOB';
   my $str = do { local $/; <$ifh> };

   $fmt //= $self->get_format;
   my $format =
       ($fmt eq 'json' || $fmt eq 'yaml') ? $fmt
     : $str =~ m<\A\s*[\[\{]>mxs          ? 'json'
     :                                      'yaml';

   return $format eq 'json' ? decode_json($str) : Load($str);
} ## end sub load_file

sub load_input ($self) {

  # Romeo::Role::IO expects inputs to appear as residual_args, so we
  # collect them here and set residual_args free. We will restore it before
  # returning.
   my @args = $self->residual_args;

   my @inputs = $self->config('input')->@*;
   push @inputs, '-' unless @inputs;
   $self->residual_args(\@inputs);

   my $format = $self->get_format;

   my $overall;
   while (defined(my $ifh = $self->next_ifh)) {
      my $data = $self->load_file($ifh, $format);

      if (!defined($overall)) {
         my $r = ref($data);
         ouch 400, 'invalid input, either hash or array accepted'
           if ($r ne 'HASH') && ($r ne 'ARRAY');
         $overall = $data;
      } ## end if (!defined($overall))
      elsif (ref($overall) ne ref($data)) {
         ouch 400, 'incompatible multiple JSON inputs';
      }
      elsif (ref($overall) eq 'ARRAY') {
         push $overall->@*, $data->@*;
      }
      else {
         $overall->%* = ($overall->%*, $data->%*);
      }
   } ## end while (defined(my $ifh = ...))

   $self->residual_args(\@args);
   return $overall;
} ## end sub load_input

sub load_slicing_definitions ($self) {
   my @raw_definitions;

   my $format = $self->get_format;
   for my $file ($self->config('definitions')->@*) {
      my $defs = $self->load_file($file, $format);
      push @raw_definitions, ref($defs) eq 'ARRAY' ? $defs->@* : $defs;
   }
   push @raw_definitions, $self->residual_args;

   return [
      map {
         my $def = $_ =~ s{\A\s+|\s+\z}{}rgmxs;
         $def = ':' if $def eq '@';    # treat '@' as an alias for ':'
         if (!ref($def)) {
            my ($dst, $src, $pos);

            if ($def =~ m{\A [:=]}mxs) {    # "prefix" part empty?
               $dst = [];
               $pos = 0;
            }
            elsif ($def =~ m{\A \@ [:=]}mxs) {    # "basename" src
               $dst = undef;  # defer extraction of destination from source
               $pos = 1;
            }
            else {            # parse the "prefix"
               ($dst, $pos) = crumble($def, 1);
               ouch 400, "invalid definition <$_>" unless defined($dst);
            }

            my $deflen = length($def);
            if ($pos == $deflen) {
               $src = $dst //= [];
            }
            else {            # there's more, hopefully!
               ouch 400, "invalid definition <$_>"
                 if substr($def, $pos, 1) !~ m{\A[:=]}mxs;
               if ($deflen == $pos + 1) {    # ending with ":"/"="
                  $src = [];
               }
               else {
                  $src = crumble(substr($def, $pos + 1))
                    or ouch 400, "invalid definition <$_>";
               }
            } ## end else [ if ($pos == $deflen) ]
            $dst //= $src->@* ? [$src->[-1]] : [];
            $def = {src => $src, dst => $dst};
         } ## end if (!ref($def))

         ouch 'invalid definition present' unless ref($def) eq 'HASH';
         $def = {$def->%*};

         ouch 'missing locator for data' unless exists($def->{src});
         $def->{dst} //= $def->{src};

         $def;
      } @raw_definitions
   ];
} ## end sub load_slicing_definitions

sub choose ($self, $definitions = undef, $input = undef) {
   $definitions //= $self->load_slicing_definitions;
   $input //= $self->load_input;

   my $target = ref($input) eq 'ARRAY' ? $input->[0] : $input;
   my @rendered = $self->renderable($target);
   my @list = map { $_->[-1] } @rendered;

   my %pre_existent;
   my %line_for;
   DEFINITION:
   for my $def_idx (0 .. $definitions->$#*) {
      my $def_src = $definitions->[$def_idx]{src};
      IDX:
      for my $idx (0 .. $#rendered) {
         my $path = $rendered[$idx][0];
         next IDX unless scalar($def_src->@*) == scalar($path->@*);
         for my $j (0 .. $path->$#*) {
            next IDX if $def_src->[$j] ne $path->[$j];
         }
         $pre_existent{$idx}{$def_idx} = 1;
         $line_for{$def_idx} = $idx;
         next DEFINITION;
      }
   }

   require Term::Choose;
   my $chooser = Term::Choose->new;
   my @chosen = $chooser->choose(\@list,
      {
         include_highlighted => 1,
         index => 1,
         layout => 2,
         mark => [ sort { $a <=> $b } keys %pre_existent ],
      }
   );
   my %chosen = map { $_ => 1 } @chosen;

   my @final =
      map { $definitions->[$_] }
      grep { $chosen{$line_for{$_}} }
      0 .. $definitions->$#*;

   my $keep_last = $self->config('summary');
   push @final, map {
      my $path = $rendered[$_][0];
      my $dst = $keep_last ? [ $path->@* ? $path->[-1] : () ] : $path;
      { src => $path, dst => $dst }
   } grep {! $pre_existent{$_} } @chosen;

   return (\@final, $input);
}

sub line_length ($self) {
   $self->{line_length} //= $self->config('line-length') // 80;
}

sub renderable ($self, $data, $path = []) {
   my $prefix = '  ' x $path->@*;
   my $len = $self->line_length - length($prefix);
   my @lines;
   if (ref($data) eq 'HASH') {
      my @keys = sort { $a cmp $b } keys $data->%*;
      for my $key (@keys) {
         my @path = ($path->@*, $key);
         my $value = $data->{$key};
         my $line = "$prefix$key:";
         if (! ref($value)) {
            if (! defined($value)) {
               $value = 'undef';
            }
            else {
               my $max_len = $len - length($line) - 1 - 2;
               $value =~ s{\n.*}{}mxs;
               $value =~ s{(["\\])}{\\$1}gmxs;
               $value = substr($value, 0, $max_len - 3) . '...'
                 if length($value) >= $max_len;
               $value = qq{"$value"};
            }
            $line .= ' ' . $value;
         }
         push @lines, [\@path, $line];
         push @lines, $self->renderable($value, \@path) if ref($value);
      }
   }
   elsif (ref($data) eq 'ARRAY') {
      for my $key (0 .. $data->$#*) {
         my @path = ($path->@*, $key);
         my $value = $data->[$key];
         my $line = "$prefix-";
         if (! ref($value)) {
            if (! defined($value)) {
               $value = 'undef';
            }
            else {
               my $max_len = $len - length($line) - 1 - 2;
               $value =~ s{\n.*}{}mxs;
               $value =~ s{(["\\])}{\\$1}gmxs;
               $value = substr($value, 0, $max_len - 3) . '...'
                 if length($value) >= $max_len;
               $value = qq{"$value"};
            }
            $line .= ' ' . $value;
         }
         push @lines, [\@path, $line];
         push @lines, $self->renderable($value, \@path) if ref($value);
      }
   }
   return @lines;
}

sub _json ($s, $x) { JSON::PP->new->canonical->ascii->pretty->encode($x) }

sub slice ($self, $definitions, $input, $skip_missing) {
   my $is_array     = ref($input) eq 'ARRAY';
   my $missing      = \'missing';               # just a reference
   my $mref         = refaddr($missing);
   my @transformed  = map {
      my $record = {};
      for my $definition ($definitions->@*) {
         my ($src, $dst) = $definition->@{qw< src dst >};
         my $val        = traverse($_, $src, {missing => $missing});
         my $is_missing = (refaddr($val) // 0.5) == $mref;
         next if $skip_missing && $is_missing;

         my $ptr = traverse(\$record, $dst)
            or ouch 400, "invalid destination definition <$dst>";
         $$ptr = $is_missing ? undef
            : ref($val) ? dclone($val) : $val;
      } ## end for my $definition ($slicing_definitions...)
      $record;
   } $is_array ? $input->@* : $input;
   return $is_array ? \@transformed : $transformed[0];
}

1;
