#!/usr/bin/env perl
package Romeo::Role::Help;
use v5.24;
use English;
use Role::Tiny;
use experimental 'signatures';

sub handle_help ($self) {
   return $self->run_help          && 0 if $self->config('help');
   return $self->run_help('usage') && 0 if $self->config('usage');
   return $self;
} ## end sub handle_help

sub help_requested ($self) { return !!($self->config('help')) }

sub handle_help_options ($self) {
   return (
      {
         getopt => 'help',
         help   => 'print help about command',
      },
      {
         getopt => 'usage',
         help   => 'print usage about command',
      },
#      {
#         getopt => 'man',
#         help   => 'print the manual page (if present)',
#      }
   );
} ## end sub handle_help_options

1;
__END__
