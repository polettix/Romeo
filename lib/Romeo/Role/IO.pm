#!/usr/bin/env perl
package Romeo::Role::IO;
use v5.24;
use English;
use Ouch;
use Role::Tiny;
use experimental 'signatures';

use Romeo::Util::IO qw< open_fh >;

requires qw< config residual_args >;

sub open_input ($self, $file) {
   open_fh($file, '<', $self->config('input_encoding'));
}

sub open_output ($self, $file) {
   open_fh($file, '>', $self->config('output_encoding'));
}

sub ofh ($self) {
   return $self->{filter}{ofh} //=
      $self->open_output($self->config('output'));
}

sub _inputs ($self) {
   return $self->{filter}{inputs} //= do {
      my $cmdline = $self->config('input') // [];
      my @inputs = ref($cmdline) ? $cmdline->@* : $cmdline;
      @inputs = ('-') unless @inputs;

      # make sure STDIN does not appear twice or more times
      my $n_stdin = grep { $_ eq '-' } @inputs;
      ouch 400, "cannot read standard input ('-') $n_stdin times"
        if $n_stdin > 1;

      \@inputs;
   };
} ## end sub _inputs

sub slurp ($self, $n = -1) {
   my @chunks;
   while ((my $fh = $self->next_ifh) && ($n != 0)) {
      local $/;
      push @chunks, <$fh> // ouch 500, "readline(): $OS_ERROR";
      close($fh) or ouch 500, "close(): $OS_ERROR";
      --$n if $n > 0;
   }
   return unless @chunks;
   return join '', @chunks;
}

sub spew ($self, $data) {
   my $fh = $self->ofh;
   print {$fh} $data or ouch 500, "print(): $OS_ERROR";
   close($fh) or ouch 500, "close(): $OS_ERROR";
   return;
}

sub n_inputs ($self) { return scalar $self->_inputs->@* }

sub current_ifh ($self) { return $self->{filter}{current_ifh} }

sub current_input_index ($self) { $self->{filter}{current_idx} //= -1 }

sub current_input ($self) {
   my $idx = $self->current_input_index;
   my $inputs = $self->_inputs;
   return $inputs->[$idx] if (0 <= $idx) && ($idx <= $inputs->$#*);
   return undef;
}

sub next_ifh ($self) {
   my $inputs = $self->_inputs;

   my $i = $self->current_input_index + 1;
   return if $i > $inputs->@*;
   $self->{filter}{current_idx} = $i;
   return if $i > $inputs->$#*;

   my $ienc = $self->config('input_encoding');
   return $self->{filter}{current_ifh} =
     open_fh($inputs->[$i], '<', $ienc);
} ## end sub next_ifh

sub ifh ($self) { $self->current_ifh // $self->next_ifh }

sub filter_inputs_to_output ($self, $method = 'filter') {
   my $ofh = $self->ofh;

   $method = $self->can($method) unless ref($method) eq 'CODE';

   my $n_inputs = $self->n_inputs;
   $self->$method($self->next_ifh, $ofh, $_, $n_inputs)
     for 0 .. ($n_inputs - 1);

   return 0;
} ## end sub filter_inputs_to_output

sub input_output_options ($self, @feats) {
   @feats = qw< input input_encoding output output_encoding >
      unless @feats;
   my %pass = map { $_ => 1 } @feats;
   return grep {
      my $name = $_->{_name} // $_->{getopt} =~ s{\|.*}{}rmxs;
      $pass{$name};
   } (
      {
         _name   => 'single-input',
         getopt  => 'input|i=s',
         help    => 'input path (supports "-" for standard input)',
         default => '-',
      },
      {
         getopt  => 'input|i=s@',
         help    => 'input path(s) (supports "-" for standard input)',
         default => [],
      },
      {
         getopt => 'input_encoding|input-encoding|I=s',
         help   => 'set input encoding',
      },
      {
         getopt  => 'output|o=s',
         help    => 'output path (supports "-" for standard output)',
         default => '-',
      },
      {
         getopt  => 'output_encoding|output-encoding|O=s',
         help    => 'set output encoding',
         default => 'UTF-8',
      },
      {
         _name   => '_output_encoding_raw',
         getopt  => 'output_encoding|output-encoding|O=s',
         help    => 'set output encoding',
         default => ':raw',
      },
      {
         _name   => '_output_encoding_none',
         getopt  => 'output_encoding|output-encoding|O=s',
         help    => 'set output encoding',
      },
   );
} ## end sub input_output_options

1;
