#!/usr/bin/env perl
package Romeo::Role::DescriptionFromPod;
use v5.24;
use Role::Tiny;
use experimental 'signatures';

sub description ($self) {
   $self->{description} //= do {
      require Pod::Usage;
      require Pod::Find;
      require Scalar::Util;

      # input handling
      my $package = Scalar::Util::blessed($self);
      my $src = Pod::Find::pod_where({-inc => 1}, $package);
      if (! defined($src)) {  # assume we're mo-bundled
         my $filename = "$package.pm" =~ s{::}{/}rgmxs;
         my $stuff = $INC{$filename};
         $src = $stuff->(x => $filename);
      }

      # output handling
      my $description = '';
      open my $ofh, '>:encoding(utf-8)', \$description;
      Pod::Usage::pod2usage(
         -noperldoc => 1,
         -exitval => 'noexit',
         -verbose => 99,
         -sections => 'DESCRIPTION',
         -output => $ofh,
         -input => $src,
      );
      $description =~ s{\A.*?$}{}rmxs;
   };
}

1;
