#!/usr/bin/env perl
package Romeo::CmdGpgKeyList;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch ':trytiny_var';
use Romeo::Util::Transform qw< encode_json >;
use Romeo::Util::Choose qw< choose_items >;
use Romeo::Util::IO qw< open_fh >;
use Capture::Tiny qw< capture_stdout >;
use IO::File;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< gpg-key-list gpg-list-keys >],
   help    => 'filter gpg --list-keys --with-colons',
};

sub description ($self) {
   return <<'END';

Filter the output of gpg --list-keys --with-colons, producing a JSON as
output.

   # get them all
   $ gpg-key-list

   # select items interactively
   $ gpg-key-list --select  # also -S

   # also ask for confirmation and in case loop again
   $ gpg-key-list -S --confirm  # also -C

   # also indicate a text above the list of keys, e.g. to hint who we
   # should look for
   $ gpg-key-list -SC --info 'choose key for foo' # also -I

   # homedir is passed to the gpg command down on the line
   $ gpg-key-list -SCI 'choose key for foo' --homedir ~/.another-gnupg

   # as a last resort, input can be provided explicitly
   $ gpg --list-keys --with-colons >keylist.txt
   $ gpg-key-list -SCI 'choose key for foo' -i keylist.txt

END
}

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options(qw< output >),
      {
         getopt => 'confirm|C',
         help => 'print out selected items and ask confirmation',
      },
      {
         getopt => 'homedir|home|H=s',
         help => 'home directory for gpg',
      },
      {
         getopt => 'info|I=s',
         help => 'info printed before prompt',
      },
      {
         getopt  => 'input|i=s',
         help    => 'input path (supports "-" for standard input)',
      },
      {
         getopt => 'prompt|P=s',
         help => 'prompt for choosing items',
      },
      {
         getopt => 'select|S',
         help => 'interactively select a subset of available keys',
      },
   );
}

sub get_raw_data_input ($self, $input) {
   my $fh = open_fh($input, '<');
   chomp(my @retval = readline($fh));
   return \@retval;
}

sub get_raw_data ($self) {
   if (defined(my $input = $self->config('input'))) {
      return $self->get_raw_data_input($input);
   }

   my @command = qw< gpg --list-keys --with-colons >;
   if (defined(my $homedir = $self->config('homedir'))) {
      push @command, '--homedir', $homedir;
   }
   my ($stdout, $exit) = capture_stdout {
      system {$command[0]} @command;
   };
   ouch 400, 'failure to run gpg, bailing out' if $exit;
   return [ split m{\r?\n}mxs, $stdout ];
}

sub printout ($self, $data) {
   my $fh = open_fh($self->config('output'), '>');
   print {$fh} encode_json($data);
}

sub parse ($self, $raw_data) {
   my @records;
   my $skip = 1;
   for my $line ($raw_data->@*) {
      my @fields = split m{:}mxs, $line;
      if ($fields[0] eq 'pub') {
         push @records, { id => $fields[4] };
         $skip = 0;
      }
      next if $skip;
      if ($fields[0] eq 'fpr') {
         $records[-1]{fingerprint} = $fields[9];
      }
      elsif ($fields[0] eq 'uid') {
         $records[-1]{description} = $fields[9];
         $skip = 1;
      }
      elsif ($fields[0] eq 'sub') {
         $skip = 1;
      }
      else {} # nothing to be done
   }
   return \@records;
}

sub interactive_select ($self, $data) {
   my $labels = [ map { $_->{description} } $data->@* ];
   my %opts = (
      confirm => $self->config('confirm'),
      main => {},
   );
   for my $feature (qw< prompt info >) {
      defined(my $value = $self->config($feature)) or next;
      $opts{main}{$feature} = $value;
   }
   my $chosen = choose_items($labels, \%opts);
   return [ $data->@[$chosen->@*] ];
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   my $data = $self->parse($self->get_raw_data);
   $data = $self->interactive_select($data) if $self->config('select');
   $self->printout($data);

   return 0;
}

1;
