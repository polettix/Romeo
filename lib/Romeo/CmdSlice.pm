#!/usr/bin/env perl
package Romeo::CmdSlice;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO Romeo::Role::Slice >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< slice >],
   help    => 'Slice input data and keep tasty samples',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      $self->slice_options,
      {
         getopt => 'interactive|I',
         help => 'interactively build the selection mapping',
         default => undef,
      },
      {
         getopt  => 'skip|skip-missing|s!',
         help    => 'skip missing elements (otherwise set as undef)',
         default => 0,
      },
   );
} ## end sub options

sub execute ($self) {
   return 0 unless $self->handle_help;

   my $definitions = $self->load_slicing_definitions;
   my $input = $self->load_input;

   ($definitions, $input) = $self->choose($definitions, $input)
      if $self->config('interactive');

   my $sliced = $self->slice($definitions, $input, $self->config('skip'));

   my $format = $self->get_format;
   $format = 'json' if $format eq 'auto';
   my $ofh = $self->ofh;
   print {$ofh} $format eq 'json' ? $self->_json($sliced) : Dump($sliced);

   return 0;
} ## end sub execute

sub description ($self) {
   return <<'END';

This command allows "slicing" input data to only retain a subset, possibly
rearranged.

   $ cat data.json
   {
      "foo": "bar",
      "array": ["one", "two", "three" ],
      "hash": {
         "sub_hash": {
            "baz": "this is important",
            "galook": "this is not"
         },
         "other-stuff": [ 1, 2, 3 ]
      }
   }

   # estract only neeeded data
   $ script/romeo slice -i tmp/data.json foo hash.sub_hash.baz
   {
      "foo" : "bar",
      "hash" : {
         "sub_hash" : {
            "baz" : "this is important"
         }
      }
   }

   # move hash.sub_hash.baz up and rename to bar
   $ script/romeo slice -i tmp/data.json foo bar:hash.sub_hash.baz
   {
      "bar" : "this is important",
      "foo" : "bar"
   }

   # emerge to top level and keep last name part with "@"
   $ script/romeo slice -i tmp/data.json foo @:hash.sub_hash.baz
   {
      "baz" : "this is important",
      "foo" : "bar"
   }

END
} ## end sub description

1;
