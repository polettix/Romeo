#!/usr/bin/env perl
package Romeo::CmdTeePee;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use YAML::Tiny        qw< Load Dump >;
use Template::Perlish qw< render traverse >;
use Ouch;

use Romeo::Util::IO 'open_fh';
use Romeo::Util::Transform qw< decode_json encode_json >;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< teepee tp >],
   help    => 'Render Template::Perlish templates from JSON data',
};

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,
      {
         getopt => 'define|set|d|s=s@',
         help   => 'set value for a variable',
      },
      {
         getopt  => 'format|f=s',
         help    => 'format of data input(s)',
         default => 'json',
      },
      {
         getopt => 'function|F=s',
         help   => 'generate template from function',
      },
      {
         getopt => 'include|I=s@',
         help   => 'add path to @INC for loading modules',
      },
      {
         getopt => 'json|j=s',
         help   => 'JSON input provided directly on command-line',
      },
      {
         getopt => 'loop|l!',
         help   => 'loop template over all inputs',
      },
      {
         getopt => 'module|M=s@',
         help   => 'import module and functions',
      },
      {
         getopt => 'newline|n!',
         help   => 'ensure a newline as the last printed character',
      },
      {
         getopt => 'no_input|no-input|N',
         help   => q{do not look for input JSON data (same as -j '')},
      },
      {
         getopt => 'template|t=s',
         help   => 'template file'
      },
      {
         getopt  => 'template_encoding',
         help    => 'set encoding of input template',
         default => 'UTF-8',
      },
      {
         getopt => 'template_text|T=s',
         help   => 'template (as direct string from command line)',
      },
      {
         getopt => 'variable|v=s',
         help   => 'generate template as single variable expansion',
      },
      {
         getopt => 'yaml|y=s',
         help   => 'YAML input provided directly on command-line',
      },
   );
} ## end sub options

sub load_data ($self) {
   my $data = $self->{data} = sub {
      return {} if $self->config('no_input');
      if (defined(my $json = $self->config('json'))) {
         return decode_json($json);
      }

      if (defined(my $yaml = $self->config('yaml'))) {
         return Load($yaml);
      }

      if ($self->{template_from_stdin}) {
         my @rs = $self->config('input')->@*;
         ouch 400, 'standard input already used for template'
           if (@rs == 0) || (grep { $_ eq '-' } @rs);
      }

      my $format = $self->config('format');
      ouch 400, "invalid format '$format'"
        unless $format =~ m{\A(?: json | yaml )\z}imxs;
      $format = lc($format);
      my $overall;
      while (defined(my $ifh = $self->next_ifh)) {
         local $/;
         my $str  = <$ifh>;
         my $data = $format eq 'json' ? decode_json($str) : Load($str);
         if (!defined($overall)) {
            my $r = ref($data);
            ouch 400, 'invalid input, either hash or array accepted'
              if ($r ne 'HASH') && ($r ne 'ARRAY');
            $overall = $data;
         } ## end if (!defined($overall))
         elsif (ref($overall) ne ref($data)) {
            ouch 400, 'incompatible multiple JSON inputs';
         }
         elsif (ref($overall) eq 'ARRAY') {
            push $overall->@*, $data->@*;
         }
         else {
            $overall->%* = ($overall->%*, $data->%*);
         }
      } ## end while (defined(my $ifh = ...))
      return $overall;
     }
     ->() // ouch 400, 'no data';

   my $cmdline = $self->config('define') // [];
   for my $definition ($cmdline->@*) {
      my ($path, $value) = split m{[:=]}mxs, $definition, 2;
      $path =~ s{%(..)}{chr(hex($1))}egmxs;    # percent-decode the path
      ${traverse(\$data, $path)} = $value;
   }

   return $self;
} ## end sub load_data

sub load_template ($self) {
   $self->{template_from_stdin} = 0;      # initialize as default
   $self->{template}            = sub {
      if (defined(my $v = $self->config('variable'))) {
         return "[% $v %]";
      }
      if (defined(my $f = $self->config('function'))) {
         return "[%= $f(\$V); %]";
      }
      my $te = $self->config('template_encoding');
      if (defined(my $tt = $self->config('template_text'))) {
         require Encode;
         return Encode::decode($te, $tt);
      }
      if (defined(my $tf = $self->config('template'))) {
         $self->{template_from_stdin} = $tf eq '-';
         my $fh = open_fh($tf, '<', $te);
         local $/;
         return <$fh>;
      } ## end if (defined(my $tf = $self...))
      return undef;
     }
     ->() // ouch 400, 'no template';
   return $self;
} ## end sub load_template

sub load_modules ($self) {
   my $includes = $self->config('include') // [];
   local @INC = @INC;
   push @INC, $includes->@*;

   my $functions = $self->{functions} = {
      JSON => \&encode_json,
      YAML => \&Dump,
   };
   my $modules = $self->config('module') // [];
   for my $module ($modules->@*) {
      my ($name, $funcs) = split m{=}mxs, $module, 2;

      my $file = "$name.pm" =~ s{::}{/}rgmxs;
      require $file;

      next unless length($funcs // '');
      for my $func (split m{,}mxs, $funcs) {
         my ($orig, $injected) = split m{:}mxs, $func, 2;
         $injected = $orig unless length($injected // '');
         $functions->{$injected} = $name->can($orig);
      }
   } ## end for my $module ($modules...)

   return $self;
} ## end sub load_modules

sub execute ($self) {
   return 0 unless $self->handle_help;

   # prepare everything
   $self->load_template->load_data->load_modules;
   my ($template, $data, $functions) =
     $self->@{qw< template data functions >};

   my $do_loop = $self->config('loop') && ref($data) eq 'ARRAY';
   my $ensure_newline = $self->config('newline');

   my $oe   = $self->config('output_encoding');
   my $text = join '', map {
      my $rendered = render(
         $template,
         $_,
         {
            binmode   => ":encoding($oe)",
            functions => $functions,
         }
      );
      $rendered .= "\n" if $ensure_newline && $rendered !~ m{\n\z}mxs;
      $rendered;
   } $do_loop ? $data->@* : $data;


   my $ofh = $self->ofh;
   binmode $ofh, ':raw';    # should already be encoded
   print {$ofh} $text;

   return 0;
} ## end sub execute

sub description ($self) {
   return <<'END';

Reduced but anyway working subset of teepee
(https://github.polettix.it/teepee/)

Examples:

   # defaults to handling JSON
   $ cat foo.json | romeo tp -T '[% this %] does [% that %]'

   # can handle YAML too
   $ cat foo.yaml | romeo tp -f yaml -T '[% this %] does [% that %]'

   # take directly from command line (JSON works best)
   $ romeo tp -T 'hey [% you %]' -j '{"you": "bar"}'
   $ romeo tp -T 'hey [% you %]' -y "$YAML_STRING"

   # sorry, no mixing of JSON and YAML stuff!

   # define vars on the fly
   $ romeo tp -T 'hey [% you %]!' -d you=Foo <other.json

   # avoid feeding structured data completely with -N
   $ romeo tp -T 'hey [% you %]!' -N -d you=Foo

   # ensure a newline with -n
   $ romeo tp -T 'hey [% you %]!' -n -N -d you=Foo

   # pretty-print
   $ romeo tp -FYAML <stuff.json
   $ romeo tp -FJSON <stuff.json

   # extract variable
   $ romeo tp -v foo <input.json

   # import modules and functions, with optional rename
   $ romeo tp -T '[%= b64(V"foo") %]' <in.json \
      -M MIME::Base64=encode_base64:b64

END
} ## end sub description

1;
