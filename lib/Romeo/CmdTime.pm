#!/usr/bin/env perl
package Romeo::CmdTime;
use v5.24;
use warnings;
use experimental 'signatures';
use utf8;
use POSIX 'strftime';
use Time::Local qw< timelocal timegm >;

use Ouch;

use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;

use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< time >],
   help    => 'Convert times depending on needs',
};

use constant EPOCH_OFFSET_HNANO  => 116444736000000000;
no warnings 'portable';
use constant AD_NEVER => 0x7fff_ffff_ffff_ffff;

sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options,

      {
         getopt => 'match|m=s@',
         help => 'use regex for detecting input to transform',
         default => [],
      },
      {
         getopt => 'input_format|input-format|f=s',
         help => 'one of ad|iso|local|gm|epoch|dwim',
         default => 'dwim',
      },
      {
         getopt => 'offset|D=s',
         help => 'apply offset to every input time',
      },
      {
         getopt => 'output_format|output-format|t=s',
         help => 'one of ad|iso|local|gm|epoch|t',
      },
   );
}

sub execute ($self) {
   return 0 unless $self->handle_help;

   return $self->filter_inputs_to_output;
}

sub validate ($self) {
   return if $self->help_requested;

   my $if = $self->config('input_format');
   ouch 400, "invalid input format <$if>"
      unless grep { $if eq $_ } qw< ad iso local gm epoch dwim >;

   if (defined(my $of = $self->config('output_format'))) {
      ouch 400, "invalid output format <$of>"
         unless grep { $of eq $_ } qw< ad iso local gm epoch t >;
   }
}

sub commit ($self) {
   return if $self->help_requested;

   my $if = $self->config('input_format');
   $self->set_config(input_format => ($if = 'local'))
      if $if eq 'iso';

   state $default_of_for = {
      ad => 'local',
      local => 'epoch',
      gm => 'epoch',
      epoch => 'local',
      dwim => 'local',
   };
   my $of = $self->config('output_format') // $default_of_for->{$if};
   $of = 'local' if $of eq 'iso';
   $self->set_config(output_format => $of);

   if (my @cmdline = $self->residual_args) {
      my $text = join "\n", (@cmdline, '');
      push $self->config('input')->@*, \$text;
   }

   return $self;
}

sub filter ($self, $ifh, $ofh, @rest) {
   my ($if, $of, $offset, $match) =
      $self->config(qw< input_format output_format offset match >);

   my $to_epoch = $self->can("__${if}_to_epoch")
      or die "unsupported transformation from <$if>";
   my $to_output = $self->can("__epoch_to_${of}")
      or die "unsupported transformation to <$of>";
   my $transform = sub ($input) {
      my $epoch = $to_epoch->($input =~ s{\A\s+|\s+\z}{}rgmxs);
      $epoch = __offset($epoch, $offset) if $offset;
      return $to_output->($epoch);
   };

   if ($match->@*) {
      while (defined(my $line = <$ifh>)) {
         my ($pre, $inner, $post) = $line =~ m{\A(\s*)(.*?)(\s*)\z}mxs;

         for my $m ($match->@*) {
            my $transformed = $inner =~ s{($m)}{$transform->($1)}rgemxs;
            if ($transformed ne $inner) {
               $inner = $transformed;
               last;
            }
         }

         print {$ofh} $pre, $inner, $post;
      }
   }
   else { # easy thing, everything is 
      while (defined(my $line = <$ifh>)) {
         say {$ofh} $transform->($line);
      }
   }
   return $self;
}

sub __epoch_to_ad ($epoch) {
   return 0 if $epoch < 0;
   return ($epoch * 10_000_000 + EPOCH_OFFSET_HNANO);
}

sub __epoch_to_local ($epoch) {
   return '<error>' if $epoch < 0;
   return strftime('%Y-%m-%dT%H:%M:%S%z', localtime($epoch));
}

sub __epoch_to_gm ($epoch) {
   return '<error>' if $epoch < 0;
   return strftime('%Y-%m-%dT%H:%M:%S+0000', gmtime($epoch));
}

sub __epoch_to_epoch ($epoch) { $epoch }

sub __epoch_to_t ($epoch) {
   state $mods = [
      [ s => 60, 2 ],
      [ m => 60, 2 ],
      [ h => 24, 2 ],
   ];
   my $retval = '';
   for my $spec ($mods->@*) {
      last unless $epoch;
      my ($unit, $mod, $len) = $spec->@*;
      my $value = $epoch % $mod;
      $retval = sprintf("%0${len}d$unit", $value) . $retval
         if length($retval) || $value > 0;
      $epoch = ($epoch - $value) / $mod;
   }
   $retval = sprintf('%dd', $epoch) . $retval if $epoch;
   return $retval;
}

sub __ad_to_epoch ($adtime) {
   return -1 if $adtime < EPOCH_OFFSET_HNANO || $adtime >= AD_NEVER;
   return (($adtime - EPOCH_OFFSET_HNANO) / 10_000_000);
}

sub __local_to_epoch ($ts) {
   my ($timeref, $tz) = __iso_parse($ts);
   return __specific_to_epoch($timeref, $tz) if defined $tz;
   return timelocal($timeref->@*);
}

sub __gm_to_epoch ($ts) {
   my ($timeref, $tz) = __iso_parse($ts);
   return __specific_to_epoch($timeref, $tz) if defined $tz;
   return timegm($timeref->@*);
}

sub __offset ($base, $offset) {
   $offset = lc($offset);
   my $sign_factor = 1;
   while (length($offset)) {
      my $sign = $offset =~ m{\A [-+]}mxs ? substr $offset, 0, 1, '' : '=';
      $sign_factor = $sign eq '-' ? -1 : $sign eq '+' ? 1 : $sign_factor;
      my ($amount, $unit) = $offset =~ m{\A (\d+) ([smhdw])}mxs
         or die "invalid offset\n";
      substr($offset, 0, length($amount) + length($unit), '');
      $amount =~ s{\A 0+}{}mxs;
      $amount ||= 0;
      if ($unit eq 's') {
         $base += $sign_factor * $amount;
      }
      elsif ($unit eq 'm') {
         $amount *= 60;
         $base += $sign_factor * $amount;
      }
      elsif ($unit eq 'h') {
         $amount *= 3600;
         $base += $sign_factor * $amount;
      }
      else { # advance preserving the hour of the day... if possible!
         $amount *= 7 if $unit eq 'w'; # 7 days per week

         # save the initial "offset inside the day"
         my ($ss, $sm, $sh) = localtime($base);
         my $exp_day_offset = $ss + 60 * ($sm + 60 * $sh);

         # approximate the jump
         $amount *= 24 * 3600; # tentative days to seconds
         $base += $sign_factor * $amount;

         # adjust by landing offset
         my ($ls, $lm, $lh) = localtime($base);
         my $got_day_offset = $ls + 60 * ($lm + 60 * $lh);
         $base += ($exp_day_offset - $got_day_offset) % (24 * 3600);
      }
   }

   return $base;
}

sub __today_to_epoch {
   my ($second, $minute, $hour, $day, $em, $ey) = localtime;
   timelocal(0, 0, 0, $day, $em, $ey);
}

sub __dwim_to_epoch ($ts) {
   die "no input\n" unless $ts =~ m{\S}mxs;

   $ts =~ s{\A\s+|\s+\z}{}gmxs;
   return time() if $ts eq 'now';

   my ($start, $offset) = lc($ts) =~ m{
      \A 
         (
            (?:
                  now | yesterday | today | tomorrow  # really dwim stuff
               |  iso(?: -? 8601)?[:/=] .+?           # ISO-8601
               |  ad              [:/=] .+?           # Active Directory
               |  \d+                                 # AD or epoch
               |  .+?                                 # anything else
            )?
         )
         ( (?: [-+] (?: \d+ [smhdw])+ )* )             # offset
      \z
   }imxs or die "invalid dwim input <$ts>";
   $start = 'now' unless length($start);

   my $base = $start eq 'now' ? time()
   : $start eq 'yesterday'    ? __offset(__today_to_epoch(), '-1d')
   : $start eq 'today'        ? __today_to_epoch()
   : $start eq 'tomorrow'     ? __offset(__today_to_epoch(), '+1d')
   : $start =~ m(\A iso.*?[:/=] (.+?) =? \z)mxs ? __local_to_epoch($1)
   : $start =~ m{\A ad    [:/=] (.+)}mxs        ? __ad_to_epoch($1)
   : $start =~ m{\D}mxs                         ? __local_to_epoch($start)
   : length($start) >= 18                       ? __ad_to_epoch($start)
   :                                              0 + $start;

   return $base unless length($offset // '');

   return __offset($base, $offset);
}

sub __iso_parse ($ts) {
   my ($date, $time, $tz) = $ts =~ m{
      \A
         (\d{8} | \d{4} [-/] \d\d [-/] \d\d)
         (?:
            [tT\ ]
            (\d{6} | \d\d   :  \d\d   :  \d\d)
            ( [zZ] | [-+] \d\d (?:\d\d)? )?
         )?
      \z
   }mxs or die "invalid ISO8601 input data";
   my ($year, $month, $day) = $date =~ m{(\d{4})\D?(\d\d)\D?(\d\d)}mxs;
   my ($hour, $minute, $second) =
      ($time || '00:00:00') =~ m{(\d\d):?(\d\d):?(\d\d)}mxs;

   return (
      [$second + 0, $minute + 0, $hour + 0, $day + 0, $month - 1, $year],
      $tz
   );
}

sub __specific_to_epoch ($timeref, $tz) {
   my $base = timegm($timeref->@*);
   $tz = '+0000' if lc($tz) eq 'z';
   my ($sign, $oh, $om) = $tz =~ m{\A ([-+]) (\d\d) (\d\d)? \z}mxs;
   my $offset = 60 * ($oh * 60 + ($om // 0));
   $offset = - $offset if $sign eq '+'; # go backwards
   return $base + $offset;
}

sub description ($self) {
   return <<'END'

This command converts time between different representations.

As dealing with calendars is inherently difficult, this program mainly
sticks with supporting only what came since Unix Epoch 0 on, which should be
good for most day-by-day needs.

Inputs are read from standard input or from files provided with the
"-i"/"--input" command line option (multiple values are accepted). They are
interpreted according to the "f"/"--input-format" option:

- "local" and "gm" accept most of ISO8601 variants, defaulting to the local
  time zone or GMT if a time zone is missing in the input (they are
  otherwise equivalent)
- "epoch" is read as a Unix Epoch integer
- "ad" is read as an Active Directory timestamp integer
- "dwim" supports *Do What I Mean* inputs, i.e.:
   - literal strings "now", "today", "yesterday", and "tomorrow"
   - a string of shape "iso:..." with an ISO8601 string (not all forms are
     supported)
   - epoch values (if a sequence of integers)
   - variants of the above strings with an offset (see below)

The "DWIM" input format overlaps some of the other ones, allowing to specify
an offset from that starting point. So, "now+3h" means three hours since...
now. The following is a way of figuring out the Active Directory timestamp
of some instants

   $ romeo time -f dwim -t ad now today yesterday tomorrow tomorrow+7d

   133238201500000000
   133237404000000000
   133236540000000000
   133238268000000000
   133245144000000000

One interesting thing is converting from Active Directory timestamps to
human-readable local times:

   $ printf 133238201500000000 | romeo time -f ad

   2023-03-20T22:09:10+0100

Output formats include "ad", "iso", "local" (same as "iso"), "gm" (outputs
ISO8601 for the GMT time zone), and "epoch".

It's possible to set offsets as strings composed of chunks with:

- an optional sign, defaulting to "+" (the first one might be required)
- an offset amount as a positive integer
- a unit of measure between "s"(econds), "m"(inutes), "h"(ours), "d"(ays),
  and "w"(eeks).

Seconds, minutes and hours are added at face value.

Days and weeks offsets are applied taking into account the local timezone.
In other terms, if today is CET and tomorrow is CEST, adding one day
starting at 4PM today lands us to 4PM tomorrow.

Offsets can be applied to any individual input when the input format is
"dwim", or applied to all inputs with option "-D"/"--offset".

Non-option command line arguments are collected as individual lines and used
as inputs in addition to the rest.

With option "-m"/"--match", it's possible to set one or more matching
regular expressions; this makes it possible to transform the input in a
sed-like fashion, like e.g. the following:

   $ printf "let's meet at 133238201500000000\\nOK?\\n" \
      | romeo time -m '\d{15,}' -f ad

   let's meet at 2023-03-20T22:09:10+0100
   OK?

END
}

1;
