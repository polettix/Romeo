#!/usr/bin/env perl
package Romeo::CmdArgs2Json;
use v5.24;
use warnings;
use experimental 'signatures';
use Ouch;
use English;
use utf8;
 
use Template::Perlish qw< traverse >;
 
use Romeo::Util::Transform
  qw< encode_json encode_json_compact url_decode >;
 
use Role::Tiny::With;
with qw< Romeo::Role::Help Romeo::Role::IO >;
 
use App::Easer::V2 dd => -command => -spec => {
   aliases => [qw< args2json >],
   help    => 'Turn arguments into a JSON (file)',
};
 
sub options ($self) {
   return (
      $self->handle_help_options,
      $self->input_output_options('output'),
      {
         getopt  => 'compact|shrink!',
         help    => 'print out compact JSON (not pretty)',
         default => 0,
      },
      {
         getopt  => 'urlencoded|url-encoded|u!',
         help    => 'input definitions are url-encoded',
         default => 0,
      },
   );
} ## end sub options
 
sub execute ($self) {
   return 0 unless $self->handle_help;
 
   my $url_encoded = $self->config('urlencoded');
 
   my $retval = {};
   for my $arg ($self->residual_args) {
      my ($key, $value) = split m{[:=]}mxs, $arg, 2;
 
      # ensure there is a leading "root" element to always start in a hash
      $key =~ s{\A \.? }{root.}mxs; # ensure a leading element
      my $ref_to_value = traverse(\$retval, $key);
 
      # some command-line values are special, like '[]' or '{}' that allow
      # inserting empty arrays/hashes. This is important if hash keys are
      # valid non-negative integers.
      $value = $value eq '[]' ? []
         : $value eq '{}'     ? {}
         : $url_encoded       ? url_decode($value)
         :                      $value; # keep as-is
      $$ref_to_value = $value;
   }
   
   my $encoder =
     $self->config('compact') ? \&encode_json_compact : \&encode_json;
   $self->spew($encoder->($retval->{root}));
 
   return 0;
}
 
1;
