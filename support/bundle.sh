#!/bin/sh
set -eu
md="$(dirname "$(readlink -f "$0")")"
rd="$(dirname "$md")"
PERL5LIB="$rd/lib:$rd/bundle-lib" mobundle -LPo romeo \
   $(
      find "$rd/bundle-lib" -type f -name '*.pm' \
      | sed -e 's#^.*bundle-lib/#-m #;s#/#::#g;s#\.pm$##' \
      | sort
   ) \
   $(
      find "$rd/lib" -type f -name '*.pm' \
      | sed -e 's#^.*lib/Romeo#-m Romeo#;s#/#::#g;s#\.pm$##' \
      | sort
   ) \
   "$rd/script/romeo"
if [ "${1:-""}" = 'commit' ] ; then
   cd "$rd"
   git commit romeo -m "Update bundled program"
fi
