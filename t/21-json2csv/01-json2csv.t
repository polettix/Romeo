#!/usr/bin/env perl
use v5.24;
use warnings;
use experimental 'signatures';

use Test::More;
use Test::Output;
use Path::Tiny;
use JSON::PP 'decode_json';

use Romeo;

my $dir = path(__FILE__)->parent;

my $romeo = Romeo->new;
my $exit;

my @specs = sort { $a->[0] cmp $b->[0] }
   grep { defined($_->[0]) }
   map {
      my ($id) = $_->basename =~  m{\A (test-\d+) \.spec \z}mxs;
      [$id, $_];
   } $dir->children;

for my $spec (@specs) {
   my ($id, $spec_file) = $spec->@*;
   my $spec = decode_json($spec_file->slurp_raw);
   my @cmdline = map { ref($_) ? $dir->child($_->[0])->stringify : $_ }
      $spec->{cmdline}->@*;

   my $output = $spec->{output} // ["$id.out"];
   $output = $dir->child($output->[0])->slurp_raw if ref $output;

   my $name = $spec->{name} // 'id';
   stdout_is(
      sub { $exit = $romeo->run($0, @cmdline) },
      $output,
      "$name: output",
   );
   is $exit, 0, "$name: exit code";
}


done_testing();
