requires 'perl',               '5.024000';
requires 'App::Easer',         '2.008';
requires 'Capture::Tiny',      '0.48';
requires 'Data::HexDump::XXD', '0.001001';
requires 'Email::Address',     '1.913';
requires 'MIME::Entity',       '5.510';
requires 'Ouch',               '0.0501';
requires 'Path::Tiny',         '0.084';
requires 'Role::Tiny',         '2.002004';
requires 'Template::Perlish',  '1.64';
requires 'Term::ANSIColor',    '5.01';
requires 'Term::Choose',       '1.757';
requires 'Text::CSV',          '2.02';
requires 'Try::Catch',         '1.1.0';
requires 'Validate::CodiceFiscale', '0.004';
requires 'YAML::Tiny',         '1.73';

on test => sub {
   requires 'Path::Tiny', '0.084';
   requires 'Test::Output', '1.034';
};

on develop => sub {
   requires 'Path::Tiny',          '0.084';
   requires 'Template::Perlish',   '1.52';
   requires 'Test::Pod::Coverage', '1.04';
   requires 'Test::Pod',           '1.51';
};
