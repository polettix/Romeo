use strict;
use warnings;
package Ouch;
$Ouch::VERSION = '0.0501';
use Carp qw(longmess shortmess);
use parent 'Exporter';
use overload bool => sub {1}, q{""} => 'scalar', fallback => 1;
use Scalar::Util qw(blessed);

our @EXPORT = qw(bleep ouch kiss hug barf);
our @EXPORT_OK = qw(try throw catch catch_all caught caught_all);
our %EXPORT_TAGS = ( traditional => [qw(try throw catch catch_all)], trytiny => [qw( throw caught caught_all )] );

sub import {
   my $package = shift;

   # filter out :trytiny_var from list of imports
   my ($alt, @imports);
   ($_ eq ':trytiny_var' ? ($alt = 1) : (push @imports, $_))
      for @_;

   # leave early if the modified interface is not of interest
   local $Exporter::ExportLevel = 1 + ($Exporter::ExportLevel || 0);
   return $package->SUPER::import(@imports) unless $alt;

   # we will default to $_ instead of $@ in the following, the real
   # import will still be done by Exporter but based on wrapped subs
   no warnings 'redefine';

   my $barf  = \&barf;
   local *barf  = sub { push @_, $_ if @_ < 1; goto $barf  };

   my $bleep = \&bleep;
   local *bleep = sub { push @_, $_ if @_ < 1; goto $bleep };

   my $hug   = \&hug;
   local *hug   = sub { push @_, $_ if @_ < 1; goto $hug   };

   my $kiss  = \&kiss;
   local *kiss  = sub { push @_, $_ if @_ < 2; goto $kiss  };

   use warnings 'redefine';

   # leave the stage to Exporter now
   $package->SUPER::import(@imports);
}

sub new {
  my ($class, $code, $message, $data) = @_;
  bless {code => $code, message => $message, data => $data, shortmess => shortmess($message), trace => longmess($message) }, $class;
}

sub try (&) {
  my $try = shift;
  eval { $try->() };
  return $@;
}

sub ouch {
  my ($code, $message, $data) = @_;
  my $self = __PACKAGE__->new($code, $message, $data);
  die $self;
}

sub throw {  # alias
  ouch @_;
}

sub kiss {
  my ($code, $e) = @_;
  $e = $@ if @_ < 2;
  if (blessed $e && $e->isa('Ouch') && $e->code eq $code) {
    return 1;
  }
  return 0;
}

sub catch {
  kiss @_;
}

sub caught {
  kiss @_;
}

sub hug {
  my $e = @_ ? $_[0] : $@;
  return $e ? 1 : 0;
}

sub catch_all {
  hug @_;
}

sub caught_all {
  hug @_;
}

sub bleep {
  my $e = @_ ? $_[0] : $@;
  if (blessed $e && $e->isa('Ouch')) {
    return $e->message;
  }
  else {
    my $message = "$e"; # force to string anyway
    if ($message =~ m{^(.*)\s+at\s.*line\s\d+.}xms) {
        return $1;
    }
    else {
        return $message;
    }
  }
}

sub barf {
    my $e = @_ ? $_[0] : $@;
    my $code;
    if (blessed $e && $e->isa('Ouch')) {
        $code = $e->code;
    }
    else {
        $code = 1;
    }

    print STDERR bleep($e)."\n";
    exit $code;
}

sub scalar {
  my $self = shift;
  return $self->{shortmess};
}

sub trace {
  my $self = shift;
  return $self->{trace};
}

sub hashref {
  my $self = shift;
  return {
    code    => $self->{code},
    message => $self->{message},
    data    => $self->{data},
  };
}

sub code {
  my $self = shift;
  return $self->{code};
}

sub message {
  my $self = shift;
  return $self->{message};
}

sub data {
  my $self = shift;
  return $self->{data};
}

1;
