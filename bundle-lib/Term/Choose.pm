package Term::Choose;

use warnings;
use strict;
use 5.10.0;

our $VERSION = '1.757';
use Exporter 'import';
our @EXPORT_OK = qw( choose );

use Carp qw( croak carp );

use Term::Choose::Constants       qw( :all );
use Term::Choose::LineFold        qw( line_fold print_columns cut_to_printwidth );
use Term::Choose::Screen          qw( :all );
use Term::Choose::ValidateOptions qw( validate_options );

my $Plugin;

BEGIN {
    if ( $^O eq 'MSWin32' ) {
        require Win32::Console::ANSI;
        require Term::Choose::Win32;
        $Plugin = 'Term::Choose::Win32';
    }
    else {
        require Term::Choose::Linux;
        $Plugin = 'Term::Choose::Linux';
    }
}

END {
    if ( $? == 255 ) {
        if( $^O eq 'MSWin32' ) {
            my $input = Win32::Console->new( Win32::Console::constant( "STD_INPUT_HANDLE",  0 ) );
            $input->Mode( 0x0001|0x0002|0x0004 );
            $input->Flush;
        }
        elsif ( TERM_READKEY ) {
            Term::ReadKey::ReadMode( 'restore' );
        }
        else {
            system( "stty sane" );
        }
        print "\n", clear_to_end_of_screen;
        print show_cursor;
    }
}


sub new {
    my $class = shift;
    my ( $opt ) = @_;
    croak "new: called with " . @_ . " arguments - 0 or 1 arguments expected" if @_ > 1;
    my $instance_defaults = _defaults();
    if ( defined $opt ) {
        croak "new: the (optional) argument must be a HASH reference" if ref $opt ne 'HASH';
        validate_options( _valid_options(), $opt, 'new' );
        for my $key ( keys %$opt ) {
            $instance_defaults->{$key} = $opt->{$key} if defined $opt->{$key};
        }
    }
    my $self = bless $instance_defaults, $class;
    $self->{backup_instance_defaults} = { %$instance_defaults };
    $self->{plugin} = $Plugin->new();
    return $self;
}


sub _valid_options {
    return {
        beep                => '[ 0 1 ]',
        clear_screen        => '[ 0 1 ]',
        codepage_mapping    => '[ 0 1 ]',
        hide_cursor         => '[ 0 1 ]',
        index               => '[ 0 1 ]',
        mouse               => '[ 0 1 ]',
        order               => '[ 0 1 ]',
        alignment           => '[ 0 1 2 ]',
        color               => '[ 0 1 2 ]',
        include_highlighted => '[ 0 1 2 ]',
        layout              => '[ 0 1 2 ]',
        page                => '[ 0 1 2 ]',
        search              => '[ 0 1 2 ]',
        keep                => '[ 1-9 ][ 0-9 ]*',
        ll                  => '[ 1-9 ][ 0-9 ]*',
        max_cols            => '[ 1-9 ][ 0-9 ]*',
        max_height          => '[ 1-9 ][ 0-9 ]*',
        max_width           => '[ 1-9 ][ 0-9 ]*',
        default             => '[ 0-9 ]+',
        pad                 => '[ 0-9 ]+',
        margin              => 'Array_Int',
        mark                => 'Array_Int',
        meta_items          => 'Array_Int',
        no_spacebar         => 'Array_Int',
        tabs_info           => 'Array_Int',
        tabs_prompt         => 'Array_Int',
        skip_items          => 'Regexp',
        empty               => 'Str',
        footer              => 'Str',
        info                => 'Str',
        prompt              => 'Str',
        undef               => 'Str',
        busy_string         => 'Str',
    };
};


sub _defaults {
    return {
        alignment           => 0,
        beep                => 0,
        clear_screen        => 0,
        codepage_mapping    => 0,
        color               => 0,
        #default            => undef,
        empty               => '<empty>',
        #footer             => undef,
        hide_cursor         => 1,
        include_highlighted => 0,
        index               => 0,
        info                => '',
        keep                => 5,
        layout              => 1,
        #ll                 => undef,
        #margin             => undef,
        #mark               => undef,
        #max_cols           => undef,
        #max_height         => undef,
        #max_width          => undef,
        #meta_items         => undef,
        mouse               => 0,
        #no_spacebar        => undef,
        order               => 1,
        pad                 => 2,
        page                => 1,
        #prompt             => undef,
        search              => 1,
        #skip_items         => undef,
        #tabs_info          => undef,
        #tabs_prompt        => undef,
        undef               => '<undef>',
        #busy_string        => undef,
    };
}


sub __copy_orig_list {
    my ( $self, $orig_list_ref ) = @_;
    if ( $self->{ll} ) {
        $self->{list} = $orig_list_ref;
    }
    else {
        $self->{list} = [ @$orig_list_ref ];
        if ( $self->{color} ) {
            $self->{orig_list} = $orig_list_ref;
        }
        for ( @{$self->{list}} ) {
            if ( ! $_ ) {
                $_ = $self->{undef} if ! defined $_;
                $_ = $self->{empty} if ! length $_;
            }
            if ( $self->{color} ) {
                s/\x{feff}//g;
                s/\e\[[\d;]*m/\x{feff}/g;
            }
            s/\t/ /g;
            s/\v+/\ \ /g;
            # \p{Cn} might not be up to date and remove assigned codepoints
            # therefore only \p{Noncharacter_Code_Point}
            s/[\p{Cc}\p{Noncharacter_Code_Point}\p{Cs}]//g;
        }
    }
}


sub __length_list_elements {
    my ( $self ) = @_;
    my $list = $self->{list};
    if ( $self->{ll} ) {
        $self->{col_width} = $self->{ll};
    }
    else {
        my $length_elements = [];
        my $longest = 0;
        for my $i ( 0 .. $#$list ) {
            $length_elements->[$i] = print_columns( $list->[$i] );
            $longest = $length_elements->[$i] if $length_elements->[$i] > $longest;
        }
        $self->{width_elements} = $length_elements;
        $self->{col_width} = $longest;
    }
}


sub __init_term {
    my ( $self ) = @_;
    my $config = {
        mode => 'ultra-raw',
        mouse => $self->{mouse},
        hide_cursor => $self->{hide_cursor},
    };
    $self->{mouse} = $self->{plugin}->__set_mode( $config );
}


sub __reset_term {
    my ( $self, $clear_choose ) = @_;
    if ( defined $self->{plugin} ) {
        $self->{plugin}->__reset_mode( { mouse => $self->{mouse}, hide_cursor => $self->{hide_cursor} } );
    }
    if ( $clear_choose ) {
        my $up = $self->{i_row} + $self->{count_prompt_lines};
        print up( $up ) if $up;
        print "\r" . clear_to_end_of_screen();
    }
    if ( exists $self->{backup_instance_defaults} ) {  # backup_instance_defaults exists if ObjectOriented
        my $instance_defaults = $self->{backup_instance_defaults};
        for my $key ( keys %$self ) {
            if ( $key eq 'plugin' || $key eq 'backup_instance_defaults' ) {
                next;
            }
            elsif ( exists $instance_defaults->{$key} ) {
                $self->{$key} = $instance_defaults->{$key};
            }
            else {
                delete $self->{$key};
            }
        }
    }
}


sub __get_key {
    my ( $self ) = @_;
    my $key;
    if ( defined $self->{skip_items} ) {
        my $idx = $self->{rc2idx}[$self->{pos}[ROW]][$self->{pos}[COL]];
        if ( $self->{list}[$idx] =~ $self->{skip_items} ) {
            $key = $self->Term::Choose::Opt::SkipItems::__key_skipped();
        }
    }
    if ( ! defined $key ) {
        $key = $self->{plugin}->__get_key_OS( $self->{mouse} );
    }
    return $key if ref $key ne 'ARRAY';
    return $self->Term::Choose::Opt::Mouse::__mouse_info_to_key( @$key );
}


sub __modify_options {
    my ( $self ) = @_;
    if ( defined $self->{max_cols} && $self->{max_cols} == 1 ) {
        $self->{layout} = 2;
    }
    if ( length $self->{footer} && $self->{page} != 2 ) {
        $self->{page} = 2;
    }
    if ( $self->{page} == 2 && ! $self->{clear_screen} ) {
        $self->{clear_screen} = 1;
    }
    if ( $self->{max_cols} && $self->{layout} == 1 ) {
        $self->{layout} = 0;
    }
    if ( ! defined $self->{prompt} ) {
        $self->{prompt} = defined $self->{wantarray} ? 'Your choice:' : 'Close with ENTER';
    }
    if ( defined $self->{margin} ) {
        ( $self->{margin_top}, $self->{margin_right}, $self->{margin_bottom}, $self->{margin_left} ) = @{$self->{margin}};
        if ( ! defined $self->{tabs_prompt} ) {
            $self->{tabs_prompt} = [ $self->{margin_left}, $self->{margin_left}, $self->{margin_right} ];
        }
        if ( ! defined $self->{tabs_info} ) {
            $self->{tabs_info} = [ $self->{margin_left}, $self->{margin_left}, $self->{margin_right} ];
        }
    }
}


sub choose {
    if ( ref $_[0] ne __PACKAGE__ ) {
        my $ob = __PACKAGE__->new();
        delete $ob->{backup_instance_defaults};
        return $ob->__choose( @_ );
    }
    my $self = shift;
    return $self->__choose( @_ );
}


sub __choose {
    my $self = shift;
    my ( $orig_list_ref, $opt ) = @_;
    croak "choose: called with " . @_ . " arguments - 1 or 2 arguments expected" if @_ < 1 || @_ > 2;
    croak "choose: the first argument must be an ARRAY reference" if ref $orig_list_ref ne 'ARRAY';
    if ( defined $opt ) {
        croak "choose: the (optional) second argument must be a HASH reference" if ref $opt ne 'HASH';
        validate_options( _valid_options(), $opt, 'choose' );
        for my $key ( keys %$opt ) {
            $self->{$key} = $opt->{$key} if defined $opt->{$key};
        }
    }
    if ( ! @$orig_list_ref ) {
        return;
    }
    local $\ = undef;
    local $, = undef;
    local $| = 1;
    if ( defined $self->{busy_string} ) {
        print "\r" . clear_to_end_of_line();
        print $self->{busy_string};
    }
    $self->{wantarray} = wantarray;
    $self->__modify_options();
    if ( $self->{mouse} ) {
        require Term::Choose::Opt::Mouse;
    }
    if ( $^O eq "MSWin32" ) {
        print $opt->{codepage_mapping} ? "\e(K" : "\e(U";
    }
    $self->__copy_orig_list( $orig_list_ref );
    $self->__length_list_elements();
    if ( defined $self->{skip_items} ) {
        require Term::Choose::Opt::SkipItems;
        $self->Term::Choose::Opt::SkipItems::__prepare_default();
    }
    if ( exists $ENV{TC_RESET_AUTO_UP} ) {
        $ENV{TC_RESET_AUTO_UP} = 0;
    }
    local $SIG{INT} = sub {
        $self->__reset_term();
        exit;
    };
    $self->__init_term();
    ( $self->{term_width}, $self->{term_height} ) = get_term_size();
    $self->__wr_first_screen();
    my $fast_page = 10;
    if ( $self->{pp_count} > 10_000 ) {
        $fast_page = 20;
    }
    my $saved_pos;

    GET_KEY: while ( 1 ) {
        my $key = $self->__get_key();
        if ( ! defined $key ) {
            $self->__reset_term( 1 );
            carp "EOT: $!";
            return;
        }
        $self->{pressed_key} = $key;
        my ( $new_width, $new_height ) = get_term_size();
        if ( $new_width != $self->{term_width} || $new_height != $self->{term_height} ) {
            if ( $self->{ll} ) {
                $self->__reset_term( 0 );
                return -1;
            }
            ( $self->{term_width}, $self->{term_height} ) = ( $new_width, $new_height );
            $self->__copy_orig_list( $orig_list_ref );
            $self->{default} = $self->{rc2idx}[$self->{pos}[ROW]][$self->{pos}[COL]];
            if ( $self->{wantarray} && @{$self->{marked}} ) {
                $self->{mark} = $self->__marked_rc2idx();
            }
            my $up = $self->{i_row} + $self->{count_prompt_lines};
            print up( $up ) if $up;
            print "\r" . clear_to_end_of_screen();
            $self->__wr_first_screen();
            next GET_KEY;
        }
        next GET_KEY if $key == NEXT_get_key;
        next GET_KEY if $key == KEY_Tilde;
        if ( exists $ENV{TC_RESET_AUTO_UP} && $ENV{TC_RESET_AUTO_UP} == 0 ) {
            if ( $key != LINE_FEED && $key != CARRIAGE_RETURN ) {
                $ENV{TC_RESET_AUTO_UP} = 1;
            }
        }
        my $page_step = 1;
        if ( $key == VK_INSERT ) {
            $page_step = $fast_page if $self->{first_page_row} - $fast_page * $self->{avail_height} >= 0;
            $key = VK_PAGE_UP;
        }
        elsif ( $key == VK_DELETE ) {
            $page_step = $fast_page if $self->{last_page_row} + $fast_page * $self->{avail_height} <= $#{$self->{rc2idx}};
            $key = VK_PAGE_DOWN;
        }
        if ( $saved_pos && $key != VK_PAGE_UP && $key != CONTROL_B && $key != VK_PAGE_DOWN && $key != CONTROL_F ) {
            $saved_pos = undef;
        }
        # $self->{rc2idx} holds the new list (AoA) formatted in "__list_idx2rc" appropriate to the chosen layout.
        # $self->{rc2idx} does not hold the values directly but the respective list indexes from the original list.
        # If the original list would be ( 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' ) and the new formatted list should be
        #     a d g
        #     b e h
        #     c f
        # then the $self->{rc2idx} would look like this
        #     0 3 6
        #     1 4 7
        #     2 5
        # So e.g. the second value in the second row of the new list would be $self->{list}[ $self->{rc2idx}[1][1] ].
        # On the other hand the index of the last row of the new list would be $#{$self->{rc2idx}}
        # or the index of the last column in the first row would be $#{$self->{rc2idx}[0]}.

        if ( $key == VK_DOWN || $key == KEY_j ) {
            if (     ! $self->{rc2idx}[$self->{pos}[ROW]+1]
                  || ! $self->{rc2idx}[$self->{pos}[ROW]+1][$self->{pos}[COL]]
            ) {
                $self->__beep();
            }
            else {
                $self->{pos}[ROW]++;
                if ( $self->{pos}[ROW] <= $self->{last_page_row} ) {
                    $self->__wr_cell( $self->{pos}[ROW] - 1, $self->{pos}[COL] );
                    $self->__wr_cell( $self->{pos}[ROW]    , $self->{pos}[COL] );
                }
                else {
                    $self->{first_page_row} = $self->{last_page_row} + 1;
                    $self->{last_page_row}  = $self->{last_page_row} + $self->{avail_height};
                    $self->{last_page_row}  = $#{$self->{rc2idx}} if $self->{last_page_row} > $#{$self->{rc2idx}};
                    $self->__wr_screen();
                }
            }
        }
        elsif ( $key == VK_UP || $key == KEY_k ) {
            if ( $self->{pos}[ROW] == 0 ) {
                $self->__beep();
            }
            else {
                $self->{pos}[ROW]--;
                if ( $self->{pos}[ROW] >= $self->{first_page_row} ) {
                    $self->__wr_cell( $self->{pos}[ROW] + 1, $self->{pos}[COL] );
                    $self->__wr_cell( $self->{pos}[ROW]    , $self->{pos}[COL] );
                }
                else {
                    $self->{last_page_row}  = $self->{first_page_row} - 1;
                    $self->{first_page_row} = $self->{first_page_row} - $self->{avail_height};
                    $self->{first_page_row} = 0 if $self->{first_page_row} < 0;
                    $self->__wr_screen();
                }
            }
        }
        elsif ( $key == KEY_TAB || $key == CONTROL_I ) { # KEY_TAB == CONTROL_I
            if (    $self->{pos}[ROW] == $#{$self->{rc2idx}}
                 && $self->{pos}[COL] == $#{$self->{rc2idx}[$self->{pos}[ROW]]}
            ) {
                $self->__beep();
            }
            else {
                if ( $self->{pos}[COL] < $#{$self->{rc2idx}[$self->{pos}[ROW]]} ) {
                    $self->{pos}[COL]++;
                    $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] - 1 );
                    $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] );
                }
                else {
                    $self->{pos}[ROW]++;
                    if ( $self->{pos}[ROW] <= $self->{last_page_row} ) {
                        $self->{pos}[COL] = 0;
                        $self->__wr_cell( $self->{pos}[ROW] - 1, $#{$self->{rc2idx}[$self->{pos}[ROW] - 1]} );
                        $self->__wr_cell( $self->{pos}[ROW]    , $self->{pos}[COL] );
                    }
                    else {
                        $self->{first_page_row} = $self->{last_page_row} + 1;
                        $self->{last_page_row}  = $self->{last_page_row} + $self->{avail_height};
                        $self->{last_page_row}  = $#{$self->{rc2idx}} if $self->{last_page_row} > $#{$self->{rc2idx}};
                        $self->{pos}[COL] = 0;
                        $self->__wr_screen();
                    }
                }
            }
        }
        elsif ( $key == KEY_BSPACE || $key == KEY_BTAB || $key == CONTROL_H ) { # KEY_BTAB == CONTROL_H
            if ( $self->{pos}[COL] == 0 && $self->{pos}[ROW] == 0 ) {
                $self->__beep();
            }
            else {
                if ( $self->{pos}[COL] > 0 ) {
                    $self->{pos}[COL]--;
                    $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] + 1 );
                    $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] );
                }
                else {
                    $self->{pos}[ROW]--;
                    if ( $self->{pos}[ROW] >= $self->{first_page_row} ) {
                        $self->{pos}[COL] = $#{$self->{rc2idx}[$self->{pos}[ROW]]};
                        $self->__wr_cell( $self->{pos}[ROW] + 1, 0 );
                        $self->__wr_cell( $self->{pos}[ROW]    , $self->{pos}[COL] );
                    }
                    else {
                        $self->{last_page_row}  = $self->{first_page_row} - 1;
                        $self->{first_page_row} = $self->{first_page_row} - $self->{avail_height};
                        $self->{first_page_row} = 0 if $self->{first_page_row} < 0;
                        $self->{pos}[COL] = $#{$self->{rc2idx}[$self->{pos}[ROW]]};
                        $self->__wr_screen();
                    }
                }
            }
        }
        elsif ( $key == VK_RIGHT || $key == KEY_l ) {
            if ( $self->{pos}[COL] == $#{$self->{rc2idx}[$self->{pos}[ROW]]} ) {
                $self->__beep();
            }
            else {
                $self->{pos}[COL]++;
                $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] - 1 );
                $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] );
            }
        }
        elsif ( $key == VK_LEFT || $key == KEY_h ) {
            if ( $self->{pos}[COL] == 0 ) {
                $self->__beep();
            }
            else {
                $self->{pos}[COL]--;
                $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] + 1 );
                $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] );
            }
        }
        elsif ( $key == VK_PAGE_UP || $key == CONTROL_P ) {
            if ( $self->{first_page_row} <= 0 ) {
                $self->__beep();
            }
            else {
                $self->{first_page_row} = $self->{avail_height} * ( int( $self->{pos}[ROW] / $self->{avail_height} ) - $page_step );
                $self->{last_page_row}  = $self->{first_page_row} + $self->{avail_height} - 1;
                if ( $saved_pos ) {
                    $self->{pos}[ROW] = $saved_pos->[ROW] + $self->{first_page_row};
                    $self->{pos}[COL] = $saved_pos->[COL];
                    $saved_pos = undef;
                }
                else {
                    $self->{pos}[ROW] -= $self->{avail_height} * $page_step;
                }
                $self->__wr_screen();
            }
        }
        elsif ( $key == VK_PAGE_DOWN || $key == CONTROL_N ) {
            if ( $self->{last_page_row} >= $#{$self->{rc2idx}} ) {
                $self->__beep();
            }
            else {
                my $backup_p_begin = $self->{first_page_row};
                $self->{first_page_row} = $self->{avail_height} * ( int( $self->{pos}[ROW] / $self->{avail_height} ) + $page_step );
                $self->{last_page_row}  = $self->{first_page_row} + $self->{avail_height} - 1;
                $self->{last_page_row}  = $#{$self->{rc2idx}} if $self->{last_page_row} > $#{$self->{rc2idx}};
                if (   $self->{pos}[ROW] + $self->{avail_height} > $#{$self->{rc2idx}}
                    || $self->{pos}[COL] > $#{$self->{rc2idx}[$self->{pos}[ROW] + $self->{avail_height}]}
                ) {
                    $saved_pos = [ $self->{pos}[ROW] - $backup_p_begin, $self->{pos}[COL] ];
                    $self->{pos}[ROW] = $#{$self->{rc2idx}};
                    if ( $self->{pos}[COL] > $#{$self->{rc2idx}[$self->{pos}[ROW]]} ) {
                        $self->{pos}[COL] = $#{$self->{rc2idx}[$self->{pos}[ROW]]};
                    }
                }
                else {
                    $self->{pos}[ROW] += $self->{avail_height} * $page_step;
                }
                $self->__wr_screen();
            }
        }
        elsif ( $key == VK_HOME || $key == CONTROL_A ) {
            if ( $self->{pos}[COL] == 0 && $self->{pos}[ROW] == 0 ) {
                $self->__beep();
            }
            else {
                $self->{pos}[ROW] = 0;
                $self->{pos}[COL] = 0;
                $self->{first_page_row} = 0;
                $self->{last_page_row}  = $self->{first_page_row} + $self->{avail_height} - 1;
                $self->{last_page_row}  = $#{$self->{rc2idx}} if $self->{last_page_row} > $#{$self->{rc2idx}};
                $self->__wr_screen();
            }
        }
        elsif ( $key == VK_END || $key == CONTROL_E ) {
            if ( $self->{order} == 1 && $self->{idx_of_last_col_in_last_row} < $#{$self->{rc2idx}[0]} ) {
                if (    $self->{pos}[ROW] == $#{$self->{rc2idx}} - 1
                     && $self->{pos}[COL] == $#{$self->{rc2idx}[$self->{pos}[ROW]]}
                ) {
                    $self->__beep();
                }
                else {
                    $self->{first_page_row} = @{$self->{rc2idx}} - ( @{$self->{rc2idx}} % $self->{avail_height} || $self->{avail_height} );
                    $self->{pos}[ROW] = $#{$self->{rc2idx}} - 1;
                    $self->{pos}[COL] = $#{$self->{rc2idx}[$self->{pos}[ROW]]};
                    if ( $self->{first_page_row} == $#{$self->{rc2idx}} ) {
                        $self->{first_page_row} = $self->{first_page_row} - $self->{avail_height};
                        $self->{last_page_row}  = $self->{first_page_row} + $self->{avail_height} - 1;
                    }
                    else {
                        $self->{last_page_row}  = $#{$self->{rc2idx}};
                    }
                    $self->__wr_screen();
                }
            }
            else {
                if (    $self->{pos}[ROW] == $#{$self->{rc2idx}}
                     && $self->{pos}[COL] == $#{$self->{rc2idx}[$self->{pos}[ROW]]}
                ) {
                    $self->__beep();
                }
                else {
                    $self->{first_page_row} = @{$self->{rc2idx}} - ( @{$self->{rc2idx}} % $self->{avail_height} || $self->{avail_height} );
                    $self->{last_page_row}  = $#{$self->{rc2idx}};
                    $self->{pos}[ROW] = $#{$self->{rc2idx}};
                    $self->{pos}[COL] = $#{$self->{rc2idx}[$self->{pos}[ROW]]};
                    $self->__wr_screen();
                }
            }
        }
        elsif ( $key == KEY_q || $key == CONTROL_Q ) {
            $self->__reset_term( 1 );
            return;
        }
        elsif ( $key == CONTROL_C ) {
            $self->__reset_term( 1 );
            print STDERR "^C\n";
            exit 1;
        }
        elsif ( $key == LINE_FEED || $key == CARRIAGE_RETURN ) { # LINE_FEED == CONTROL_J, CARRIAGE_RETURN == CONTROL_M      # ENTER key
            if ( length $self->{search_info} ) {
                require Term::Choose::Opt::Search;
                $self->Term::Choose::Opt::Search::__search_end();
                next GET_KEY;
            }
            my $opt_index = $self->{index} || $self->{ll};
            my $list_idx = $self->{rc2idx}[$self->{pos}[ROW]][$self->{pos}[COL]];
            if ( ! defined $self->{wantarray} ) {
                $self->__reset_term( 1 );
                return;
            }
            elsif ( $self->{wantarray} ) {
                if ( $self->{include_highlighted} == 1 ) {
                    $self->{marked}[$self->{pos}[ROW]][$self->{pos}[COL]] = 1;
                }
                elsif ( $self->{include_highlighted} == 2 ) {
                    my $chosen = $self->__marked_rc2idx();
                    if ( ! @$chosen ) {
                        $self->{marked}[$self->{pos}[ROW]][$self->{pos}[COL]] = 1;
                    }
                }
                if ( defined $self->{meta_items} && ! $self->{marked}[$self->{pos}[ROW]][$self->{pos}[COL]] ) {
                    for my $meta_item ( @{$self->{meta_items}} ) {
                        if ( $meta_item == $list_idx ) {
                            $self->{marked}[$self->{pos}[ROW]][$self->{pos}[COL]] = 1;
                            last;
                        }
                    }
                }
                my $chosen = $self->__marked_rc2idx();
                $self->__reset_term( 1 );
                return $opt_index ? @$chosen : @{$orig_list_ref}[@$chosen];
            }
            else {
                my $chosen = $opt_index ? $list_idx : $orig_list_ref->[$list_idx];
                $self->__reset_term( 1 );
                return $chosen;
            }
        }
        elsif ( $key == KEY_SPACE ) {
            if ( $self->{wantarray} ) {
                my $list_idx = $self->{rc2idx}[$self->{pos}[ROW]][$self->{pos}[COL]];
                my $locked = 0;
                if ( defined $self->{no_spacebar} || defined $self->{meta_items} ) {
                    for my $no_spacebar ( @{$self->{no_spacebar}||[]}, @{$self->{meta_items}||[]} ) {
                        if ( $list_idx == $no_spacebar ) {
                            ++$locked;
                            last;
                        }
                    }
                }
                if ( $locked ) {
                    $self->__beep();
                }
                else {
                    $self->{marked}[$self->{pos}[ROW]][$self->{pos}[COL]] = ! $self->{marked}[$self->{pos}[ROW]][$self->{pos}[COL]];
                    $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] );
                }
            }
            else {
                $self->__beep();
            }
        }
        elsif ( $key == CONTROL_SPACE ) {
            if ( $self->{wantarray} ) {
                for my $i ( 0 .. $#{$self->{rc2idx}} ) {
                    for my $j ( 0 .. $#{$self->{rc2idx}[$i]} ) {
                        $self->{marked}[$i][$j] = ! $self->{marked}[$i][$j];
                    }
                }
                if ( $self->{skip_items} ) {
                    $self->Term::Choose::Opt::SkipItems::__unmark_skip_items();
                }
                if ( defined $self->{no_spacebar} ) {
                    $self->__marked_idx2rc( $self->{no_spacebar}, 0 );
                }
                if ( defined $self->{meta_items} ) {
                    $self->__marked_idx2rc( $self->{meta_items}, 0 );
                }

                $self->__wr_screen();
            }
            else {
                $self->__beep();
            }
        }
        elsif ( $key == CONTROL_F && $self->{search} ) {
            require Term::Choose::Opt::Search;
            if ( $self->{ll} ) {
                $ENV{TC_POS_AT_SEARCH} = $self->{rc2idx}[$self->{pos}[ROW]][$self->{pos}[COL]];
                $self->__reset_term( 0 );
                return -13;
            }
            if ( length $self->{search_info} ) {
                $self->Term::Choose::Opt::Search::__search_end();
            }
            $self->Term::Choose::Opt::Search::__search_begin();
        }
        else {
            $self->__beep();
        }
    }
}


sub __beep {
    my ( $self, $beep ) = @_;
    if ( $beep ) {
        print bell();
    }
}


sub __prepare_info_and_prompt_lines {
    my ( $self ) = @_;
    my $info_w = $self->{term_width};
    if ( $^O ne 'MSWin32' && $^O ne 'cygwin' ) {
        $info_w += WIDTH_CURSOR;
    }
    if ( $self->{max_width} && $info_w > $self->{max_width} ) { ##
        $info_w = $self->{max_width};
    }
    my @tmp_prompt;
    if ( $self->{margin_top} ) {
        push @tmp_prompt, ( '' ) x $self->{margin_top};
    }
    if ( length $self->{info} ) {
        my $init     = $self->{tabs_info}[0] // 0;
        my $subseq   = $self->{tabs_info}[1] // 0;
        my $r_margin = $self->{tabs_info}[2] // 0;
        push @tmp_prompt, line_fold(
            $self->{info}, $info_w - $r_margin,
            { init_tab => ' ' x $init, subseq_tab => ' ' x $subseq, color => $self->{color}, join => 0 }
        );
    }
    if ( length $self->{prompt} ) {
        my $init     = $self->{tabs_prompt}[0] // 0;
        my $subseq   = $self->{tabs_prompt}[1] // 0;
        my $r_margin = $self->{tabs_prompt}[2] // 0;
        push @tmp_prompt, line_fold(
            $self->{prompt}, $info_w - $r_margin,
            { init_tab => ' ' x $init, subseq_tab => ' ' x $subseq, color => $self->{color}, join => 0 }
        );
    }
    if ( length $self->{search_info} ) {
        push @tmp_prompt, ( $self->{margin_left} ? ' ' x $self->{margin_left} : '' ) . $self->{search_info};
    }
    $self->{count_prompt_lines} = @tmp_prompt;
    if ( ! $self->{count_prompt_lines} ) {
        $self->{prompt_copy} = '';
        return;
    }
    $self->{prompt_copy} = join( "\n\r", @tmp_prompt ) . "\n\r"; #
    # \n\r -> stty 'raw' mode and Term::Readkey 'ultra-raw' mode don't translate newline to carriage_return/newline
}


sub __prepare_footer_line {
    my ( $self ) = @_;
    if ( exists $self->{footer_fmt} ) {
        delete $self->{footer_fmt};
    }
    my $pp_total = int( $#{$self->{rc2idx}} / $self->{avail_height} ) + 1;
    if ( $self->{page} == 0 ) {
        # nothing to do
    }
    elsif ( $self->{page} == 1 && $pp_total == 1 ) {
        $self->{avail_height}++;
    }
    else {
        my $pp_total_width = length $pp_total;
        $self->{footer_fmt} = '--- %0' . $pp_total_width . 'd/' . $pp_total . ' --- ';
        if ( defined $self->{footer} ) {
            $self->{footer_fmt} .= $self->{footer};
        }
        if ( print_columns( sprintf $self->{footer_fmt}, $pp_total ) > $self->{avail_width} ) { # color
            $self->{footer_fmt} = '%0' . $pp_total_width . 'd/' . $pp_total;
            if ( length( sprintf $self->{footer_fmt}, $pp_total ) > $self->{avail_width} ) {
                $pp_total_width = $self->{avail_width} if $pp_total_width > $self->{avail_width};
                $self->{footer_fmt} = '%0' . $pp_total_width . '.' . $pp_total_width . 's';
            }
        }
    }
    $self->{pp_count} = $pp_total;
}


sub __set_cell {
    my ( $self, $list_idx ) = @_;
    LOOP: for my $i ( 0 .. $#{$self->{rc2idx}} ) {
        for my $j ( 0 .. $#{$self->{rc2idx}[$i]} ) {
            if ( $list_idx == $self->{rc2idx}[$i][$j] ) {
                $self->{pos} = [ $i, $j ];
                last LOOP;
            }
        }
    }
    $self->{first_page_row} = $self->{avail_height} * int( $self->{pos}[ROW] / $self->{avail_height} );
    $self->{last_page_row} = $self->{first_page_row} + $self->{avail_height} - 1;
    $self->{last_page_row} = $#{$self->{rc2idx}} if $self->{last_page_row} > $#{$self->{rc2idx}};
}


sub __wr_first_screen {
    my ( $self ) = @_;
    $self->__avail_screen_size();
    $self->__current_layout();
    $self->__list_idx2rc();
    $self->__prepare_footer_line();
    $self->{first_page_row} = 0;
    my $avail_height_idx = $self->{avail_height} - 1;
    $self->{last_page_row}  = $avail_height_idx > $#{$self->{rc2idx}} ? $#{$self->{rc2idx}} : $avail_height_idx;
    $self->{i_row}  = 0;
    $self->{i_col}  = 0;
    $self->{pos}    = [ 0, 0 ];
    $self->{marked} = [];
    if ( $self->{wantarray} && defined $self->{mark} ) {
        $self->__marked_idx2rc( $self->{mark}, 1 );
    }
    if ( defined $self->{default} && $self->{default} <= $#{$self->{list}} ) {
        $self->__set_cell( $self->{default} );
    }
    if ( $self->{clear_screen} ) {
        print clear_screen();
    }
    else {
        print "\r" . clear_to_end_of_screen();
    }
    if ( $self->{prompt_copy} ne '' ) {
        print $self->{prompt_copy};
    }
    $self->__wr_screen();
    if ( $self->{mouse} ) {
        my $abs_cursor_y = $self->{plugin}->__get_cursor_row();
        $self->{offset_rows} = $abs_cursor_y - 1 - $self->{i_row};
    }
}


sub __wr_screen {
    my ( $self ) = @_;
    $self->__goto( 0, 0 );
    print "\r" . clear_to_end_of_screen();
    if ( defined $self->{footer_fmt} ) {
        my $pp_line = sprintf $self->{footer_fmt}, int( $self->{first_page_row} / $self->{avail_height} ) + 1;
        if ( $self->{margin_left} ) {
            print right( $self->{margin_left} );
        }
        print "\n" x ( $self->{avail_height} );
        print $pp_line . "\r";
        if ( $self->{margin_bottom} ) {
            print "\n" x $self->{margin_bottom};
            print up( $self->{margin_bottom} );
        }
        print up( $self->{avail_height} );
    }
    elsif ( $self->{margin_bottom} ) {
        my $count = ( $self->{last_page_row} - $self->{first_page_row} ) + $self->{margin_bottom};
        print "\n" x $count;
        print up( $count );
    }
    if ( $self->{margin_left} ) {
        print right( $self->{margin_left} ); # left margin after each "\r"
    }
    my $pad_str = ' ' x $self->{pad};
    for my $row ( $self->{first_page_row} .. $self->{last_page_row} ) {
        my $line = $self->__prepare_cell( $row, 0 );
        if ( $#{$self->{rc2idx}[$row]} ) { #
            for my $col ( 1 .. $#{$self->{rc2idx}[$row]} ) {
                $line = $line . $pad_str . $self->__prepare_cell( $row, $col );
            }
        }
        print $line . "\n\r";
        if ( $self->{margin_left} ) {
            print right( $self->{margin_left} );
        }
    }
    print up( $self->{last_page_row} - $self->{first_page_row} + 1 );
    # relativ cursor pos: 0, 0
    $self->__wr_cell( $self->{pos}[ROW], $self->{pos}[COL] );
}


sub __prepare_cell {
    my( $self, $row, $col ) = @_;
    my $is_current_pos = $row == $self->{pos}[ROW] && $col == $self->{pos}[COL];
    my $emphasised = ( $self->{marked}[$row][$col] ? bold_underline() : '' ) . ( $is_current_pos ? reverse_video() : '' );
    my $idx = $self->{rc2idx}[$row][$col];
    if ( $self->{ll} ) {
        if ( $self->{color} ) {
            my $str = $self->{list}[$idx];
            if ( $emphasised ) {
                if ( $is_current_pos && $self->{color} == 1 ) {
                    # no color for the selected cell if color == 1
                    $str =~ s/(\e\[[\d;]*m)//g;
                }
                else {
                    # keep marked cells marked after color escapes
                    $str =~ s/(\e\[[\d;]*m)/${1}$emphasised/g;
                }
                $str = $emphasised . $str;
            }
            return $str . normal(); # if \e[
        }
        else {
            if ( $emphasised ) {
                return $emphasised . $self->{list}[$idx] . normal();
            }
            else {
                return $self->{list}[$idx];
            }
        }
    }
    else {
        my $str = $self->{current_layout} == -1 ? $self->{list}[$idx] : $self->__pad_str_to_colwidth( $idx );
        if ( $self->{color} ) {
            my @color;
            if ( ! $self->{orig_list}[$idx] ) {
                if ( ! defined $self->{orig_list}[$idx] ) {
                    @color = $self->{undef} =~ /(\e\[[\d;]*m)/g;
                }
                elsif ( ! length $self->{orig_list}[$idx] ) {
                    @color = $self->{empty} =~ /(\e\[[\d;]*m)/g;
                }
            }
            else {
                @color = $self->{orig_list}[$idx] =~ /(\e\[[\d;]*m)/g;
            }
            if ( $emphasised ) {
                for ( @color ) {
                    # keep marked cells marked after color escapes
                    $_ .= $emphasised;
                }
                $str = $emphasised . $str . normal();
                if ( $is_current_pos && $self->{color} == 1 ) {
                    # no color for the selected cell if color == 1
                    @color = ();
                    $str =~ s/\x{feff}//g;
                }
            }
            if ( @color ) {
                $str =~ s/\x{feff}/shift @color/ge;
                if ( ! $emphasised ) {
                    $str .= normal();
                }
            }
            return $str;
        }
        else {
            if ( $emphasised ) {
                $str = $emphasised . $str . normal();
            }
            return $str;
        }
    }
}


sub __wr_cell {
    my( $self, $row, $col ) = @_;
    my $idx = $self->{rc2idx}[$row][$col];
    if ( $self->{current_layout} == -1 ) {
        my $x = 0;
        if ( $col > 0 ) {
            for my $cl ( 0 .. $col - 1 ) {
                my $i = $self->{rc2idx}[$row][$cl];
                $x += $self->{width_elements}[$i] + $self->{pad};
            }
        }
        $self->__goto( $row - $self->{first_page_row}, $x );
        $self->{i_col} = $self->{i_col} + $self->{width_elements}[$idx];
    }
    else {
        $self->__goto( $row - $self->{first_page_row}, $col * $self->{col_width_plus} );
        $self->{i_col} = $self->{i_col} + $self->{col_width};
    }
    print $self->__prepare_cell( $row, $col );
}


sub __pad_str_to_colwidth {
    my ( $self, $idx ) = @_;
    if ( $self->{width_elements}[$idx] < $self->{col_width} ) {
        if ( $self->{alignment} == 0 ) {
            return $self->{list}[$idx] . ( " " x ( $self->{col_width} - $self->{width_elements}[$idx] ) );
        }
        elsif ( $self->{alignment} == 1 ) {
            return " " x ( $self->{col_width} - $self->{width_elements}[$idx] ) . $self->{list}[$idx];
        }
        elsif ( $self->{alignment} == 2 ) {
            my $all = $self->{col_width} - $self->{width_elements}[$idx];
            my $half = int( $all / 2 );
            return ( " " x $half ) . $self->{list}[$idx] . ( " " x ( $all - $half ) );
        }
    }
    elsif ( $self->{width_elements}[$idx] > $self->{col_width} ) {
        if ( $self->{col_width} > 6 ) {
            return cut_to_printwidth( $self->{list}[$idx], $self->{col_width} - 3 ) . '...';
        }
        else {
            return cut_to_printwidth( $self->{list}[$idx], $self->{col_width} );
        }
    }
    else {
        return $self->{list}[$idx];
    }
}


sub __goto {
    my ( $self, $newrow, $newcol ) = @_;
    # requires up, down, left or right to be 1 or greater
    if ( $newrow > $self->{i_row} ) {
        print down( $newrow - $self->{i_row} );
        $self->{i_row} = $newrow;
    }
    elsif ( $newrow < $self->{i_row} ) {
        print up( $self->{i_row} - $newrow );
        $self->{i_row} = $newrow;
    }
    if ( $newcol > $self->{i_col} ) {
        print right( $newcol - $self->{i_col} );
        $self->{i_col} = $newcol;
    }
    elsif ( $newcol < $self->{i_col} ) {
        print left( $self->{i_col} - $newcol );
        $self->{i_col} = $newcol;
    }
}


sub __avail_screen_size {
    my ( $self ) = @_;
    ( $self->{avail_width}, $self->{avail_height} ) = ( $self->{term_width}, $self->{term_height} );
    if ( $self->{margin_left} ) {
        $self->{avail_width} -= $self->{margin_left};
    }
    if ( $self->{margin_right} ) {
        $self->{avail_width} -= $self->{margin_right};
    }
    if ( $self->{margin_right} || ( $self->{col_width} > $self->{avail_width} && $^O ne 'MSWin32' && $^O ne 'cygwin' ) ) {
        $self->{avail_width} += WIDTH_CURSOR;
        # + WIDTH_CURSOR: use also the last terminal column if there is only one item-column;
        #                 with only one item-column the output doesn't get messed up if an item
        #                 reaches the right edge of the terminal on a non-MSWin32-OS
    }
    if ( $self->{max_width} && $self->{avail_width} > $self->{max_width} ) {
        $self->{avail_width} = $self->{max_width};
    }
    if ( $self->{avail_width} < 1 ) {
        $self->{avail_width} = 1;
    }
    #if ( $self->{ll} && $self->{ll} > $self->{avail_width} ) {
    #    return -2;
    #}
    $self->__prepare_info_and_prompt_lines();
    if ( $self->{count_prompt_lines} ) {
        $self->{avail_height} -= $self->{count_prompt_lines};
    }
    if ( $self->{page} ) {
        $self->{avail_height}--;
    }
    if ( $self->{margin_bottom} ) {
        $self->{avail_height} -= $self->{margin_bottom};
    }
    if ( $self->{avail_height} < $self->{keep} ) {
        $self->{avail_height} = $self->{term_height} >= $self->{keep} ? $self->{keep} : $self->{term_height};
    }
    if ( $self->{max_height} && $self->{max_height} < $self->{avail_height} ) {
        $self->{avail_height} = $self->{max_height};
    }
}


sub __current_layout {
    my ( $self ) = @_;
    my $all_in_first_row;
    if ( $self->{layout} <= 1 && ! $self->{ll} && ! $self->{max_cols} ) {
        my $firstrow_width = 0;
        for my $list_idx ( 0 .. $#{$self->{list}} ) {
            $firstrow_width += $self->{width_elements}[$list_idx] + $self->{pad};
            if ( $firstrow_width - $self->{pad} > $self->{avail_width} ) {
                $firstrow_width = 0;
                last;
            }
        }
        $all_in_first_row = $firstrow_width;
    }
    if ( $all_in_first_row ) {
        $self->{current_layout} = -1;
    }
    elsif ( $self->{col_width} >= $self->{avail_width} ) {
        $self->{current_layout} = 2;
        $self->{col_width} = $self->{avail_width};
    }
    else {
        $self->{current_layout} = $self->{layout};
    }
    $self->{col_width_plus} = $self->{col_width} + $self->{pad};
    # 'col_width_plus' no effects if layout == 2
}


sub __list_idx2rc {
    my ( $self ) = @_;
    my $layout = $self->{current_layout};
    $self->{rc2idx} = [];
    if ( $layout == -1 ) {
        $self->{rc2idx}[0] = [ 0 .. $#{$self->{list}} ];
        $self->{idx_of_last_col_in_last_row} = $#{$self->{list}};
    }
    elsif ( $layout == 2 ) {
        for my $list_idx ( 0 .. $#{$self->{list}} ) {
            $self->{rc2idx}[$list_idx][0] = $list_idx;
            $self->{idx_of_last_col_in_last_row} = 0;
        }
    }
    else {
        my $tmp_avail_width = $self->{avail_width} + $self->{pad};
        # auto_format
        if ( $layout == 1 ) {
            my $tmc = int( @{$self->{list}} / $self->{avail_height} );
            $tmc++ if @{$self->{list}} % $self->{avail_height};
            $tmc *= $self->{col_width_plus};
            if ( $tmc < $tmp_avail_width ) {
                $tmc = int( $tmc + ( ( $tmp_avail_width - $tmc ) / 1.5 ) );
                $tmp_avail_width = $tmc;
            }
        }
        # order
        my $cols_per_row = int( $tmp_avail_width / $self->{col_width_plus} );
        if ( $self->{max_cols} && $cols_per_row > $self->{max_cols} ) {
            $cols_per_row = $self->{max_cols};
        }
        $cols_per_row = 1 if $cols_per_row < 1;
        $self->{idx_of_last_col_in_last_row} = ( @{$self->{list}} % $cols_per_row || $cols_per_row ) - 1;
        if ( $self->{order} == 1 ) {
            my $rows = int( ( @{$self->{list}} - 1 + $cols_per_row ) / $cols_per_row );
            my @rearranged_idx;
            my $begin = 0;
            my $end = $rows - 1 ;
            for my $c ( 0 .. $cols_per_row - 1 ) {
                --$end if $c > $self->{idx_of_last_col_in_last_row};
                $rearranged_idx[$c] = [ $begin .. $end ];
                $begin = $end + 1;
                $end = $begin + $rows - 1;
            }
            for my $r ( 0 .. $rows - 1 ) {
                my @temp_idx;
                for my $c ( 0 .. $cols_per_row - 1 ) {
                    next if $r == $rows - 1 && $c > $self->{idx_of_last_col_in_last_row};
                    push @temp_idx, $rearranged_idx[$c][$r];
                }
                push @{$self->{rc2idx}}, \@temp_idx;
            }
        }
        else {
            my $begin = 0;
            my $end = $cols_per_row - 1;
            $end = $#{$self->{list}} if $end > $#{$self->{list}};
            push @{$self->{rc2idx}}, [ $begin .. $end ];
            while ( $end < $#{$self->{list}} ) {
                $begin += $cols_per_row;
                $end   += $cols_per_row;
                $end    = $#{$self->{list}} if $end > $#{$self->{list}};
                push @{$self->{rc2idx}}, [ $begin .. $end ];
            }
        }
    }
}


sub __marked_idx2rc {
    my ( $self, $list_of_indexes, $boolean ) = @_;
    my $last_list_idx = $#{$self->{list}};
    if ( $self->{current_layout} == 2 ) {
        for my $list_idx ( @$list_of_indexes ) {
            if ( $list_idx > $last_list_idx ) {
                next;
            }
            $self->{marked}[$list_idx][0] = $boolean;
        }
        return;
    }
    my ( $row, $col );
    my $cols_per_row = @{$self->{rc2idx}[0]};
    if ( $self->{order} == 0 ) {
        for my $list_idx ( @$list_of_indexes ) {
            if ( $list_idx > $last_list_idx ) {
                next;
            }
            $row = int( $list_idx / $cols_per_row );
            $col = $list_idx % $cols_per_row;
            $self->{marked}[$row][$col] = $boolean;
        }
    }
    elsif ( $self->{order} == 1 ) {
        my $rows_per_col = @{$self->{rc2idx}};
        my $col_count_last_row = $self->{idx_of_last_col_in_last_row} + 1;
        my $last_list_idx_in_cols_full = $rows_per_col * $col_count_last_row - 1;
        my $first_list_idx_in_cols_short = $last_list_idx_in_cols_full + 1;

        for my $list_idx ( @$list_of_indexes ) {
            if ( $list_idx > $last_list_idx ) {
                next;
            }
            if ( $list_idx < $last_list_idx_in_cols_full ) {
                $row = $list_idx % $rows_per_col;
                $col = int( $list_idx / $rows_per_col );
            }
            else {
                my $rows_per_col_short = $rows_per_col - 1;
                $row = ( $list_idx - $first_list_idx_in_cols_short ) % $rows_per_col_short;
                $col = int( ( $list_idx - $col_count_last_row ) / $rows_per_col_short );
            }
            $self->{marked}[$row][$col] = $boolean;
        }
    }
}


sub __marked_rc2idx {
    my ( $self ) = @_;
    my $list_idx = [];
    if ( $self->{order} == 1 ) {
        for my $col ( 0 .. $#{$self->{rc2idx}[0]} ) {
            for my $row ( 0 .. $#{$self->{rc2idx}} ) {
                if ( $self->{marked}[$row][$col] ) {
                    push @$list_idx, $self->{rc2idx}[$row][$col];
                }
            }
        }
    }
    else {
        for my $row ( 0 .. $#{$self->{rc2idx}} ) {
            for my $col ( 0 .. $#{$self->{rc2idx}[$row]} ) {
                if ( $self->{marked}[$row][$col] ) {
                    push @$list_idx, $self->{rc2idx}[$row][$col];
                }
            }
        }
    }
    return $list_idx;
}


1;


__END__
