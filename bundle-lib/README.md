# Bundled library files

The files in this sub-directory have been stripped of their documentation to
make the bundled program shorter. They come from the available code in CPAN
for the included versions.

See file `lib/Romeo.pm` (starting from the main directory of the project) for
the respective copyright and licensing data.
