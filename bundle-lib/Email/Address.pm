use v5.12.0;
use warnings;
package Email::Address 1.913;
# ABSTRACT: RFC 2822 Address Parsing and Creation

our $COMMENT_NEST_LEVEL ||= 1;
our $STRINGIFY          ||= 'format';
our $COLLAPSE_SPACES    //= 1;


my $CTL            = q{\x00-\x1F\x7F};
my $special        = q{()<>\\[\\]:;@\\\\,."};

my $text           = qr/[^\x0A\x0D]/;

my $quoted_pair    = qr/\\$text/;

my $ctext          = qr/(?>[^()\\]+)/;
my ($ccontent, $comment) = (q{})x2;
for (1 .. $COMMENT_NEST_LEVEL) {
  $ccontent = qr/$ctext|$quoted_pair|$comment/;
  $comment  = qr/(?>\s*\((?:\s*$ccontent)*\s*\)\s*)/;
}
my $cfws           = qr/$comment|(?>\s+)/;

my $atext          = qq/[^$CTL$special\\s]/;
my $atom           = qr/(?>$cfws*$atext+$cfws*)/;
my $dot_atom_text  = qr/(?>$atext+(?:\.$atext+)*)/;
my $dot_atom       = qr/(?>$cfws*$dot_atom_text$cfws*)/;

my $qtext          = qr/[^\\"]/;
my $qcontent       = qr/$qtext|$quoted_pair/;
my $quoted_string  = qr/(?>$cfws*"$qcontent*"$cfws*)/;

my $word           = qr/$atom|$quoted_string/;

my $simple_word    = qr/(?>$atom|\.|\s*"$qcontent+"\s*)/;
my $obs_phrase     = qr/(?>$simple_word+)/;

my $phrase         = qr/$obs_phrase|(?>$word+)/;

my $local_part     = qr/$dot_atom|$quoted_string/;
my $dtext          = qr/[^\[\]\\]/;
my $dcontent       = qr/$dtext|$quoted_pair/;
my $domain_literal = qr/(?>$cfws*\[(?:\s*$dcontent)*\s*\]$cfws*)/;
my $domain         = qr/$dot_atom|$domain_literal/;

my $display_name   = $phrase;

our $addr_spec  = qr/$local_part\@$domain/;
our $angle_addr = qr/(?>$cfws*<$addr_spec>$cfws*)/;
our $name_addr  = qr/(?>$display_name?)$angle_addr/;
our $mailbox    = qr/(?:$name_addr|$addr_spec)(?>$comment*)/;

sub _PHRASE   () { 0 }
sub _ADDRESS  () { 1 }
sub _COMMENT  () { 2 }
sub _ORIGINAL () { 3 }
sub _IN_CACHE () { 4 }

sub __dump {
  return {
    phrase   => $_[0][_PHRASE],
    address  => $_[0][_ADDRESS],
    comment  => $_[0][_COMMENT],
    original => $_[0][_ORIGINAL],
  }
}

our (%PARSE_CACHE, %FORMAT_CACHE, %NAME_CACHE);
my $NOCACHE;

sub __get_cached_parse {
    return if $NOCACHE;

    my ($class, $line) = @_;

    return @{$PARSE_CACHE{$line}} if exists $PARSE_CACHE{$line};
    return;
}

sub __cache_parse {
    return if $NOCACHE;

    my ($class, $line, $addrs) = @_;

    $PARSE_CACHE{$line} = $addrs;
}

sub parse {
    my ($class, $line) = @_;
    return unless $line;

    $line =~ s/[ \t]+/ /g if $COLLAPSE_SPACES;

    if (my @cached = $class->__get_cached_parse($line)) {
        return @cached;
    }

    my %mailboxes;
    my $str = $line;
    $str =~ s!($name_addr(?>$comment*))!$mailboxes{pos($str)} = $1; ',' x length $1!ego
        if $str =~ /$angle_addr/;
    $str =~ s!($addr_spec(?>$comment*))!$mailboxes{pos($str)} = $1; ',' x length $1!ego;
    my @mailboxes = map { $mailboxes{$_} } sort { $a <=> $b } keys %mailboxes;

    my @addrs;
    foreach (@mailboxes) {
      my $original = $_;

      my @comments = /($comment)/go;
      s/$comment//go if @comments;

      my ($user, $host, $com);
      ($user, $host) = ($1, $2) if s/<($local_part)\@($domain)>\s*\z//o;
      if (! defined($user) || ! defined($host)) {
          s/($local_part)\@($domain)//o;
          ($user, $host) = ($1, $2);
      }

      next if $user =~ /\P{ASCII}/;
      next if $host =~ /\P{ASCII}/;

      my ($phrase)       = /($display_name)/o;

      for ( $phrase, $host, $user, @comments ) {
        next unless defined $_;
        s/^\s+//;
        s/\s+$//;
        $_ = undef unless length $_;
      }

      $phrase =~ s/\\(.)/$1/g if $phrase;

      my $new_comment = join q{ }, @comments;
      push @addrs,
        $class->new($phrase, "$user\@$host", $new_comment, $original);
      $addrs[-1]->[_IN_CACHE] = [ \$line, $#addrs ]
    }

    $class->__cache_parse($line, \@addrs);
    return @addrs;
}

sub new {
  my ($class, $phrase, $email, $comment, $orig) = @_;
  $phrase =~ s/\A"(.+)"\z/$1/ if $phrase;

  bless [ $phrase, $email, $comment, $orig ] => $class;
}

sub purge_cache {
    %NAME_CACHE   = ();
    %FORMAT_CACHE = ();
    %PARSE_CACHE  = ();
}

sub disable_cache {
  my ($class) = @_;
  $class->purge_cache;
  $NOCACHE = 1;
}

sub enable_cache {
  $NOCACHE = undef;
}

BEGIN {
  my %_INDEX = (
    phrase   => _PHRASE,
    address  => _ADDRESS,
    comment  => _COMMENT,
    original => _ORIGINAL,
  );

  for my $method (keys %_INDEX) {
    no strict 'refs';
    my $index = $_INDEX{ $method };
    *$method = sub {
      if ($_[1]) {
        if ($_[0][_IN_CACHE]) {
          my $replicant = bless [ @{$_[0]} ] => ref $_[0];
          $PARSE_CACHE{ ${ $_[0][_IN_CACHE][0] } }[ $_[0][_IN_CACHE][1] ]
            = $replicant;
          $_[0][_IN_CACHE] = undef;
        }
        $_[0]->[ $index ] = $_[1];
      } else {
        $_[0]->[ $index ];
      }
    };
  }
}

sub host { ($_[0]->[_ADDRESS] =~ /\@($domain)/o)[0]     }
sub user { ($_[0]->[_ADDRESS] =~ /($local_part)\@/o)[0] }

sub format {
    my $cache_str = do { no warnings 'uninitialized'; "@{$_[0]}" };
    return $FORMAT_CACHE{$cache_str} if exists $FORMAT_CACHE{$cache_str};
    $FORMAT_CACHE{$cache_str} = $_[0]->_format;
}

sub _format {
    my ($self) = @_;

    unless (length $self->[_PHRASE] || length $self->[_COMMENT]) {
        return $self->[_ADDRESS] // '';
    }

    my $comment = $self->[_COMMENT] // '';
    $comment = "($comment)" if length $comment and $comment !~ /\A\(.*\)\z/;

    my $format = sprintf q{%s <%s> %s},
                 $self->_enquoted_phrase,
                 ($self->[_ADDRESS] // ''),
                 $comment;

    $format =~ s/^\s+//;
    $format =~ s/\s+$//;

    return $format;
}

sub _enquoted_phrase {
  my ($self) = @_;

  my $phrase = $self->[_PHRASE];

  return '' unless length $phrase;

  # if it's encoded -- rjbs, 2007-02-28
  return $phrase if $phrase =~ /\A=\?.+\?=\z/;

  $phrase =~ s/\A"(.+)"\z/$1/;
  $phrase =~ s/([\\"])/\\$1/g;

  return qq{"$phrase"};
}

sub name {
    my $cache_str = do { no warnings 'uninitialized'; "@{$_[0]}" };
    return $NAME_CACHE{$cache_str} if exists $NAME_CACHE{$cache_str};

    my ($self) = @_;
    my $name = q{};
    if ( $name = $self->[_PHRASE] ) {
        $name =~ s/^"//;
        $name =~ s/"$//;
        $name =~ s/($quoted_pair)/substr $1, -1/goe;
    } elsif ( $name = $self->[_COMMENT] ) {
        $name =~ s/^\(//;
        $name =~ s/\)$//;
        $name =~ s/($quoted_pair)/substr $1, -1/goe;
        $name =~ s/$comment/ /go;
    } else {
        ($name) = $self->[_ADDRESS] =~ /($local_part)\@/o;
    }
    $NAME_CACHE{$cache_str} = $name;
}

sub as_string {
  warn 'altering $Email::Address::STRINGIFY is deprecated; subclass instead'
    if $STRINGIFY ne 'format';

  $_[0]->can($STRINGIFY)->($_[0]);
}

use overload '""' => 'as_string', fallback => 1;

1;

__END__

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2004 by Casey West.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
