package MIME::Body;

### Pragmas:
use strict;
use vars qw($VERSION);

### System modules:
use Carp;
use IO::File;

### The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";


sub new {
    my $self = bless {}, shift;
    $self->init(@_);
    $self;
}

sub init { 1 }

sub as_lines {
    my $self = shift;
    my @lines;
    my $io = $self->open("r") || return ();
    local $_;
    push @lines, $_ while (defined($_ = $io->getline()));
    $io->close;
    @lines;
}

sub as_string {
    my $self = shift;
    my $str = '';
    my $fh = IO::File->new(\$str, '>:') or croak("Cannot open in-memory file: $!");
    $self->print($fh);
    close($fh);
    return $str;
}
*data = \&as_string;         ### silently invoke preferred usage

sub binmode {
    my ($self, $onoff) = @_;
    $self->{MB_Binmode} = $onoff if (@_ > 1);
    $self->{MB_Binmode};
}

sub is_encoded {
    my ($self, $yesno) = @_;
    $self->{MB_IsEncoded} = $yesno if (@_ > 1);
    $self->{MB_IsEncoded};
}

sub dup {
    my $self = shift;
    bless { %$self }, ref($self);   ### shallow copy ok for ::File and ::Scalar
}

sub open {
    undef;
}

sub path {
    my $self = shift;
    $self->{MB_Path} = shift if @_;
    $self->{MB_Path};
}

sub print {
    my ($self, $fh) = @_;
    my $nread;

    ### Get output filehandle, and ensure that it's a printable object:
    $fh ||= select;

    ### Write it:
    my $buf = '';
    my $io = $self->open("r") || return undef;
    $fh->print($buf) while ($nread = $io->read($buf, 8192));
    $io->close;
    return defined($nread);    ### how'd we do?
}

sub purge {
    1;
}

#------------------------------------------------------------
package MIME::Body::File;
#------------------------------------------------------------


### Pragmas:
use vars qw(@ISA);
use strict;

### System modules:
use IO::File;

### Kit modules:
use MIME::Tools qw(whine);

@ISA = qw(MIME::Body);


#------------------------------
# init PATH
#------------------------------
sub init {
    my ($self, $path) = @_;
    $self->path($path);               ### use it as-is
    $self;
}

#------------------------------
# open READWRITE
#------------------------------
sub open {
    my ($self, $mode) = @_;

    my $path = $self->path;

    if( $mode ne 'r' && $mode ne 'w' ) {
	die "bad mode: '$mode'";
    }

    my $IO = IO::File->new($path, $mode) || die "MIME::Body::File->open $path: $!";

    $IO->binmode() if $self->binmode;

    return $IO;
}

#------------------------------
# purge
#------------------------------
# Unlink the path (and undefine it).
#
sub purge {
    my $self = shift;
    if (defined($self->path)) {
	unlink $self->path or whine "couldn't unlink ".$self->path.": $!";
	$self->path(undef);
    }
    1;
}




#------------------------------------------------------------
package MIME::Body::Scalar;
#------------------------------------------------------------

use vars qw(@ISA);
use strict;

use Carp;

@ISA = qw(MIME::Body);


#------------------------------
# init DATA
#------------------------------
sub init {
    my ($self, $data) = @_;
    $data = join('', @$data)    if (ref($data) && (ref($data) eq 'ARRAY'));
    $self->{MBS_Data} = (defined($data) ? $data : '');
    $self;
}

#------------------------------
# as_string
#------------------------------
sub as_string {
    shift->{MBS_Data};
}

#------------------------------
# open READWRITE
#------------------------------
sub open {
    my ($self, $mode) = @_;
    $self->{MBS_Data} = '' if ($mode eq 'w');        ### writing

    if ($mode eq 'w') {
	    $mode = '>:';
    } elsif ($mode eq 'r') {
	    $mode = '<:';
    } else {
	    die "bad mode: $mode";
    }

    return IO::File->new(\ $self->{MBS_Data}, $mode);
}





#------------------------------------------------------------
package MIME::Body::InCore;
#------------------------------------------------------------

use vars qw(@ISA);
use strict;

use Carp;

@ISA = qw(MIME::Body::Scalar);


#------------------------------
# init DATA
#------------------------------
sub init {
    my ($self, $data) = @_;
    if (!defined($data)) {  ### nothing
	$self->{MBS_Data} = '';
    }
    elsif (!ref($data)) {   ### simple scalar
	$self->{MBS_Data} = $data;
    }
    elsif (ref($data) eq 'SCALAR') {
	$self->{MBS_Data} = $$data;
    }
    elsif (ref($data) eq 'ARRAY') {
	$self->{MBS_Data} = join('', @$data);
    }
    else {
	croak "I can't handle DATA which is a ".ref($data)."\n";
    }
    $self;
}

1;
__END__
