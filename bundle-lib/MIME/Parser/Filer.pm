package MIME::Parser::Filer;

use strict;

### Kit modules:
use MIME::Tools qw(:msgtypes);
use File::Spec;
use File::Path qw(rmtree);
use MIME::WordDecoder;

### Output path uniquifiers:
my $GFileNo = 0;
my $GSubdirNo = 0;

### Map content-type to extension.
### If we can't map "major/minor", we try "major/*", then use "*/*".
my %DefaultTypeToExt =
qw(

application/andrew-inset	.ez
application/octet-stream	.bin
application/oda			.oda
application/pdf			.pdf
application/pgp			.pgp
application/postscript		.ps
application/rtf			.rtf
application/x-bcpio		.bcpio
application/x-chess-pgn		.pgn
application/x-cpio		.cpio
application/x-csh		.csh
application/x-dvi		.dvi
application/x-gtar		.gtar
application/x-gunzip		.gz
application/x-hdf		.hdf
application/x-latex		.latex
application/x-mif		.mif
application/x-netcdf		.cdf
application/x-netcdf		.nc
application/x-sh		.sh
application/x-shar		.shar
application/x-sv4cpio		.sv4cpio
application/x-sv4crc		.sv4crc
application/x-tar		.tar
application/x-tcl		.tcl
application/x-tex		.tex
application/x-texinfo		.texi
application/x-troff		.roff
application/x-troff		.tr
application/x-troff-man		.man
application/x-troff-me		.me
application/x-troff-ms		.ms
application/x-ustar		.ustar
application/x-wais-source	.src
application/zip			.zip

audio/basic			.snd
audio/ulaw			.au
audio/x-aiff			.aiff
audio/x-wav			.wav

image/gif			.gif
image/ief			.ief
image/jpeg			.jpg
image/png                       .png
image/xbm                       .xbm
image/tiff			.tif
image/x-cmu-raster		.ras
image/x-portable-anymap		.pnm
image/x-portable-bitmap		.pbm
image/x-portable-graymap	.pgm
image/x-portable-pixmap		.ppm
image/x-rgb			.rgb
image/x-xbitmap			.xbm
image/x-xpixmap			.xpm
image/x-xwindowdump		.xwd

text/*                          .txt
text/html			.html
text/plain			.txt
text/richtext			.rtx
text/tab-separated-values	.tsv
text/x-setext			.etx
text/x-vcard                    .vcf

video/mpeg			.mpg
video/quicktime			.mov
video/x-msvideo			.avi
video/x-sgi-movie		.movie

message/*                       .msg

*/*                             .dat

);

sub new {
    my ($class, @initargs) = @_;
    my $self = bless {
	MPF_Prefix    => "msg",
	MPF_Dir       => ".",
	MPF_Ext       => { %DefaultTypeToExt },
	MPF_Purgeable => [],       ### files created by the last parse

	MPF_MaxName   => 80,       ### max filename before treated as evil
	MPF_TrimRoot  => 14,       ### trim root to this length
	MPF_TrimExt   => 3,        ### trim extension to this length
    }, $class;
    $self->init(@initargs);
    $self;
}

sub init {
    ### no-op
}

#------------------------------
# My PID, but untainted
#
sub untainted_pid
{
        if ($$ =~ /^(\d+)$/) {
                return $1;
        }
        # Can't happen...
        return "0";
}

#------------------------------
#
# cleanup_dir
#
# Instance method, private.
# Cleanup a directory, defaulting empty to "."
#
sub cleanup_dir {
    my ($self, $dir) = @_;
    $dir = '.' if (!defined($dir) || ($dir eq ''));   # coerce empty to "."
    $dir = '/.' if ($dir eq '/');   # coerce "/" so "$dir/$filename" works
    $dir =~ s|/$||;                 # be nice: get rid of any trailing "/"
    $dir;
}

sub results {
    my ($self, $results) = @_;
    $self->{MPF_Results} = $results if (@_ > 1);
    $self->{MPF_Results};
}

### Log debug messages:
sub debug {
    my $self = shift;
    if (MIME::Tools->debugging()) {
	if ($self->{MPF_Results}) {
	    unshift @_, $self->{MPF_Results}->indent;
	    $self->{MPF_Results}->msg($M_DEBUG, @_);
	}
	MIME::Tools::debug(@_);
    }
}

### Log warning messages:
sub whine {
    my $self = shift;
    if ($self->{MPF_Results}) {
	unshift @_, $self->{MPF_Results}->indent;
	$self->{MPF_Results}->msg($M_WARNING, @_);
    }
    MIME::Tools::whine(@_);
}

sub init_parse {
    my $self = shift;
    $self->{MPF_Purgeable} = [];
}

sub evil_filename {
    my ($self, $name) = @_;

    $self->debug("is this evil? '$name'");

    return 1 if (!defined($name) or ($name eq ''));   ### empty
    return 1 if ($name =~ m{(^\s)|(\s+\Z)});  ### leading/trailing whitespace
    return 1 if ($name =~ m{^\.+\Z});         ### dots
    return 1 if ($name =~ /[^-A-Z0-9_+=.,@\#\$\% ]/i); # Only allow good chars
    return 1 if ($self->{MPF_MaxName} and
		 (length($name) > $self->{MPF_MaxName}));
    $self->debug("it's ok");
    0;
}

sub exorcise_filename {
    my ($self, $fname) = @_;

    ### Isolate to last path element:
    my $last = $fname;

    ### Path separators are / or \
    $last =~ s{^.*[/\\]}{};

    ### Convert semi-evil characters to underscores
    $last =~ s/[\/\\\[\]:]/_/g;
    if ($last and !$self->evil_filename($last)) {
	$self->debug("looks like I can use the last path element");
	return $last;
    }

    ### Break last element into root and extension, and truncate:
    my ($root, $ext) = (($last =~ /^(.*)\.([^\.]+)\Z/)
			? ($1, $2)
			: ($last, ''));
    ### Delete leading and trailing whitespace
    $root =~ s/^\s+//;
    $ext  =~ s/\s+$//;
    $root = substr($root, 0, ($self->{MPF_TrimRoot} || 14));
    $ext  = substr($ext,  0, ($self->{MPF_TrimExt}  ||  3));
    $ext =~ /^\w+$/ or $ext = "dat";
    my $trunc = $root . ($ext ? ".$ext" : '');
    if (!$self->evil_filename($trunc)) {
	$self->debug("looks like I can use the truncated last path element");
	return $trunc;
    }

    ### Remove all bad characters
    $trunc =~ s/([^-A-Z0-9_+=.,@\#\$ ])/sprintf("%%%02X", unpack("C", $1))/ige;
    if (!$self->evil_filename($trunc)) {
	$self->debug("looks like I can use a munged version of the truncated last path element");
	return $trunc;
    }

    ### Hope that works:
    undef;
}

sub find_unused_path {
    my ($self, $dir, $fname) = @_;
    my $i = 0;
    while (1) {

	### Create suffixed name (from filename), and see if we can use it:
	my $suffix = ($i ? "-$i" : "");
	my $sname = $fname; $sname =~ s/^(.*?)(\.|\Z)/$1$suffix$2/;
	my $path = File::Spec->catfile($dir, $sname);
	if (! -e $path) {   ### it's good!
	    $i and $self->whine("collision with $fname in $dir: using $path");
	    return $path;
	}
	$self->debug("$path already taken");
    } continue { ++$i; }
}

sub ignore_filename {
    my $self = shift;
    $self->{MPF_IgnoreFilename} = $_[0] if @_;
    $self->{MPF_IgnoreFilename};
}

sub output_dir {
    my ($self, $head) = @_;
    return ".";
}

sub output_filename {
    my ($self, $head) = @_;

    ### Get the recommended name:
    my $recommended = $head->recommended_filename;

    ### Get content type:
    my ($type, $subtype) = split m{/}, $head->mime_type; $subtype ||= '';

    ### Get recommended extension, being quite conservative:
    my $recommended_ext = (($recommended and ($recommended =~ m{(\.\w+)\Z}))
			   ? $1
			   : undef);

    ### Try and get an extension, honoring a given one first:
    my $ext = ($recommended_ext ||
	       $self->{MPF_Ext}{"$type/$subtype"} ||
	       $self->{MPF_Ext}{"$type/*"} ||
	       $self->{MPF_Ext}{"*/*"} ||
	       ".dat");

    ### Get a prefix:
    ++$GFileNo;
    return ($self->output_prefix . "-" . untainted_pid() . "-$GFileNo$ext");
}

sub output_prefix {
    my ($self, $prefix) = @_;
    $self->{MPF_Prefix} = $prefix if (@_ > 1);
    $self->{MPF_Prefix};
}

sub output_type_ext  {
    my $self = shift;
    return $self->{MPF_Ext};
}

sub output_path {
    my ($self, $head) = @_;

    ### Get the output directory:
    my $dir = $self->output_dir($head);

    ### Get the output filename as UTF-8
    my $fname = $head->recommended_filename;

    ### Can we use it:
    if    (!defined($fname)) {
	$self->debug("no filename recommended: synthesizing our own");
	$fname = $self->output_filename($head);
    }
    elsif ($self->ignore_filename) {
	$self->debug("ignoring all external filenames: synthesizing our own");
	$fname = $self->output_filename($head);
    }
    elsif ($self->evil_filename($fname)) {

	### Can we save it by just taking the last element?
	my $ex = $self->exorcise_filename($fname);
	if (defined($ex) and !$self->evil_filename($ex)) {
	    $self->whine("Provided filename '$fname' is regarded as evil, ",
			 "but I was able to exorcise it and get something ",
			 "usable.");
	    $fname = $ex;
	}
	else {
	    $self->whine("Provided filename '$fname' is regarded as evil; ",
			 "I'm ignoring it and supplying my own.");
	    $fname = $self->output_filename($head);
	}
    } else {
            # Untaint $fname... we know it's not evil
            if ($fname =~ /^(.*)$/) {
                    $fname = $1;
            }
    }

    $self->debug("planning to use '$fname'");

    ### Resolve collisions and return final path:
    return $self->find_unused_path($dir, $fname);
}

sub purge {
    my ($self) = @_;
    foreach my $path (reverse @{$self->{MPF_Purgeable}}) {
	(-e $path) or next;   ### must check: might delete DIR before DIR/FILE
	rmtree($path, 0, 1);
	(-e $path) and $self->whine("unable to purge: $path");
    }
    1;
}

sub purgeable {
    my ($self, $path) = @_;
    return @{$self->{MPF_Purgeable}} if (@_ == 1);

    if (ref($path)) { $self->{MPF_Purgeable} = [ @$path ]; }
    else            { push @{$self->{MPF_Purgeable}}, $path; }
    1;
}

package MIME::Parser::FileInto;

use strict;
use vars qw(@ISA);
@ISA = qw(MIME::Parser::Filer);

sub init {
    my ($self, $dir) = @_;
    $self->{MPFI_Dir} = $self->cleanup_dir($dir);
}

#------------------------------
#
# output_dir HEAD
#
# I<Instance method, concrete override.>
# Return the output directory where the files go.
#
sub output_dir {
    shift->{MPFI_Dir};
}

package MIME::Parser::FileUnder;

use strict;
use vars qw(@ISA);
@ISA = qw(MIME::Parser::Filer);

sub init {
    my ($self, $basedir, %opts) = @_;

    $self->{MPFU_Base}    = $self->cleanup_dir($basedir);
    $self->{MPFU_DirName} = $opts{DirName};
    $self->{MPFU_Purge}   = $opts{Purge};
}

#------------------------------
#
# init_parse
#
# I<Instance method, override.>
# Prepare to start parsing a new message.
#
sub init_parse {
    my $self = shift;

    ### Invoke inherited method first!
    $self->SUPER::init_parse;

    ### Determine the subdirectory of their base to use:
    my $subdir = (defined($self->{MPFU_DirName})
		  ?       $self->{MPFU_DirName}
		  :       ("msg-" . scalar(time) . "-" .
                           MIME::Parser::Filer::untainted_pid() . "-" . $GSubdirNo++));
    $self->debug("subdir = $subdir");

    ### Determine full path to the per-message output directory:
    $self->{MPFU_Dir} = File::Spec->catfile($self->{MPFU_Base}, $subdir);

    ### Remove and re-create the per-message output directory:
    rmtree $self->output_dir if $self->{MPFU_Purge};
    (-d $self->output_dir) or
	mkdir $self->output_dir, 0700 or
	    die "mkdir ".$self->output_dir.": $!\n";

    ### Add the per-message output directory to the puregables:
    $self->purgeable($self->output_dir);
    1;
}

#------------------------------
#
# output_dir HEAD
#
# I<Instance method, concrete override.>
# Return the output directory that we used for the last parse.
#
sub output_dir {
    shift->{MPFU_Dir};
}

1;
__END__
