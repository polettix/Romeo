package MIME::Entity;

#------------------------------

### Pragmas:
use vars qw(@ISA $VERSION);
use strict;

### System modules:
use Carp;

### Other modules:
use Mail::Internet 1.28 ();
use Mail::Field    1.05 ();

### Kit modules:
use MIME::Tools qw(:config :msgs :utils);
use MIME::Head;
use MIME::Body;
use MIME::Decoder;

@ISA = qw(Mail::Internet);


#------------------------------
#
# Globals...
#
#------------------------------

### The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

### Boundary counter:
my $BCount = 0;

### Standard "Content-" MIME fields, for scrub():
my $StandardFields = 'Description|Disposition|Id|Type|Transfer-Encoding';

### Known Mail/MIME fields... these, plus some general forms like
### "x-*", are recognized by build():
my %KnownField = map {$_=>1}
qw(
   bcc         cc          comments      date          encrypted
   from        keywords    message-id    mime-version  organization
   received    references  reply-to      return-path   sender
   subject     to
   );

### Fallback preamble and epilogue:
my $DefPreamble = [ "This is a multi-part message in MIME format..." ];
my $DefEpilogue = [ ];


#==============================
#
# Utilities, private
#

#------------------------------
#
# known_field FIELDNAME
#
# Is this a recognized Mail/MIME field?
#
sub known_field {
    my $field = lc(shift);
    $KnownField{$field} or ($field =~ m{^(content|resent|x)-.});
}

#------------------------------
#
# make_boundary
#
# Return a unique boundary string.
# This is used both internally and by MIME::ParserBase, but it is NOT in
# the public interface!  Do not use it!
#
# We generate one containing a "=_", as RFC2045 suggests:
#    A good strategy is to choose a boundary that includes a character
#    sequence such as "=_" which can never appear in a quoted-printable
#    body.  See the definition of multipart messages in RFC 2046.
#
sub make_boundary {
    return "----------=_".scalar(time)."-$$-".$BCount++;
}

sub new {
    my $class = shift;
    my $self = $class->Mail::Internet::new(@_);   ### inherited
    $self->{ME_Parts} = [];                       ### no parts extracted
    $self;
}

sub add_part {
    my ($self, $part, $index) = @_;
    defined($index) or $index = -1;

    ### Make $index count from the end if negative:
    $index = $#{$self->{ME_Parts}} + 2 + $index if ($index < 0);
    splice(@{$self->{ME_Parts}}, $index, 0, $part);
    $part;
}

sub attach {
    my $self = shift;
    $self->make_multipart;
    $self->add_part(ref($self)->build(@_, Top=>0));
}

sub build {
    my ($self, @paramlist) = @_;
    my %params = @paramlist;
    my ($field, $filename, $boundary);

    ### Create a new entity, if needed:
    ref($self) or $self = $self->new;


    ### GET INFO...

    ### Get sundry field:
    my $type         = $params{Type} || 'text/plain';
    my $charset      = $params{Charset};
    my $is_multipart = ($type =~ m{^multipart/}i);
    my $encoding     = $params{Encoding} || '';
    my $desc         = $params{Description};
    my $top          = exists($params{Top}) ? $params{Top} : 1;
    my $disposition  = $params{Disposition} || 'inline';
    my $id           = $params{Id};

    ### Get recommended filename, allowing explicit no-value value:
    my ($path_fname) = (($params{Path}||'') =~ m{([^/]+)\Z});
    $filename = (exists($params{Filename}) ? $params{Filename} : $path_fname);
    $filename = undef if (defined($filename) and $filename eq '');

    ### Type-check sanity:
    if ($type =~ m{^(multipart/|message/(rfc822|partial|external-body|delivery-status|disposition-notification|feedback-report)$)}i) {
	($encoding =~ /^(|7bit|8bit|binary|-suggest)$/i)
	    or croak "can't have encoding $encoding for message type $type!";
    }

    ### Multipart or not? Do sanity check and fixup:
    if ($is_multipart) {      ### multipart...

	### Get any supplied boundary, and check it:
	if (defined($boundary = $params{Boundary})) {  ### they gave us one...
	    if ($boundary eq '') {
		whine "empty string not a legal boundary: I'm ignoring it";
		$boundary = undef;
	    }
	    elsif ($boundary =~ m{[^0-9a-zA-Z_\'\(\)\+\,\.\/\:\=\?\- ]}) {
		whine "boundary ignored: illegal characters ($boundary)";
		$boundary = undef;
	    }
	}

	### If we have to roll our own boundary, do so:
	defined($boundary) or $boundary = make_boundary();
    }
    else {                    ### single part...
	### Create body:
	if ($params{Path}) {
	    $self->bodyhandle(new MIME::Body::File $params{Path});
	}
	elsif (defined($params{Data})) {
	    $self->bodyhandle(new MIME::Body::InCore $params{Data});
	}
	else {
	    die "can't build entity: no body, and not multipart\n";
	}

	### Check whether we need to binmode():   [Steve Kilbane]
	$self->bodyhandle->binmode(1) unless textual_type($type);
    }


    ### MAKE HEAD...

    ### Create head:
    my $head = new MIME::Head;
    $self->head($head);
    $head->modify(1);

    ### Add content-type field:
    $field = new Mail::Field 'Content_type';         ### not a typo :-(
    $field->type($type);
    $field->charset($charset)    if $charset;
    $field->name($filename)      if defined($filename);
    $field->boundary($boundary)  if defined($boundary);
    $head->replace('Content-type', $field->stringify);

    ### Now that both body and content-type are available, we can suggest
    ### content-transfer-encoding (if desired);
    if (!$encoding) {
	$encoding = $self->suggest_encoding_lite;
    }
    elsif (lc($encoding) eq '-suggest') {
	$encoding = $self->suggest_encoding;
    }

    ### Add content-disposition field (if not multipart):
    unless ($is_multipart) {
	$field = new Mail::Field 'Content_disposition';  ### not a typo :-(
	$field->type($disposition);
	$field->filename($filename) if defined($filename);
	$head->replace('Content-disposition', $field->stringify);
    }

    ### Add other MIME fields:
    $head->replace('Content-transfer-encoding', $encoding) if $encoding;
    $head->replace('Content-description', $desc)           if $desc;

    # Content-Id value should be surrounded by < >, but versions before 5.428
    # did not do this.  So, we check, and add if the caller has not done so
    # already.
    if( defined $id ) {
	if( $id !~ /^<.*>$/ ) {
		$id = "<$id>";
	}
	$head->replace('Content-id', $id);
    }
    $head->replace('MIME-Version', '1.0')                  if $top;

    ### Add the X-Mailer field, if top level (use default value if not given):
    $top and $head->replace('X-Mailer',
			    "MIME-tools ".(MIME::Tools->version).
			    " (Entity "  .($VERSION).")");

    ### Add remaining user-specified fields, if any:
    while (@paramlist) {
	my ($tag, $value) = (shift @paramlist, shift @paramlist);

	### Get fieldname, if that's what it is:
	if    ($tag =~ /^-(.*)/s)  { $tag = lc($1) }    ### old style, b.c.
	elsif ($tag =~ /(.*):$/s ) { $tag = lc($1) }    ### new style
	elsif (known_field(lc($tag)))     { 1 }    ### known field
	else { next; }                             ### not a field

	### Clear head, get list of values, and add them:
	$head->delete($tag);
	foreach $value (ref($value) ? @$value : ($value)) {
	    (defined($value) && ($value ne '')) or next;
	    $head->add($tag, $value);
	}
    }

    ### Done!
    $self;
}

sub dup {
    my $self = shift;
    local($_);

    ### Self (this will also dup the header):
    my $dup = bless $self->SUPER::dup(), ref($self);

    ### Any simple inst vars:
    foreach (keys %$self) {$dup->{$_} = $self->{$_} unless ref($self->{$_})};

    ### Bodyhandle:
    $dup->bodyhandle($self->bodyhandle ? $self->bodyhandle->dup : undef);

    ### Preamble and epilogue:
    foreach (qw(ME_Preamble ME_Epilogue)) {
	$dup->{$_} = [@{$self->{$_}}]  if $self->{$_};
    }

    ### Parts:
    $dup->{ME_Parts} = [];
    foreach (@{$self->{ME_Parts}}) { push @{$dup->{ME_Parts}}, $_->dup }

    ### Done!
    $dup;
}

sub body {
	my ($self, $value) = @_;
	my $boundary_delimiter = $MIME::Entity::BOUNDARY_DELIMITER || "\n";
	if (@_ > 1) {      ### setting body line(s)...
		croak "you cannot use body() to set the encoded contents\n";
	} else {
		my $output = '';
		my $fh = IO::File->new(\$output, '>:') or croak("Cannot open in-memory file: $!");
		$self->print_body($fh);
		close($fh);
		my @ary = split(/\n/, $output);
		# Each line needs the terminating newline
		@ary = map { "$_$boundary_delimiter" } @ary;

		return \@ary;
	}
}

sub bodyhandle {
    my ($self, $newvalue) = @_;
    my $value = $self->{ME_Bodyhandle};
    $self->{ME_Bodyhandle} = $newvalue if (@_ > 1);
    $value;
}

sub effective_type {
    my $self = shift;
    $self->{ME_EffType} = shift if @_;
    return ($self->{ME_EffType} ? lc($self->{ME_EffType}) : $self->mime_type);
}

sub epilogue {
    my ($self, $lines) = @_;
    $self->{ME_Epilogue} = $lines if @_ > 1;
    $self->{ME_Epilogue};
}

sub head {
    my ($self, $value) = @_;
    (@_ > 1) and $self->{'mail_inet_head'} = $value;
    $self->{'mail_inet_head'} ||= new MIME::Head;       ### KLUDGE!
}

sub is_multipart {
    my $self = shift;
    $self->head or return undef;        ### no head, so no MIME type!
    my ($type, $subtype) = split('/', $self->effective_type);
    (($type eq 'multipart') ? 1 : 0);
}

sub mime_type {
    my $self = shift;
    $self->head or return (wantarray ? () : undef);
    $self->head->mime_type;
}

sub open {
    my $self = shift;
    $self->bodyhandle and $self->bodyhandle->open(@_);
}

sub parts {
    my $self = shift;
    ref($_[0]) and return @{$self->{ME_Parts} = [@{$_[0]}]};  ### set the parts
    (@_ ? $self->{ME_Parts}[$_[0]] : @{$self->{ME_Parts}});
}

sub parts_DFS {
    my $self = shift;
    return ($self, map { $_->parts_DFS } $self->parts);
}

sub preamble {
    my ($self, $lines) = @_;
    $self->{ME_Preamble} = $lines if @_ > 1;
    $self->{ME_Preamble};
}

sub make_multipart {
    my ($self, $subtype, %opts) = @_;
    my $tag;
    $subtype ||= 'mixed';
    my $force = $opts{Force};

    ### Trap for simple case: already a multipart?
    return 'ALREADY' if ($self->is_multipart and !$force);

    ### Rip out our guts, and spew them into our future part:
    my $part = bless {%$self}, ref($self);         ### part is a shallow copy
    %$self = ();                                   ### lobotomize ourselves!
    $self->head($part->head->dup);                 ### dup the header

    ### Remove content headers from top-level, and set it up as a multipart:
    foreach $tag (grep {/^content-/i} $self->head->tags) {
	$self->head->delete($tag);
    }
    $self->head->mime_attr('Content-type'          => "multipart/$subtype");
    $self->head->mime_attr('Content-type.boundary' => make_boundary());

    ### Remove NON-content headers from the part:
    foreach $tag (grep {!/^content-/i} $part->head->tags) {
	$part->head->delete($tag);
    }

    ### Add the [sole] part:
    $self->{ME_Parts} = [];
    $self->add_part($part);
    'DONE';
}

sub make_singlepart {
    my $self = shift;

    ### Trap for simple cases:
    return 'ALREADY' if !$self->is_multipart;      ### already a singlepart?
    return '0' if ($self->parts > 1);              ### can this even be done?

    # Get rid of all our existing content info
    my $tag;
    foreach $tag (grep {/^content-/i} $self->head->tags) {
        $self->head->delete($tag);
    }

    if ($self->parts == 1) {    ### one part
	my $part = $self->parts(0);

	### Populate ourselves with any content info from the part:
	foreach $tag (grep {/^content-/i} $part->head->tags) {
	    foreach ($part->head->get($tag)) { $self->head->add($tag, $_) }
	}

	### Save reconstructed header, replace our guts, and restore header:
	my $new_head = $self->head;
	%$self = %$part;               ### shallow copy is ok!
	$self->head($new_head);

	### One more thing: the part *may* have been a multi with 0 or 1 parts!
	return $self->make_singlepart(@_) if $self->is_multipart;
    }
    else {                      ### no parts!
	$self->head->mime_attr('Content-type'=>'text/plain');   ### simple
    }
    'DONE';
}

sub purge {
    my $self = shift;
    $self->bodyhandle and $self->bodyhandle->purge;      ### purge me
    foreach ($self->parts) { $_->purge }                 ### recurse
    1;
}

#------------------------------
#
# _do_remove_sig
#
# Private.  Remove a signature within NLINES lines from the end of BODY.
# The signature must be flagged by a line containing only "-- ".

sub _do_remove_sig {
    my ($body, $nlines) = @_;
    $nlines ||= 10;
    my $i = 0;

    my $line = int(@$body) || return;
    while ($i++ < $nlines and $line--) {
	if ($body->[$line] =~ /\A--[ \040][\r\n]+\Z/) {
	    $#{$body} = $line-1;
	    return;
	}
    }
}

sub remove_sig {
    my $self = shift;
    my $nlines = shift;

    # If multipart, we only attempt to remove the sig from the first
    # part.  This is usually a good assumption for multipart/mixed, but
    # may not always be correct.  It is also possibly incorrect on
    # multipart/alternative (both may have sigs).
    if( $self->is_multipart ) {
	my $first_part = $self->parts(0);
	if( $first_part ) {
            return $first_part->remove_sig(@_);
	}
	return undef;
    }

    ### Refuse non-textual unless forced:
    textual_type($self->head->mime_type)
	or return error "I won't un-sign a non-text message unless I'm forced";

    ### Get body data, as an array of newline-terminated lines:
    $self->bodyhandle or return undef;
    my @body = $self->bodyhandle->as_lines;

    ### Nuke sig:
    _do_remove_sig(\@body, $nlines);

    ### Output data back into body:
    my $io = $self->bodyhandle->open("w");
    foreach (@body) { $io->print($_) };  ### body data
    $io->close;

    ### Done!
    1;
}

sub sign {
    my $self = shift;
    my %params = @_;
    my $io;

    my $boundary_delimiter = $MIME::Entity::BOUNDARY_DELIMITER || "\n";
    ### If multipart and not attaching, try to sign our first part:
    if ($self->is_multipart and !$params{Attach}) {
	return $self->parts(0)->sign(@_);
    }

    ### Get signature:
    my $sig;
    if (defined($sig = $params{Signature})) {    ### scalar or array
	$sig = (ref($sig) ? join('', @$sig) : $sig);
    }
    elsif ($params{File}) {                      ### file contents
	my $fh = IO::File->new( $params{File} ) or croak "can't open $params{File}: $!";
	$sig = join('', $fh->getlines);
	$fh->close or croak "can't close $params{File}: $!";
    }
    else {
	croak "no signature given!";
    }

    ### Add signature to message as appropriate:
    if ($params{Attach}) {      ### Attach .sig as new part...
	return $self->attach(Type        => 'text/plain',
			     Description => 'Signature',
			     Disposition => 'inline',
			     Encoding    => '-SUGGEST',
			     Data        => $sig);
    }
    else {                      ### Add text of .sig to body data...

	### Refuse non-textual unless forced:
	($self->head->mime_type =~ m{text/}i or $params{Force}) or
	    return error "I won't sign a non-text message unless I'm forced";

	### Get body data, as an array of newline-terminated lines:
	$self->bodyhandle or return undef;
	my @body = $self->bodyhandle->as_lines;

	### Nuke any existing sig?
	if (!defined($params{Remove}) || ($params{Remove} > 0)) {
	    _do_remove_sig(\@body, $params{Remove});
	}

	### Output data back into body, followed by signature:
	my $line;
	$io = $self->open("w") or croak("open: $!");
	foreach $line (@body) { $io->print($line) };      ### body data
	(($body[-1]||'') =~ /\n\Z/) or $io->print($boundary_delimiter);  ### ensure final \n
	$io->print("-- $boundary_delimiter$sig");                          ### separator + sig
	$io->close or croak("close: $!");
	return 1;         ### done!
    }
}

### TO DO: resolve encodings of nested entities (possibly in sync_headers).

sub suggest_encoding {
    my $self = shift;

    my ($type) = split '/', $self->effective_type;
    if (($type eq 'text') || ($type eq 'message')) {    ### scan message body
	$self->bodyhandle || return ($self->parts ? 'binary' : '7bit');
	my ($IO, $unclean);
	if ($IO = $self->bodyhandle->open("r")) {
	    ### Scan message for 7bit-cleanliness
	    local $_;
	    while (defined($_ = $IO->getline)) {
		last if ($unclean = ((length($_) > 999) or /[\200-\377]/));
	    }

	    ### Return '7bit' if clean; try and encode if not...
	    ### Note that encodings are not permitted for messages!
	    return ($unclean
		    ? (($type eq 'message') ? 'binary' : 'quoted-printable')
		    : '7bit');
	}
    }
    else {
	return ($type eq 'multipart') ? 'binary' : 'base64';
    }
}

sub suggest_encoding_lite {
    my $self = shift;
    my ($type) = split '/', $self->effective_type;
    return (($type =~ /^(text|message|multipart)$/) ? 'binary' : 'base64');
}

sub sync_headers {
    my $self = shift;
    my $opts = ((int(@_) % 2 == 0) ? {@_} : shift);
    my $ENCBODY;     ### keep it around until done!

    ### Get options:
    my $o_nonstandard = ($opts->{Nonstandard} || 0);
    my $o_length      = ($opts->{Length}      || 0);

    ### Get head:
    my $head = $self->head;

    ### What to do with "nonstandard" MIME fields?
    if ($o_nonstandard eq 'ERASE') {       ### Erase them...
	my $tag;
	foreach $tag ($head->tags()) {
	    if (($tag =~ /\AContent-/i) &&
		($tag !~ /\AContent-$StandardFields\Z/io)) {
		$head->delete($tag);
	    }
	}
    }

    ### What to do with the "Content-Length" MIME field?
    if ($o_length eq 'COMPUTE') {        ### Compute the content length...
	my $content_length = '';

	### We don't have content-lengths in multiparts...
	if ($self->is_multipart) {           ### multipart...
	    $head->delete('Content-length');
	}
	else {                               ### singlepart...

	    ### Get the encoded body, if we don't have it already:
	    unless ($ENCBODY) {
		$ENCBODY = tmpopen() || die "can't open tmpfile";
		$self->print_body($ENCBODY);    ### write encoded to tmpfile
	    }

	    ### Analyse it:
	    $ENCBODY->seek(0,2);                ### fast-forward
	    $content_length = $ENCBODY->tell;   ### get encoded length
	    $ENCBODY->seek(0,0);                ### rewind

	    ### Remember:
	    $self->head->replace('Content-length', $content_length);
	}
    }
    elsif ($o_length eq 'ERASE') {         ### Erase the content-length...
	$head->delete('Content-length');
    }

    ### Done with everything for us!
    undef($ENCBODY);

    ### Recurse:
    my $part;
    foreach $part ($self->parts) { $part->sync_headers($opts) or return undef }
    1;
}

sub tidy_body {
    usage "MIME::Entity::tidy_body currently does nothing";
    0;
}

sub dump_skeleton {
    my ($self, $fh, $indent) = @_;
    $fh or $fh = select;
    defined($indent) or $indent = 0;
    my $ind = '    ' x $indent;
    my $part;
    no strict 'refs';


    ### The content type:
    print $fh $ind,"Content-type: ",   ($self->mime_type||'UNKNOWN'),"\n";
    print $fh $ind,"Effective-type: ", ($self->effective_type||'UNKNOWN'),"\n";

    ### The name of the file containing the body (if any!):
    my $path = ($self->bodyhandle ? $self->bodyhandle->path : undef);
    print $fh $ind, "Body-file: ", ($path || 'NONE'), "\n";

    ### The recommended file name (thanks to Allen Campbell):
    my $filename = $self->head->recommended_filename;
    print $fh $ind, "Recommended-filename: ", $filename, "\n" if ($filename);

    ### The subject (note: already a newline if 2.x!)
    my $subj = $self->head->get('subject',0);
    defined($subj) or $subj = '';
    chomp($subj);
    print $fh $ind, "Subject: $subj\n" if $subj;

    ### The parts:
    my @parts = $self->parts;
    print $fh $ind, "Num-parts: ", int(@parts), "\n" if @parts;
    print $fh $ind, "--\n";
    foreach $part (@parts) {
	$part->dump_skeleton($fh, $indent+1);
    }
}
use Symbol;
sub print {
    my ($self, $out) = @_;
    my $boundary_delimiter = $MIME::Entity::BOUNDARY_DELIMITER || "\n";
    $out = select if @_ < 2;
    $out = Symbol::qualify($out,scalar(caller)) unless ref($out);

    $self->print_header($out);   ### the header
    $out->print($boundary_delimiter);
    $self->print_body($out);     ### the "stuff after the header"
}

sub print_body {
    my ($self, $out) = @_;
    $out ||= select;
    my ($type) = split '/', lc($self->mime_type);  ### handle by MIME type
    my $boundary_delimiter = $MIME::Entity::BOUNDARY_DELIMITER || "\n";

    ### Multipart...
    if ($type eq 'multipart') {
	my $boundary = $self->head->multipart_boundary;

	### Preamble:
	my $plines = $self->preamble;
	if (defined $plines) {
	    # Defined, so output the preamble if it exists (avoiding additional
	    # newline as per ticket 60931)
	    $out->print( join('', @$plines) . $boundary_delimiter) if (@$plines > 0);
	} else {
	    # Undefined, so use default preamble
	    $out->print( join('', @$DefPreamble) . $boundary_delimiter . $boundary_delimiter );
	}

	### Parts:
	my $part;
	foreach $part ($self->parts) {
	    $out->print("--$boundary$boundary_delimiter");
	    $part->print($out);
	    $out->print($boundary_delimiter);           ### needed for next delim/close
	}
	$out->print("--$boundary--$boundary_delimiter");

	### Epilogue:
	my $epilogue = join('', @{ $self->epilogue || $DefEpilogue });
	if ($epilogue ne '') {
	    $out->print($epilogue);
	    $out->print($boundary_delimiter) if ($epilogue !~ /\n\Z/);  ### be nice
	}
    }

    ### Singlepart type with parts...
    ###    This makes $ent->print handle message/rfc822 bodies
    ###    when parse_nested_messages('NEST') is on [idea by Marc Rouleau].
    elsif ($self->parts) {
	my $need_sep = 0;
	my $part;
	foreach $part ($self->parts) {
	    $out->print("$boundary_delimiter$boundary_delimiter") if $need_sep++;
	    $part->print($out);
	}
    }

    ### Singlepart type, or no parts: output body...
    else {
	$self->bodyhandle ? $self->print_bodyhandle($out)
	                  : whine "missing body; treated as empty";
    }
    1;
}

#------------------------------
#
# print_bodyhandle
#
# Instance method, unpublicized.  Print just the bodyhandle, *encoded*.
#
# WARNING: $self->print_bodyhandle() != $self->bodyhandle->print()!
# The former encodes, and the latter does not!
#
sub print_bodyhandle {
    my ($self, $out) = @_;
    $out ||= select;

    my $IO = $self->open("r")     || die "open body: $!";
    if ( $self->bodyhandle->is_encoded ) {
      ### Transparent mode: data is already encoded, so no
      ### need to encode it again
      my $buf;
      $out->print($buf) while ($IO->read($buf, 8192));
    } else {
      ### Get the encoding, defaulting to "binary" if unsupported:
      my $encoding = ($self->head->mime_encoding || 'binary');
      my $decoder = best MIME::Decoder $encoding;
      $decoder->head($self->head);      ### associate with head, if any
      $decoder->encode($IO, $out, textual_type($self->head->mime_type) ? 1 : 0)   || return error "encoding failed";
    }

    $IO->close;
    1;
}

sub stringify {
	my ($self) = @_;
	my $output = '';
	my $fh = IO::File->new( \$output, '>:' ) or croak("Cannot open in-memory file: $!");
	$self->print($fh);
	$fh->close;
	return $output;
}

sub as_string { shift->stringify };      ### silent BC

sub stringify_body {
	my ($self) = @_;
	my $output = '';
	my $fh = IO::File->new( \$output, '>:' ) or croak("Cannot open in-memory file: $!");
	$self->print_body($fh);
	$fh->close;
	return $output;
}

sub body_as_string { shift->stringify_body }

sub stringify_header {
    shift->head->stringify;
}
sub header_as_string { shift->stringify_header }


1;
__END__
