package MIME::Head;

use MIME::WordDecoder;

require 5.002;

### Pragmas:
use strict;
use vars qw($VERSION @ISA @EXPORT_OK);

### System modules:
use IO::File;

### Other modules:
use Mail::Header 1.09 ();
use Mail::Field  1.05 ();

### Kit modules:
use MIME::Words qw(:all);
use MIME::Tools qw(:config :msgs);
use MIME::Field::ParamVal;
use MIME::Field::ConTraEnc;
use MIME::Field::ContDisp;
use MIME::Field::ContType;

@ISA = qw(Mail::Header);


#------------------------------
#
# Public globals...
#
#------------------------------

### The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

### Sanity (we put this test after our own version, for CPAN::):
use Mail::Header 1.06 ();

sub new {
    my $class = shift;
    bless Mail::Header->new(@_), $class;
}

sub from_file {
    my ($self, $file, @opts) = @_; ### at this point, $self is inst. or class!
    my $class = ref($self) ? ref($self) : $self;

    ### Parse:
    my $fh = IO::File->new($file, '<') or return error("open $file: $!");
    $fh->binmode() or return error("binmode $file: $!");  # we expect to have \r\n at line ends, and want to keep 'em.
    $self = $class->new($fh, @opts);      ### now, $self is instance or undef
    $fh->close or return error("close $file: $!");
    $self;
}

sub read {
    my $self = shift;      ### either instance or class!
    ref($self) or $self = $self->new;    ### if used as class method, make new
    $self->SUPER::read(@_);
}

### Inherited.

#------------------------------
#
# copy
#
# Instance method, DEPRECATED.
# Duplicate the object.
#
sub copy {
    usage "deprecated: use dup() instead.";
    shift->dup(@_);
}

sub decode {
    my $self = shift;

    ### Warn if necessary:
    my $force = shift || 0;
    unless (($force eq "I_NEED_TO_FIX_THIS") ||
	    ($force eq "I_KNOW_WHAT_I_AM_DOING")) {
	usage "decode is deprecated for safety";
    }

    my ($tag, $i, @decoded);
    foreach $tag ($self->tags) {
	@decoded = map { scalar(decode_mimewords($_, Field=>$tag))
			 } $self->get_all($tag);
	for ($i = 0; $i < @decoded; $i++) {
	    $self->replace($tag, $decoded[$i], $i);
	}
    }
    $self->{MH_Decoded} = 1;
    $self;
}

#------------------------------
#
# exists
#
sub exists {
    usage "deprecated; use count() instead";
    shift->count(@_);
}

#------------------------------
#
# fields
#
sub fields {
    usage "deprecated: use tags() instead",
    shift->tags(@_);
}

sub get_all {
    my ($self, $tag) = @_;
    $self->count($tag) or return ();          ### empty if doesn't exist
    ($self->get($tag));
}

#------------------------------
#
# original_text
#
# Instance method, DEPRECATED.
# Return an approximation of the original text.
#
sub original_text {
    usage "deprecated: use stringify() instead";
    shift->stringify(@_);
}

sub print {
    my ($self, $fh) = @_;
    $fh ||= select;
    $fh->print($self->as_string);
}

#------------------------------
#
# set TAG,TEXT
#
# Instance method, DEPRECATED.
# Set the field named TAG to [the single occurrence given by the TEXT.
#
sub set {
    my $self = shift;
    usage "deprecated: use the replace() method instead.";
    $self->replace(@_);
}

sub stringify {
    my $self = shift;          ### build clean header, and output...
    my @header = grep {defined($_) ? $_ : ()} @{$self->header};
    my $header_delimiter = $MIME::Entity::BOUNDARY_DELIMITER || "\n";
    join "", map { /\n$/ ? substr($_, 0, -1) . $header_delimiter : $_ . $header_delimiter } @header;
}
sub as_string { shift->stringify(@_) }

#------------------------------
#
# params TAG
#
# Instance method, DEPRECATED.
# Extract parameter info from a structured field, and return
# it as a hash reference.  Provided for 1.0 compatibility only!
# Use the new MIME::Field interface classes (subclasses of Mail::Field).

sub params {
    my ($self, $tag) = @_;
    usage "deprecated: use the MIME::Field interface classes from now on!";
    return MIME::Field::ParamVal->parse_params($self->get($tag,0));
}

sub mime_attr {
    my ($self, $attr, $value) = @_;

    ### Break attribute name up:
    my ($tag, $subtag) = split /\./, $attr;
    $subtag ||= '_';

    ### Set or get?
    my $field = MIME::Field::ParamVal->parse($self->get($tag, 0));
    if (@_ > 2) {   ### set it:
	$field->param($subtag, $value);             ### set subfield
	$self->replace($tag, $field->stringify);    ### replace!
	return $value;
    }
    else {          ### get it:
	return $field->param($subtag);
    }
}

sub mime_encoding {
    my $self = shift;
    my $enc = lc($self->mime_attr('content-transfer-encoding') || '7bit');
    $enc =~ s{^([78])[ _-]bit\Z}{$1bit};
    $enc;
}

sub mime_type {
    my ($self, $default) = @_;
    $self->{MIH_DefaultType} = $default if @_ > 1;
    my $s = $self->mime_attr('content-type') ||
       $self->{MIH_DefaultType} ||
       'text/plain';
    # avoid [perl #87336] bug, lc laundering tainted data
    return lc($s)  if $] <= 5.008 || $] >= 5.014;
    $s =~ tr/A-Z/a-z/;
    $s;
}

sub multipart_boundary {
    my $self = shift;
    my $value =  $self->mime_attr('content-type.boundary');
    (!defined($value)) ? undef : $value;
}

sub recommended_filename
{
	my $self = shift;

	# Try these headers in order, taking the first defined,
	# non-blank one we find.
	my $wd = supported MIME::WordDecoder 'UTF-8';
	foreach my $attr_name ( qw( content-disposition.filename content-type.name ) ) {
		my $value = $self->mime_attr( $attr_name );
		if ( defined $value
		    && $value ne ''
		    && $value =~ /\S/ ) {
			return $wd->decode($value);
		}
	}

	return undef;
}

#------------------------------
#
# tweak_FROM_parsing
#
# DEPRECATED.  Use the inherited mail_from() class method now.

sub tweak_FROM_parsing {
    my $self = shift;
    usage "deprecated.  Use mail_from() instead.";
    $self->mail_from(@_);
}

1;

__END__
