package MIME::Field::ContType;

require 5.001;
use strict;
use MIME::Field::ParamVal;
use vars qw($VERSION @ISA);

@ISA = qw(MIME::Field::ParamVal);

# The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

# Install it:
bless([])->register('Content-type');

#------------------------------
#
# Basic access/storage methods...
#
sub charset {
    lc(shift->paramstr('charset', @_)) || 'us-ascii';   # RFC 2045
}
sub id {
    shift->paramstr('id', @_);
}
sub name {
    shift->paramstr('name', @_);
}
sub number {
    shift->paramstr('number', @_);
}
sub total {
    shift->paramstr('total', @_);
}

sub boundary {
    my $value = shift->param('boundary', @_);
    defined($value) || return '';
    $value =~ s/\s+$//;                  # kill trailing white, per RFC 2046
    $value;
}

sub multipart_boundary {
    my $self = shift;
    my ($type) = split('/', $self->type);
    return '' if ($type ne 'multipart');    # not multipart!
    $self->boundary;                        # okay, return the boundary
}

sub type {
    lc(shift->paramstr('_', @_)) || 'text/plain';  # RFC 2045
}

1;
