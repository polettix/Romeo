package MIME::Field::ConTraEnc;

require 5.001;
use strict;
use MIME::Field::ParamVal;
use vars qw($VERSION @ISA);

@ISA = qw(MIME::Field::ParamVal);

# The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

# Install it:
bless([])->register('Content-transfer-encoding');

#------------------------------

sub encoding {
    shift->paramstr('_', @_);
}

#------------------------------
1;

