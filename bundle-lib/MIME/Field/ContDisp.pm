package MIME::Field::ContDisp;

require 5.001;
use strict;
use MIME::Field::ParamVal;
use vars qw($VERSION @ISA);

@ISA = qw(MIME::Field::ParamVal);

# The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

# Install it:
bless([])->register('Content-disposition');

#------------------------------

sub filename {
    shift->paramstr('filename', @_);
}

sub type {
    shift->paramstr('_', @_);
}

#------------------------------
1;

