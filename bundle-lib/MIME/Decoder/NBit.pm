package MIME::Decoder::NBit;
use strict;
use warnings;

use vars qw(@ISA $VERSION);

use MIME::Decoder;
use MIME::Tools qw(:msgs);

@ISA = qw(MIME::Decoder);

### The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

### How many bytes to decode at a time?
my $DecodeChunkLength = 8 * 1024;

#------------------------------
#
# decode_it IN, OUT
#
sub decode_it {
    my ($self, $in, $out) = @_;
    my $and_also;

    ### Allocate a buffer suitable for a chunk and a line:
    local $_ = (' ' x ($DecodeChunkLength + 1024)); $_ = '';

    ### Get chunks until done:
    while ($in->read($_, $DecodeChunkLength)) {
	$and_also = $in->getline;
	$_ .= $and_also if defined($and_also);

	### Just got a chunk ending in a line.
	s/\015\012$/\n/g;
	$out->print($_);
    }
    1;
}

#------------------------------
#
# encode_it IN, OUT
#
sub encode_it {
    my ($self, $in, $out) = @_;
    my $saw_8bit = 0;    ### warn them ONCE PER ENCODING if 8-bit data exists
    my $saw_long = 0;    ### warn them ONCE PER ENCODING if long lines exist
    my $seven_bit = ($self->encoding eq '7bit');      ### 7bit?

    my $line;
    while (defined($line = $in->getline)) {

	### Whine if encoding is 7bit and it has 8-bit data:
	if ($seven_bit && ($line =~ /[\200-\377]/)) { ### oops! saw 8-bit data!
	    whine "saw 8-bit data while encoding 7bit" unless $saw_8bit++;
	}

	### Whine if long lines detected:
	if (length($line) > 998) {
	    whine "saw long line while encoding 7bit/8bit" unless $saw_long++;
	}

	### Output!
	$out->print($line);
    }
    1;
}

1;
