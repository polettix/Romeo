package MIME::Decoder::Gzip64;

require 5.002;
use strict;
use vars qw(@ISA $VERSION $GZIP $GUNZIP);
use MIME::Decoder;
use MIME::Base64;
use MIME::Decoder::Base64;
use MIME::Tools qw(tmpopen whine);

# Inheritance:
@ISA = qw(MIME::Decoder::Base64);

# The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

# How to compress stdin to stdout:
$GZIP   = "gzip -c";

# How to UNcompress stdin to stdout:
$GUNZIP = "gzip -d -c";


#------------------------------
#
# decode_it IN, OUT
#
sub decode_it {
    my ($self, $in, $out) = @_;

    # Open a temp file (assume the worst, that this is a big stream):
    my $tmp = tmpopen() || die "can't get temp file";

    # Stage 1: decode the base64'd stream into zipped data:
    $self->SUPER::decode_it($in, $tmp)    or die "base64 decoding failed!";
    
    # Stage 2: un-zip the zipped data:
    $tmp->seek(0, 0); 
    $self->filter($tmp, $out, $GUNZIP)    or die "gzip decoding failed!";
}

#------------------------------
#
# encode_it IN, OUT
#
sub encode_it {
    my ($self, $in, $out) = @_;
    whine "Encoding ", $self->encoding, " is not standard MIME!"; 
    
    # Open a temp file (assume the worst, that this is a big stream):
    my $tmp = tmpopen() || die "can't get temp file";
  
    # Stage 1: zip the raw data:
    $self->filter($in, $tmp, $GZIP)       or die "gzip encoding failed!";
    
    # Stage 2: encode the zipped data via base64:
    $tmp->seek(0, 0);    
    $self->SUPER::encode_it($tmp, $out)   or die "base64 encoding failed!";
}

#------------------------------
1;
