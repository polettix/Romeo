package MIME::Decoder::Binary;
use strict;
use warnings;

use MIME::Decoder;
use vars qw(@ISA $VERSION);

@ISA = qw(MIME::Decoder);

### The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

### Buffer length:
my $BUFLEN = 8192;

#------------------------------
#
# decode_it IN, OUT
#
sub decode_it {
    my ($self, $in, $out) = @_;

    my ($buf, $nread) = ('', 0);
    while ($nread = $in->read($buf, $BUFLEN)) {
	$out->print($buf);
    }
    defined($nread) or return undef;      ### check for error
    1;
}

#------------------------------
#
# encode_it IN, OUT
#
sub encode_it {
    my ($self, $in, $out) = @_;

    my ($buf, $nread) = ('', 0);
    while ($nread = $in->read($buf, $BUFLEN)) {
	$out->print($buf);
    }
    defined($nread) or return undef;      ### check for error
    1;
}

#------------------------------
1;
