package MIME::WordDecoder;

use strict;
use Carp qw( carp croak );
use MIME::Words qw(decode_mimewords);
use Exporter;
use vars qw(@ISA @EXPORT);

@ISA = qw(Exporter);
@EXPORT = qw( unmime mime_to_perl_string );



#------------------------------
#
# Globals
#
#------------------------------

### Decoders.
my %DecoderFor = ();

### Standard handlers.
my %Handler =
(
 KEEP   => sub {$_[0]},
 IGNORE => sub {''},
 WARN   => sub { carp "ignoring text in character set `$_[1]'\n" },
 DIE    => sub { croak "can't handle text in character set `$_[1]'\n" },
 );

### Global default decoder.  We init it below.
my $Default;

### Global UTF8 decoder.
my $DefaultUTF8;

sub default {
    my $class = shift;
    if (@_) {
	$Default = shift;
    }
    $Default;
}

sub supported {
    my ($class, $charset, $decoder) = @_;
    $DecoderFor{uc($charset)} = $decoder if (@_ > 2);
    $DecoderFor{uc($charset)};
}

sub new {
    my ($class, $h) = @_;
    my $self = bless { MWD_Map=>{} }, $class;

    ### Init the map:
    $self->handler(@$h);

    ### Add fallbacks:
    $self->{MWD_Map}{'*'}   ||= $Handler{WARN};
    $self->{MWD_Map}{'raw'} ||= $self->{MWD_Map}{'US-ASCII'};
    $self;
}

sub handler {
    my $self = shift;

    ### Copy the hash, and edit it:
    while (@_) {
	my $c   = shift;
	my $sub = shift;
	$self->{MWD_Map}{$c} = $self->real_handler($sub);
    }
    $self;
}

sub decode {
    my ($self, $str) = @_;
    defined($str) or return undef;
    join('', map {
	### Get the data and (upcased) charset:
	my $data    = $_->[0];
	my $charset = (defined($_->[1]) ? uc($_->[1]) : 'raw');
	$charset =~ s/\*\w+\Z//;   ### RFC2184 language suffix

	### Get the handler; guess if never seen before:
	defined($self->{MWD_Map}{$charset}) or
	    $self->{MWD_Map}{$charset} =
		($self->real_handler($self->guess_handler($charset)) || 0);
	my $subr = $self->{MWD_Map}{$charset} || $self->{MWD_Map}{'*'};

	### Map this chunk:
	&$subr($data, $charset, $self);
    } decode_mimewords($str));
}

#------------------------------
#
# guess_handler CHARSET
#
# Instance method.
# An unrecognized charset has been seen.  Guess a handler subref
# for the given charset, returning false if there is none.
# Successful mappings will be cached in the main map.
#
sub guess_handler {
    undef;
}

#------------------------------
#
# real_handler HANDLER
#
# Instance method.
# Translate the given handler, which might be a subref or a string.
#
sub real_handler {
    my ($self, $sub) = @_;
    (!$sub) or
	(ref($sub) eq 'CODE') or
	    $sub = ($Handler{$sub} || croak "bad named handler: $sub\n");
    $sub;
}

sub unmime($) {
    my $str = shift;
    $Default->decode($str);
}

sub mime_to_perl_string($) {
    my $str = shift;
    $DecoderFor{'UTF-8'}->decode($str);
}

package MIME::WordDecoder::ISO_8859;

use strict;
use vars qw(@ISA);
@ISA = qw( MIME::WordDecoder );


#------------------------------
#
# HANDLERS
#
#------------------------------

### Keep 7bit characters.
### Turn all else to the special \x00.
sub h_keep7bit {
    local $_    = $_[0];
#   my $unknown = $_[2]->{MWDI_Unknown};

    s{[\x80-\xFF]}{\x00}g;
    $_;
}

### Note: should use Unicode::String, converting/manipulating
### everything into full Unicode form.

### Keep 7bit UTF8 characters (ASCII).
### Keep ISO-8859-1 if this decoder is for Latin-1.
### Turn all else to the special \x00.
sub h_utf8 {
    local $_    = $_[0];
#   my $unknown = $_[2]->{MWDI_Unknown};
    my $latin1 = ($_[2]->{MWDI_Num} == 1);
    #print STDERR "UTF8 in:  <$_>\n";

    local($1,$2,$3);
    my $tgt = '';
    while (m{\G(
          ([\x00-\x7F])                | # 0xxxxxxx
	  ([\xC0-\xDF] [\x80-\xBF])    | # 110yyyyy 10xxxxxx
	  ([\xE0-\xEF] [\x80-\xBF]{2}) | # 1110zzzz 10yyyyyy 10xxxxxx
	  ([\xF0-\xF7] [\x80-\xBF]{3}) | # 11110uuu 10uuzzzz 10yyyyyy 10xxxxxx
	  .                              # error; synch
	  )}gcsx and ($1 ne '')) {

	if    (defined($2))            { $tgt .= $2 }
	elsif (defined($3) && $latin1) { $tgt .= "\x00" }
        else                           { $tgt .= "\x00" }
    }

    #print STDERR "UTF8 out: <$tgt>\n";
    $tgt;
}

### Keep characters which are 7bit in UTF8 (ASCII).
### Keep ISO-8859-1 if this decoder is for Latin-1.
### Turn all else to the special \x00.
sub h_utf16 {
    local $_    = $_[0];
#   my $unknown = $_[2]->{MWDI_Unknown};
    my $latin1 = ($_[2]->{MWDI_Num} == 1);
    #print STDERR "UTF16 in:  <$_>\n";

    local($1,$2,$3,$4,$5);
    my $tgt = '';
    while (m{\G(
		(  \x00  ([\x00-\x7F])) |  # 00000000 0xxxxxxx
		(  \x00  ([\x80-\xFF])) |  # 00000000 1xxxxxxx
		( [^\x00] [\x00-\xFF])  |  # etc
		)
	     }gcsx and ($1 ne '')) {

	if    (defined($2))            { $tgt .= $3 }
	elsif (defined($4) && $latin1) { $tgt .= $5 }
        else                           { $tgt .= "\x00" }
    }

    #print STDERR "UTF16 out: <$tgt>\n";
    $tgt;
}


#------------------------------
#
# PUBLIC INTERFACE
#
#------------------------------

#------------------------------
#
# new NUMBER
#
sub new {
    my ($class, $num) = @_;

    my $self = $class->SUPER::new();
    $self->handler('raw'      => 'KEEP',
		   'US-ASCII' => 'KEEP');

    $self->{MWDI_Num} = $num;
    $self->{MWDI_Unknown} = "?";
    $self->{MWDI_Collapse} = 0;
    $self;
}

#------------------------------
#
# guess_handler CHARSET
#
sub guess_handler {
    my ($self, $charset) = @_;
    return 'KEEP'              if (($charset =~ /^ISO[-_]?8859[-_](\d+)$/) &&
				   ($1 eq $self->{MWDI_Num}));
    return \&h_keep7bit        if ($charset =~ /^ISO[-_]?8859/);
    return \&h_utf8            if ($charset =~ /^UTF[-_]?8$/);
    return \&h_utf16           if ($charset =~ /^UTF[-_]?16$/);
    undef;
}

#------------------------------
#
# unknown [REPLACEMENT]
#
sub unknown {
    my $self = shift;
    $self->{MWDI_Unknown} = shift if @_;
    $self->{MWDI_Unknown};
}

#------------------------------
#
# collapse [YESNO]
#
sub collapse {
    my $self = shift;
    $self->{MWDI_Collapse} = shift if @_;
    $self->{MWDI_Collapse};
}

#------------------------------
#
# decode STRING
#
sub decode {
    my $self = shift;

    ### Do inherited action:
    my $basic = $self->SUPER::decode(@_);
    defined($basic) or return undef;

    ### Translate/consolidate illegal characters:
    $basic =~ tr{\x00}{\x00}c     if $self->{MWDI_Collapse};
    $basic =~ s{\x00}{$self->{MWDI_Unknown}}g;
    $basic;
}

package MIME::WordDecoder::US_ASCII;

use strict;
use vars qw(@ISA);
@ISA = qw( MIME::WordDecoder::ISO_8859 );

sub new {
    my ($class) = @_;
    return $class->SUPER::new("1");
}

sub decode {
    my $self = shift;

    ### Do inherited action:
    my $basic = $self->SUPER::decode(@_);
    defined($basic) or return undef;

    ### Translate/consolidate 8-bit characters:
    $basic =~ tr{\x80-\xFF}{}c     if $self->{MWDI_Collapse};
    $basic =~ s{[\x80-\xFF]}{$self->{MWDI_Unknown}}g;
    $basic;
}

package MIME::WordDecoder::UTF_8;
use strict;
use Encode qw();
use Carp qw( carp );
use vars qw(@ISA);

@ISA = qw( MIME::WordDecoder );

sub h_convert_to_utf8
{
	my ($data, $charset, $decoder) = @_;
	$charset = 'US-ASCII' if ($charset eq 'raw');
	my $enc = Encode::find_encoding($charset);
	if (!$enc) {
		carp "Unable to convert text in character set `$charset' to UTF-8... ignoring\n";
		return '';
	}
	my $ans = $enc->decode($data, Encode::FB_PERLQQ);
	return $ans;
}

sub new {
	my ($class) = @_;
	my $self = $class->SUPER::new();
	$self->handler('*'     => \&h_convert_to_utf8);
}


package MIME::WordDecoder;

### Now we can init the default handler.
$Default = (MIME::WordDecoder::ISO_8859->new('1'));


### Add US-ASCII handler:
$DecoderFor{"US-ASCII"} = MIME::WordDecoder::US_ASCII->new;

### Add ISO-8859-{1..15} handlers:
for (1..15) {
    $DecoderFor{"ISO-8859-$_"} = MIME::WordDecoder::ISO_8859->new($_);
}

### UTF-8
$DecoderFor{'UTF-8'} = MIME::WordDecoder::UTF_8->new();

1;           # end the module
__END__
