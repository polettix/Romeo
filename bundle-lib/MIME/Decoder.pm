package MIME::Decoder;

### Pragmas:
use strict;
use vars qw($VERSION %DecoderFor);

### System modules:
use IPC::Open2;
use IO::Select;
use FileHandle;

### Kit modules:
use MIME::Tools qw(:config :msgs);
use Carp;

#------------------------------
#
# Globals
#
#------------------------------

### The stream decoders:
%DecoderFor = (

  ### Standard...
    '7bit'       => 'MIME::Decoder::NBit',
    '8bit'       => 'MIME::Decoder::NBit',
    'base64'     => 'MIME::Decoder::Base64',
    'binary'     => 'MIME::Decoder::Binary',
    'none'       => 'MIME::Decoder::Binary',
    'quoted-printable' => 'MIME::Decoder::QuotedPrint',

  ### Non-standard...
    'binhex'     => 'MIME::Decoder::BinHex',
    'binhex40'   => 'MIME::Decoder::BinHex',
    'mac-binhex40' => 'MIME::Decoder::BinHex',
    'mac-binhex' => 'MIME::Decoder::BinHex',
    'x-uu'       => 'MIME::Decoder::UU',
    'x-uuencode' => 'MIME::Decoder::UU',

  ### This was removed, since I fear that x-gzip != x-gzip64...
### 'x-gzip'     => 'MIME::Decoder::Gzip64',

  ### This is no longer installed by default, since not all folks have gzip:
### 'x-gzip64'   => 'MIME::Decoder::Gzip64',
);

### The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

### Me:
my $ME = 'MIME::Decoder';

sub new {
    my ($class, @args) = @_;
    my ($encoding) = @args;

    ### Coerce the type to be legit:
    $encoding = lc($encoding || '');

    ### Get the class:
    my $concrete_name = $DecoderFor{$encoding};

    if( ! $concrete_name ) {
	carp "no decoder for $encoding";
	return undef;
    }

    ### Create the new object (if we can):
    my $self = { MD_Encoding => lc($encoding) };
    unless (eval "require $concrete_name;") {
	carp $@;
	return undef;
    }
    bless $self, $concrete_name;
    $self->init(@args);
}

sub best {
    my ($class, $enc, @args) = @_;
    my $self = $class->new($enc, @args);
    if (!$self) {
	usage "unsupported encoding '$enc': using 'binary'";
	$self = $class->new('binary') || croak "ack! no binary decoder!";
    }
    $self;
}

sub decode {
    my ($self, $in, $out) = @_;

    ### Set up the default input record separator to be CRLF:
    ### $in->input_record_separator("\012\015");

    ### Invoke back-end method to do the work:
    $self->decode_it($in, $out) ||
	die "$ME: ".$self->encoding." decoding failed\n";
    1;
}

sub encode {
    my ($self, $in, $out, $textual_type) = @_;

    ### Invoke back-end method to do the work:
    $self->encode_it($in, $out, $self->encoding eq 'quoted-printable' ? ($textual_type) : ()) ||
	die "$ME: ".$self->encoding." encoding failed\n";
}

sub encoding {
    shift->{MD_Encoding};
}

sub head {
    my ($self, $head) = @_;
    $self->{MD_Head} = $head if @_ > 1;
    $self->{MD_Head};
}

sub supported {
    my ($class, $decoder) = @_;
    defined($decoder) ? $DecoderFor{lc($decoder)}: { %DecoderFor };
}

sub decode_it {
    die "attempted to use abstract 'decode_it' method!";
}

sub encode_it {
    die "attempted to use abstract 'encode_it' method!";
}

sub filter
{
	my ($self, $in, $out, @cmd) = @_;
	my $buf = '';

	### Open pipe:
	STDOUT->flush;  ### very important, or else we get duplicate output!

	my $kidpid = open2(my $child_out, my $child_in, @cmd) || die "@cmd: open2 failed: $!";

	### We have to use select() for doing both reading and writing.
	my $rsel = IO::Select->new( $child_out );
	my $wsel = IO::Select->new( $child_in  );

	while (1) {

		### Wait for one hour; if that fails, it's too bad.
		my ($read, $write) = IO::Select->select( $rsel, $wsel, undef, 3600);

		if( !defined $read && !defined $write ) {
			kill 1, $kidpid;
			waitpid $kidpid, 0;
			die "@cmd: select failed: $!";
		}

		### If can read from child:
		if( my $fh = shift @$read ) {
			if( $fh->sysread(my $buf, 1024) ) {
				$out->print($buf);
			} else {
				$rsel->remove($fh);
				$fh->close();
			}
		}

		### If can write to child:
		if( my $fh = shift @$write ) {
			if($in->read(my $buf, 1024)) {
				local $SIG{PIPE} = sub {
					warn "got SIGPIPE from @cmd";
					$wsel->remove($fh);
					$fh->close();
				};
				$fh->syswrite( $buf );
			} else {
				$wsel->remove($fh);
				$fh->close();
			}
		}

		### If both $child_out and $child_in are done:
		last unless ($rsel->count() || $wsel->count());
	}

	### Wait for it:
	waitpid($kidpid, 0) == $kidpid or die "@cmd: couldn't reap child $kidpid";
	### Check if it failed:
	$? == 0 or die "@cmd: bad exit status: \$? = $?";
	1;
}

sub init {
    $_[0];
}

sub install {
    my $class = shift;
    $DecoderFor{lc(shift @_)} = $class while (@_);
}

sub uninstall {
    shift;
    $DecoderFor{lc(shift @_)} = undef while (@_);
}

1;

__END__
