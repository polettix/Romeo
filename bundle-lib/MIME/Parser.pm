package MIME::Parser;

#------------------------------

require 5.004;

### Pragmas:
use strict;
use vars (qw($VERSION $CAT $CRLF));

### core Perl modules
use IO::File;
use File::Spec;
use File::Path;
use Config qw(%Config);
use Carp;

### Kit modules:
use MIME::Tools qw(:config :utils :msgtypes usage tmpopen );
use MIME::Head;
use MIME::Body;
use MIME::Entity;
use MIME::Decoder;
use MIME::Parser::Reader;
use MIME::Parser::Filer;
use MIME::Parser::Results;

#------------------------------
#
# Globals
#
#------------------------------

### The package version, both in 1.23 style *and* usable by MakeMaker:
$VERSION = "5.510";

### How to catenate:
$CAT = '/bin/cat';

### The CRLF sequence:
$CRLF = "\015\012";

### Who am I?
my $ME = 'MIME::Parser';

sub new {
    my $self = bless {}, shift;
    $self->init(@_);
}

sub init {
    my $self = shift;

    $self->{MP5_DecodeHeaders}   = 0;
    $self->{MP5_DecodeBodies}    = 1;
    $self->{MP5_Interface}       = {};
    $self->{MP5_ParseNested}     = 'NEST';
    $self->{MP5_TmpToCore}       = 0;
    $self->{MP5_IgnoreErrors}    = 1;
    $self->{MP5_UUDecode}        = 0;
    $self->{MP5_MaxParts}        = -1;
    $self->{MP5_TmpDir}          = undef;

    $self->interface(ENTITY_CLASS => 'MIME::Entity');
    $self->interface(HEAD_CLASS   => 'MIME::Head');

    $self->output_dir(".");

    $self;
}

sub init_parse {
    my $self = shift;

    $self->{MP5_Results} = new MIME::Parser::Results;

    $self->{MP5_Filer}->results($self->{MP5_Results});
    $self->{MP5_Filer}->purgeable([]);
    $self->{MP5_Filer}->init_parse();
    $self->{MP5_NumParts} = 0;
    1;
}

sub decode_headers {
    my ($self, $yesno) = @_;
    if (@_ > 1) {
	$self->{MP5_DecodeHeaders} = $yesno;
	if ($yesno) {
	    if (($yesno eq "I_KNOW_WHAT_I_AM_DOING") ||
		($yesno eq "I_NEED_TO_FIX_THIS")) {
		### ok
	    }
	    else {
		$self->whine("as of 5.4xx, decode_headers() should NOT be ".
			     "set true... if you are doing this to make sure ".
			     "that non-ASCII filenames are translated, ".
			     "that's now done automatically; for all else, ".
			     "use MIME::Words.");
	    }
	}
    }
    $self->{MP5_DecodeHeaders};
}

sub extract_nested_messages {
    my ($self, $option) = @_;
    $self->{MP5_ParseNested} = $option if (@_ > 1);
    $self->{MP5_ParseNested};
}

sub parse_nested_messages {
    usage "parse_nested_messages() is now extract_nested_messages()";
    shift->extract_nested_messages(@_);
}

sub extract_uuencode {
    my ($self, $yesno) = @_;
    $self->{MP5_UUDecode} = $yesno if @_ > 1;
    $self->{MP5_UUDecode};
}

sub ignore_errors {
    my ($self, $yesno) = @_;
    $self->{MP5_IgnoreErrors} = $yesno if (@_ > 1);
    $self->{MP5_IgnoreErrors};
}


sub decode_bodies {
    my ($self, $yesno) = @_;
    $self->{MP5_DecodeBodies} = $yesno if (@_ > 1);
    $self->{MP5_DecodeBodies};
}

#------------------------------
#
# MESSAGES...
#

#------------------------------
#
# debug MESSAGE...
#
sub debug {
    my $self = shift;
    if (MIME::Tools->debugging()) {
	    if (my $r = $self->{MP5_Results}) {
		    unshift @_, $r->indent;
		    $r->msg($M_DEBUG, @_);
	    }
	    MIME::Tools::debug(@_);
    }
}

#------------------------------
#
# whine PROBLEM...
#
sub whine {
    my $self = shift;
    if (my $r = $self->{MP5_Results}) {
	unshift @_, $r->indent;
	$r->msg($M_WARNING, @_);
    }
    &MIME::Tools::whine(@_);
}

#------------------------------
#
# error PROBLEM...
#
# Possibly-forgivable parse error occurred.
# Raises a fatal exception unless we are ignoring errors.
#
sub error {
    my $self = shift;
    if (my $r = $self->{MP5_Results}) {
	unshift @_, $r->indent;
	$r->msg($M_ERROR, @_);
    }
    &MIME::Tools::error(@_);
    $self->{MP5_IgnoreErrors} ? return undef : die @_;
}




#------------------------------
#
# PARSING...
#

#------------------------------
#
# process_preamble IN, READER, ENTITY
#
# I<Instance method.>
# Dispose of a multipart message's preamble.
#
sub process_preamble {
    my ($self, $in, $rdr, $ent) = @_;

    ### Sanity:
    ($rdr->depth > 0) or die "$ME: internal logic error";

    ### Parse preamble:
    my @saved;
    my $data = '';
    open(my $fh, '>', \$data) or die $!;
    $rdr->read_chunk($in, $fh, 1);
    close $fh;

    # Ugh.  Horrible.  If the preamble consists only of CRLF, squash it down
    # to the empty string.  Else, remove the trailing CRLF.
    if( $data =~ m/^[\r\n]\z/ ) {
	@saved = ('');
    } else {
	$data =~ s/[\r\n]\z//;
        @saved = split(/^/, $data);
    }
    $ent->preamble(\@saved);
    1;
}

#------------------------------
#
# process_epilogue IN, READER, ENTITY
#
# I<Instance method.>
# Dispose of a multipart message's epilogue.
#
sub process_epilogue {
    my ($self, $in, $rdr, $ent) = @_;
    $self->debug("process_epilogue");

    ### Parse epilogue:
    my @saved;
    $rdr->read_lines($in, \@saved);
    $ent->epilogue(\@saved);
    1;
}

#------------------------------
#
# process_to_bound IN, READER, OUT
#
# I<Instance method.>
# Dispose of the next chunk into the given output stream OUT.
#
sub process_to_bound {
    my ($self, $in, $rdr, $out) = @_;

    ### Parse:
    $rdr->read_chunk($in, $out);
    1;
}

#------------------------------
#
# process_header IN, READER
#
# I<Instance method.>
# Process and return the next header.
# Return undef if, instead of a header, the encapsulation boundary is found.
# Fatal exception on failure.
#
sub process_header {
    my ($self, $in, $rdr) = @_;
    $self->debug("process_header");

    ### Parse and save the (possibly empty) header, up to and including the
    ###    blank line that terminates it:
    my $head = $self->interface('HEAD_CLASS')->new;

    ### Read the lines of the header.
    ### We localize IO inside here, so that we can support the IO:: interface
    my @headlines;
    my $hdr_rdr = $rdr->spawn;
    $hdr_rdr->add_terminator("");
    $hdr_rdr->add_terminator("\r");           ### sigh

    my $headstr = '';
    open(my $outfh, '>:scalar', \$headstr) or die $!;
    $hdr_rdr->read_chunk($in, $outfh, 0, 1);
    close $outfh;

    ### How did we do?
    if ($hdr_rdr->eos_type eq 'DELIM') {
       $self->whine("bogus part, without CRLF before body");
       return undef;
    }
    ($hdr_rdr->eos_type eq 'DONE') or
	$self->error("unexpected end of header\n");


    ### If header line begins with a UTF-8 Byte-Order mark, remove it.
    $headstr =~ s/^\x{EF}\x{BB}\x{BF}//;

    ### Extract the header (note that zero-size headers are admissible!):
    open(my $readfh, '<:scalar', \$headstr) or die $!;
    $head->read( $readfh );

    unless( $readfh->eof() ) {
	# Not entirely correct, since ->read consumes the line it gives up on.
	# it's actually the line /before/ the one we get with ->getline
	$self->error("couldn't parse head; error near:\n", $readfh->getline());
    }


    ### If desired, auto-decode the header as per RFC 2047
    ###    This shouldn't affect non-encoded headers; however, it will decode
    ###    headers with international characters.  WARNING: currently, the
    ###    character-set information is LOST after decoding.
    $head->decode($self->{MP5_DecodeHeaders}) if $self->{MP5_DecodeHeaders};

    ### If this is the top-level head, save it:
    $self->results->top_head($head) if !$self->results->top_head;

    return $head;
}

#------------------------------
#
# process_multipart IN, READER, ENTITY
#
# I<Instance method.>
# Process the multipart body, and return the state.
# Fatal exception on failure.
# Invoked by process_part().
#
sub process_multipart {
    my ($self, $in, $rdr, $ent) = @_;
    my $head = $ent->head;

    $self->debug("process_multipart...");

    ### Get actual type and subtype from the header:
    my ($type, $subtype) = (split('/', $head->mime_type, -1), '');

    ### If this was a type "multipart/digest", then the RFCs say we
    ### should default the parts to have type "message/rfc822".
    ### Thanks to Carsten Heyl for suggesting this...
    my $retype = (($subtype eq 'digest') ? 'message/rfc822' : '');

    ### Get the boundaries for the parts:
    my $bound = $head->multipart_boundary;
    if (!defined($bound) || ($bound =~ /[\r\n]/)) {
	$self->error("multipart boundary is missing, or contains CR or LF\n");
	$ent->effective_type("application/x-unparseable-multipart");
	return $self->process_singlepart($in, $rdr, $ent);
    }
    my $part_rdr = $rdr->spawn->add_boundary($bound);

    ### Prepare to parse:
    my $eos_type;
    my $more_parts;

    ### Parse preamble...
    $self->process_preamble($in, $part_rdr, $ent);

    ### ...and look at how we finished up:
    $eos_type = $part_rdr->eos_type;
    if    ($eos_type eq 'DELIM'){ $more_parts = 1 }
    elsif ($eos_type eq 'CLOSE'){ $self->whine("empty multipart message\n");
				  $more_parts = 0; }
    else                        { $self->error("unexpected end of preamble\n");
				  return 1; }

    ### Parse parts:
    my $partno = 0;
    my $part;
    while ($more_parts) {
	++$partno;
	$self->debug("parsing part $partno...");

	### Parse the next part, and add it to the entity...
	my $part = $self->process_part($in, $part_rdr, Retype=>$retype);
	return undef unless defined($part);

	$ent->add_part($part);

	### ...and look at how we finished up:
	$eos_type = $part_rdr->eos_type;
	if    ($eos_type eq 'DELIM') { $more_parts = 1 }
	elsif ($eos_type eq 'CLOSE') { $more_parts = 0; }
	else                         { $self->error("unexpected end of parts ".
						    "before epilogue\n");
				       return 1; }
    }

    ### Parse epilogue...
    ###    (note that we use the *parent's* reader here, which does not
    ###     know about the boundaries in this multipart!)
    $self->process_epilogue($in, $rdr, $ent);

    ### ...and there's no need to look at how we finished up!
    1;
}

#------------------------------
#
# process_singlepart IN, READER, ENTITY
#
# I<Instance method.>
# Process the singlepart body.  Returns true.
# Fatal exception on failure.
# Invoked by process_part().
#
sub process_singlepart {
    my ($self, $in, $rdr, $ent) = @_;
    my $head    = $ent->head;

    $self->debug("process_singlepart...");

    ### Obtain a filehandle for reading the encoded information:
    ###    We have two different approaches, based on whether or not we
    ###    have to contend with boundaries.
    my $ENCODED;             ### handle
    my $can_shortcut = (!$rdr->has_bounds and !$self->{MP5_UUDecode});
    if ($can_shortcut) {
	$self->debug("taking shortcut");

	$ENCODED = $in;
	$rdr->eos('EOF');   ### be sure to bogus-up the reader state to EOF:
    }
    else {

	$self->debug("using temp file");
	$ENCODED = $self->new_tmpfile();

	### Read encoded body until boundary (or EOF)...
	$self->process_to_bound($in, $rdr, $ENCODED);

	### ...and look at how we finished up.
	###     If we have bounds, we want DELIM or CLOSE.
	###     Otherwise, we want EOF (and that's all we'd get, anyway!).
	if ($rdr->has_bounds) {
	    ($rdr->eos_type =~ /^(DELIM|CLOSE)$/) or
		$self->error("part did not end with expected boundary\n");
	}

	### Flush and rewind encoded buffer, so we can read it:
	$ENCODED->flush or die "$ME: can't flush: $!";
	$ENCODED->seek(0, 0) or die "$ME: can't seek: $!";
    }

    ### Get a content-decoder to decode this part's encoding:
    my $encoding = $head->mime_encoding;
    my $decoder = new MIME::Decoder $encoding;
    if (!$decoder) {
	$self->whine("Unsupported encoding '$encoding': using 'binary'... \n".
		     "The entity will have an effective MIME type of \n".
		     "application/octet-stream.");  ### as per RFC-2045
	$ent->effective_type('application/octet-stream');
	$decoder = new MIME::Decoder 'binary';
	$encoding = 'binary';
    }

    ### Data should be stored encoded / as-is?
    if ( !$self->decode_bodies ) {
	$decoder = new MIME::Decoder 'binary';
	$encoding = 'binary';
    }

    ### If desired, sidetrack to troll for UUENCODE:
    $self->debug("extract uuencode? ", $self->extract_uuencode);
    $self->debug("encoding?         ", $encoding);
    $self->debug("effective type?   ", $ent->effective_type);

    if ($self->extract_uuencode and
	($encoding =~ /^(7bit|8bit|binary)\Z/) and
	($ent->effective_type =~
		m{^(?:text/plain|application/mac-binhex40|application/mac-binhex)\Z})) {
	### Hunt for it:
	my $uu_ent = eval { $self->hunt_for_uuencode($ENCODED, $ent) };
	if ($uu_ent) {   ### snark
	    %$ent = %$uu_ent;
	    return 1;
	}
	else {           ### boojum
	    $self->whine("while hunting for uuencode: $@");
	    $ENCODED->seek(0,0) or die "$ME: can't seek: $!";
	}
    }

    ### Open a new bodyhandle for outputting the data:
    my $body = $self->new_body_for($head) or die "$ME: no body"; # gotta die
    $body->binmode(1) or die "$ME: can't set to binmode: $!"
        unless textual_type($ent->effective_type) or !$self->decode_bodies;
    $body->is_encoded(1) if !$self->decode_bodies;

    ### Decode and save the body (using the decoder):
    my $DECODED = $body->open("w") or die "$ME: body not opened: $!";
    eval { $decoder->decode($ENCODED, $DECODED); };
    $@ and $self->error($@);
    $DECODED->close or die "$ME: can't close: $!";

    ### Success!  Remember where we put stuff:
    $ent->bodyhandle($body);

    ### Done!
    1;
}

#------------------------------
#
# hunt_for_uuencode ENCODED, ENTITY
#
# I<Instance method.>
# Try to detect and dispatch embedded uuencode as a fake multipart message.
# Returns new entity or undef.
#
sub hunt_for_uuencode {
    my ($self, $ENCODED, $ent) = @_;
    my ($good, $how_encoded);
    local $_;
    $self->debug("sniffing around for UUENCODE");

    ### Heuristic:
    $ENCODED->seek(0,0) or die "$ME: can't seek: $!";
    while (defined($_ = $ENCODED->getline)) {
	if ($good = /^begin [0-7]{3}/) {
	  $how_encoded = 'uu';
	  last;
	}
	if ($good = /^\(This file must be converted with/i) {
	  $how_encoded = 'binhex';
	  last;
	}
    }
    $good or do { $self->debug("no one made the cut"); return 0 };

    # If a decoder doesn't exist for this type, forget it!
    my $decoder = MIME::Decoder->new(($how_encoded eq 'uu')?'x-uuencode'
						     :'binhex');
    unless (defined($decoder)) {
	$self->debug("No decoder for $how_encoded attachments");
	return 0;
    }

    ### New entity:
    my $top_ent = $ent->dup;      ### no data yet
    $top_ent->make_multipart;
    my @parts;

    ### Made the first cut; on to the real stuff:
    $ENCODED->seek(0,0) or die "$ME: can't seek: $!";
    $self->whine("Found a $how_encoded attachment");
    my $pre;
    while (1) {
	my $bin_data = '';

	### Try next part:
	my $out = IO::File->new(\$bin_data, '>:');
	eval { $decoder->decode($ENCODED, $out) }; last if $@;
	my $preamble = $decoder->last_preamble;
	my $filename = $decoder->last_filename;
	my $mode     = $decoder->last_mode;

	### Get probable type:
	my $type = 'application/octet-stream';
	my ($ext) = $filename =~ /\.(\w+)\Z/; $ext = lc($ext || '');
	if ($ext =~ /^(gif|jpe?g|xbm|xpm|png)\Z/) { $type = "image/$1" }

	### If we got our first preamble, create the text portion:
	if (@$preamble and
	    (grep /\S/, @$preamble) and
	    !@parts) {
	    my $txt_ent = $self->interface('ENTITY_CLASS')->new;

	    MIME::Entity->build(Type => "text/plain",
				Data => "");
	    $txt_ent->bodyhandle($self->new_body_for($txt_ent->head));
	    my $io = $txt_ent->bodyhandle->open("w") or die "$ME: can't create: $!";
	    $io->print(@$preamble) or die "$ME: can't print: $!";
	    $io->close or die "$ME: can't close: $!";
	    push @parts, $txt_ent;
	}

	### Create the attachment:
	### We use the x-unix-mode convention from "dtmail 1.2.1 SunOS 5.6".
	if (1) {
	    my $bin_ent = MIME::Entity->build(Type=>$type,
					      Filename=>$filename,
					      Data=>"");
	    $bin_ent->head->mime_attr('Content-type.x-unix-mode' => "0$mode");
	    $bin_ent->bodyhandle($self->new_body_for($bin_ent->head));
	    $bin_ent->bodyhandle->binmode(1) or die "$ME: can't set to binmode: $!";
	    my $io = $bin_ent->bodyhandle->open("w") or die "$ME: can't create: $!";
	    $io->print($bin_data) or die "$ME: can't print: $!";
	    $io->close or die "$ME: can't close: $!";
	    push @parts, $bin_ent;
	}
    }

    ### Did we get anything?
    @parts or return undef;
    ### Set the parts and a nice preamble:
    $top_ent->parts(\@parts);
    $top_ent->preamble
	(["The following is a multipart MIME message which was extracted\n",
	  "from a $how_encoded-encoded message.\n"]);
    $top_ent;
}

#------------------------------
#
# process_message IN, READER, ENTITY
#
# I<Instance method.>
# Process the singlepart body, and return true.
# Fatal exception on failure.
# Invoked by process_part().
#
sub process_message {
    my ($self, $in, $rdr, $ent) = @_;
    my $head = $ent->head;

    $self->debug("process_message");

    ### Verify the encoding restrictions:
    my $encoding = $head->mime_encoding;
    if ($encoding !~ /^(7bit|8bit|binary)$/) {
	$self->error("illegal encoding [$encoding] for MIME type ".
		     $head->mime_type."\n");
	$encoding = 'binary';
    }

    ### Parse the message:
    my $msg = $self->process_part($in, $rdr);
    return undef unless defined($msg);

    ### How to handle nested messages?
    if ($self->extract_nested_messages eq 'REPLACE') {
	%$ent = %$msg;          ### shallow replace
	%$msg = ();
    }
    else {                      ### "NEST" or generic 1:
	$ent->bodyhandle(undef);
	$ent->add_part($msg);
    }
    1;
}

#------------------------------
#
# process_part IN, READER, [OPTSHASH...]
#
# I<Instance method.>
# The real back-end engine.
# See the documentation up top for the overview of the algorithm.
# The OPTSHASH can contain:
#
#    Retype => retype this part to the given content-type
#
# Return the entity.
# Fatal exception on failure.  Returns undef if message to complex
#
sub process_part {
    my ($self, $in, $rdr, %p) = @_;

    if ($self->{MP5_MaxParts} > 0) {
	$self->{MP5_NumParts}++;
	if ($self->{MP5_NumParts} > $self->{MP5_MaxParts}) {
	    # Return UNDEF if msg too complex
	    return undef;
	}
    }

    $rdr ||= MIME::Parser::Reader->new;
    #debug "process_part";
    $self->results->level(+1);

    ### Create a new entity:
    my $ent = $self->interface('ENTITY_CLASS')->new;

    ### Parse and add the header:
    my $head = $self->process_header($in, $rdr);
    if (not defined $head) {
       $self->debug("bogus empty part");
       $head = $self->interface('HEAD_CLASS')->new;
       $head->mime_type('text/plain');
       $ent->head($head);
       $ent->bodyhandle($self->new_body_for($head));
       $ent->bodyhandle->open("w")->close or die "$ME: can't close: $!";
       $self->results->level(-1);
       return $ent;
    }
    $ent->head($head);

    ### Tweak the content-type based on context from our parent...
    ### For example, multipart/digest messages default to type message/rfc822:
    $head->mime_type($p{Retype}) if $p{Retype};

    ### Get the MIME type and subtype:
    my ($type, $subtype) = (split('/', $head->mime_type, -1), '');
    $self->debug("type = $type, subtype = $subtype");

    ### Handle, according to the MIME type:
    if ($type eq 'multipart') {
	return undef unless defined($self->process_multipart($in, $rdr, $ent));
    }
    elsif (("$type/$subtype" eq "message/rfc822" ||
	    "$type/$subtype" eq "message/external-body" ||
	    ("$type/$subtype" eq "message/partial" && defined($head->mime_attr("content-type.number")) && $head->mime_attr("content-type.number") == 1)) &&
	    $self->extract_nested_messages) {
	$self->debug("attempting to process a nested message");
	return undef unless defined($self->process_message($in, $rdr, $ent));
    }
    else {
	$self->process_singlepart($in, $rdr, $ent);
    }

    ### Done (we hope!):
    $self->results->level(-1);
    return $ent;
}

sub parse_data {
    my ($self, $data) = @_;

    if (!defined($data)) {
	    croak "parse_data: No data passed";
    }

    ### Get data as a scalar:
    my $io;

    if (! ref $data ) {
        $io = IO::File->new(\$data, '<:');
    } elsif( ref $data eq 'SCALAR' ) {
        $io = IO::File->new($data, '<:');
    } elsif( ref $data eq 'ARRAY' ) {
	# Passing arrays is deprecated now that we've nuked IO::ScalarArray
	# but for backwards compatibility we still support it by joining the
	# array lines to a scalar and doing scalar IO on it.
	my $tmp_data = join('', @$data);
	$io = IO::File->new(\$tmp_data, '<:');
    } else {
        croak "parse_data: wrong argument ref type: ", ref($data);
    }

    if (!$io) {
	    croak "parse_data: unable to open in-memory file handle";
    }

    ### Parse!
    return $self->parse($io);
}

#------------------------------

=item parse INSTREAM

I<Instance method.>
Takes a MIME-stream and splits it into its component entities.

The INSTREAM can be given as an IO::File, a globref filehandle (like
C<\*STDIN>), or as I<any> blessed object conforming to the IO::
interface (which minimally implements getline() and read()).

Returns the parsed MIME::Entity on success.
Throws exception on failure.  If the message contained too many
parts (as set by I<max_parts>), returns undef.

=cut

sub parse {
    my $self = shift;
    my $in = shift;
    my $entity;
    local $/ = "\n";    ### just to be safe

    local $\ = undef; # CPAN ticket #71041
    $self->init_parse;
    $entity = $self->process_part($in, undef);  ### parse!

    $entity;
}

### Backcompat:
sub read {
    shift->parse(@_);
}
sub parse_FH {
    shift->parse(@_);
}

sub parse_open {
    my ($self, $expr) = @_;
    my $ent;

    my $io = IO::File->new($expr) or die "$ME: couldn't open $expr: $!";
    $ent = $self->parse($io);
    $io->close or die "$ME: can't close: $!";
    $ent;
}

### Backcompat:
sub parse_in {
    usage "parse_in() is now parse_open()";
    shift->parse_open(@_);
}

sub parse_two {
    my ($self, $headfile, $bodyfile) = @_;
    my $data;
    foreach ($headfile, $bodyfile) {
	open IN, "<$_" or die "$ME: open $_: $!";
	$data .= do { local $/; <IN> };
	close IN or die "$ME: can't close: $!";
    }
    return $self->parse_data($data);
}

sub filer {
    my ($self, $filer) = @_;
    if (@_ > 1) {
	$self->{MP5_Filer} = $filer;
	$filer->results($self->results);  ### but we still need in init_parse
    }
    $self->{MP5_Filer};
}

sub output_dir {
    my ($self, @init) = @_;
    if (@_ > 1) {
	$self->filer(MIME::Parser::FileInto->new(@init));
    }
    else {
	&MIME::Tools::whine("0-arg form of output_dir is deprecated.");
	return $self->filer->output_dir;
    }
}

sub output_under {
    my ($self, @init) = @_;
    if (@_ > 1) {
	$self->filer(MIME::Parser::FileUnder->new(@init));
    }
    else {
	&MIME::Tools::whine("0-arg form of output_under is deprecated.");
	return $self->filer->output_dir;
    }
}

sub output_path {
    my $self = shift;
    ### We use it, so don't warn!
    ### &MIME::Tools::whine("output_path deprecated in MIME::Parser");
    $self->filer->output_path(@_);
}

sub output_prefix {
    my $self = shift;
    &MIME::Tools::whine("output_prefix deprecated in MIME::Parser");
    $self->filer->output_prefix(@_);
}

sub evil_filename {
    my $self = shift;
    &MIME::Tools::whine("evil_filename deprecated in MIME::Parser");
    $self->filer->evil_filename(@_);
}

sub max_parts {
    my($self, $num) = @_;
    if (@_ > 1) {
	$self->{MP5_MaxParts} = $num;
    }
    return $self->{MP5_MaxParts};
}

sub output_to_core {
    my ($self, $yesno) = @_;
    if (@_ > 1) {
	$yesno = 0 if ($yesno and $yesno eq 'NONE');
	$self->{MP5_FilerToCore} = $yesno;
    }
    $self->{MP5_FilerToCore};
}


sub tmp_recycling 
{
	return;
}

sub tmp_to_core {
    my ($self, $yesno) = @_;
    $self->{MP5_TmpToCore} = $yesno if (@_ > 1);
    $self->{MP5_TmpToCore};
}

sub use_inner_files {
	return 0;
}

sub interface {
    my ($self, $role, $value) = @_;
    $self->{MP5_Interface}{$role} = $value if (defined($value));
    $self->{MP5_Interface}{$role};
}

sub new_body_for {
    my ($self, $head) = @_;

    if ($self->output_to_core) {
	$self->debug("outputting body to core");
	return (new MIME::Body::InCore);
    }
    else {
	my $outpath = $self->output_path($head);
	$self->debug("outputting body to disk file: $outpath");
	$self->filer->purgeable($outpath);        ### we plan to use it
	return (new MIME::Body::File $outpath);
    }
}

sub tmp_dir
{
    my ($self, $dirname) = @_;
    if ( $dirname ) {
	$self->{MP5_TmpDir} = $dirname;
    }

    return $self->{MP5_TmpDir};
}

sub new_tmpfile {
    my ($self) = @_;

    my $io;
    if ($self->{MP5_TmpToCore}) {
	my $var;
	$io = IO::File->new(\$var, '+>:') or die "$ME: Can't open in-core tmpfile: $!";
    } else {
	my $args = {};
	if( $self->tmp_dir ) {
		$args->{DIR} = $self->tmp_dir;
	}
	$io = tmpopen( $args ) or die "$ME: can't open tmpfile: $!\n";
	binmode($io) or die "$ME: can't set to binmode: $!";
    }
    return $io;
}

sub last_error {
    join '', shift->results->errors;
}

sub last_head {
    shift->results->top_head;
}

sub results {
    shift->{MP5_Results};
}

1;
__END__
